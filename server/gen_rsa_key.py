from Crypto.PublicKey import RSA

key = RSA.generate(2048)
with open("desire.pem", "wb") as f:
    f.write(key.export_key('PEM'))
