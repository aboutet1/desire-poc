Flask==1.1.2
Flask-RESTful==0.3.8
pycryptodome==3.9.7
flask-swagger-ui
pyyaml
