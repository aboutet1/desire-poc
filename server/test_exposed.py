# -*- coding:utf-8 -*-
import pytest
import requests
from test_util import create_user
from util import to_b64, random_int
from datetime import datetime

HOST = "http://localhost:8080/"

def test_exposed():
    (uid, key, r1, sigma1) = create_user(HOST, "0600000004")

    n = 10
    r = requests.get(HOST + "report", params = {"r": r1, "sigma": sigma1, "number_of_tokens": n})
    assert(r.status_code == 200)
    tokens = r.json()["tokens"]
    # Then reports n PETs
    for t in tokens:
        pet = to_b64(random_int(8), 8)
        r = requests.post(HOST + "report", json = {"token": t, "pet": pet, "datetime": int(datetime.timestamp(datetime.now()))})
        assert(r.status_code == 200)
    
    r = requests.get(HOST + "exposed", params = {"r": r1, "sigma": sigma1})
    assert(r.status_code == 200)

    l = r.json()["exposed"]
    assert(type(l) == list)
    assert(len(l) > 0)
