import pytest
import requests
from Crypto.Hash import SHA256
from util import to_b64, from_b64, gen_secret

HOST = "http://localhost:8080/"

@pytest.mark.run("first")
def test_register_get():
    global pin, public_key_e, public_key_n

    r = requests.get(HOST + "register", params = {"phone_number": "0600000000"})
    assert(r.status_code == 200)

    pin = r.json()["pin"]
    [public_key_e, public_key_n] = r.json()["public_key"]
    public_key_e = from_b64(public_key_e)
    public_key_n = from_b64(public_key_n)

@pytest.mark.run(after='test_register_get')
def test_register_post():
    global pin, public_key_e, public_key_n, secret_r, secret_sigma

    (secret_r, secret_c, inv_secret_c, secret) = gen_secret(public_key_e, public_key_n)

    r = requests.post(HOST + "register", json = {"pin": pin, "secret": to_b64(secret, 256)})
    assert(r.status_code == 200)

    rep = from_b64(r.json()["rep"])

    secret_r = to_b64(secret_r, 256)
    secret_sigma = to_b64((rep * inv_secret_c) % public_key_n, 256)

@pytest.mark.run(after='test_register_post')
def test_register_put():
    global secret_r, secret_sigma, uid, key

    r = requests.put(HOST + "register", json = {"r": secret_r, "sigma": secret_sigma})
    assert(r.status_code == 200)

    uid = r.json()["id"]
    key = r.json()["key"]
