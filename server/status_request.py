from util import DesireBaseError
from datetime import datetime

class StatusRequestError(DesireBaseError):
    pass

class StatusRequest:
    # TODO: put this parameter in a configuration file
    __ESR_min = 86400 # in s

    @staticmethod
    def check_request(user):
        now = datetime.now()

        if (now - user.sre).total_seconds() < StatusRequest.__ESR_min:
            raise StatusRequestError("Status request too close to the previous one", 1)

        user.sre = now
        
