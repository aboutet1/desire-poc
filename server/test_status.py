# -*- coding:utf-8 -*-
import pytest
import requests
from util import to_b64, random_int
from datetime import datetime
from test_util import create_user

HOST = "http://localhost:8080/"

@pytest.mark.run("first")
def test_status_init():
    global uid, key

    (uid, key, _, _) = create_user(HOST, "0600000001")

@pytest.mark.run(after = "test_status_init")
def test_status_get():
    global uid, key

    pets = [to_b64(random_int(8), 8) for i in range(10)]

    r = requests.get(HOST + "status", params = {"id": uid, "key": key, "pets": pets})
    assert(r.status_code == 200)

@pytest.mark.run(after = "test_status_get")
def test_status_get_fail():
    global uid, key

    pets = [to_b64(random_int(8), 8) for i in range(10)]

    r = requests.get(HOST + "status", params = {"id": uid, "key": key, "pets": pets})
    assert(r.status_code == 400)
    assert(r.json()["type"] == "StatusRequestError")

def test_at_risk():
    # User 1
    (uid1, key1, r1, sigma1) = create_user(HOST, "0600000002")
    # User 2
    (uid2, key2, r2, sigma2) = create_user(HOST, "0600000003")

    ### User 1 reports
    # First get n tokens
    n = 10
    r = requests.get(HOST + "report", params = {"r": r1, "sigma": sigma1, "number_of_tokens": n})
    assert(r.status_code == 200)
    tokens = r.json()["tokens"]
    # Then reports n PETs
    for t in tokens:
        pet = to_b64(random_int(8), 8)
        r = requests.post(HOST + "report", json = {"token": t, "pet": pet, "datetime": int(datetime.timestamp(datetime.now()))})
        assert(r.status_code == 200)

    ### User 2 ask his status
    l = [to_b64(random_int(8), 8) for i in range(n)]
    l[-1] = pet
    r = requests.get(HOST + "status", params = {"id": uid2, "key": key2, "pets": l})
    assert(r.status_code == 200)
