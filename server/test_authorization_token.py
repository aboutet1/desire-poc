# -*- coding:utf-8 -*-
import pytest
from authorization_token import AuthorizationToken, AuthorizationTokenError
from util import to_b64, from_b64, random_int, token_hash, gen_secret

def test_check_token_pass():
    token = [to_b64(1234567890, 256), to_b64(10254923758410301262030993467072873485423371521592858426823784813668106104226420851856882161862885060838327094481149167199460714916301821310339442655972619803870294291248848174125038067096145408131432495941150055684975301740820124085034002621989848496873569749984699605115071083803966954225573287125278813543608923298935571703309976468003961422973520501025542809108790887379251112412577827962893489617305533116913714160886094426633731994222327317033073370462762715514199584619296465803978652581399607282170553204690198248382060656054563129887762624332703948322660944874953274718029747643078629204023278170266167703517, 256)]
    AuthorizationToken.check_token(token)

def test_check_token_fail():
    token = [to_b64(1234567890, 256), to_b64(0, 256)]
    try:
        AuthorizationToken.check_token(token)
        raise Exception("Token should not be authorized")
    except AuthorizationTokenError:
        pass

def test_generate_pin_pass():
    global pin
    pin = AuthorizationToken.generate_pin("0600000000")

@pytest.mark.run(after='test_generate_pin_pass')
def test_generate_pin_fail():
    try:
        AuthorizationToken.generate_pin("0600000000")
        raise Exception("Should not be able to generate a pin from the same phone number twice")
    except AuthorizationTokenError:
        pass

@pytest.mark.run(after='test_generate_pin_pass')
def test_use_pin_pass():
    global pin
    AuthorizationToken.use_pin(pin)

@pytest.mark.run(after='test_use_pin_pass')
def test_use_pin_fail():
    global pin
    try:
        AuthorizationToken.use_pin(pin)
        raise Exception("Should not be able to use pin twice")
    except AuthorizationTokenError:
        pass

def test_use_pin_not_found():
    try:
        AuthorizationToken.use_pin(b"")
        raise Exception("Should not be able to use unknown pin")
    except AuthorizationTokenError:
        pass

def test_check_token():
    [public_key_e, public_key_n] = AuthorizationToken.get_public_key()
    public_key_e = from_b64(public_key_e)
    public_key_n = from_b64(public_key_n)

    (secret_r, secret_c, inv_secret_c, secret) = gen_secret(public_key_e, public_key_n)

    # Get decoded secret
    rep = from_b64(AuthorizationToken.compute_secret(to_b64(secret, 256)))

    # Compute second part of authorization token (sigma)
    sigma = (rep * inv_secret_c) % public_key_n

    AuthorizationToken.check_token([to_b64(secret_r, 32), to_b64(sigma, 256)])
