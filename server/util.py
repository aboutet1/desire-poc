# -*- coding:utf-8 -*-
from Crypto.Random import get_random_bytes
from base64 import b64encode, b64decode
from Crypto.Hash import SHA256

def to_b64(x, k):
    return b64encode(x.to_bytes(k, "big")).decode()

def from_b64(x):
    return int.from_bytes(b64decode(x), "big")

def random_int(n_bytes):
    return int.from_bytes(get_random_bytes(n_bytes), "big")

def token_hash(x):
    return int.from_bytes(SHA256.new(data = x.to_bytes(32, "big")).digest(), "big")

# Utility functions to compute inverse modulo n
def __modinv(a, m):
    def egcd(a, b):
        if a == 0:
            return (b, 0, 1)
        else:
            g, y, x = egcd(b % a, a)
            return (g, x - (b // a) * y, y)

    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def gen_secret(e, n):
    # Generates an inversible (modulo n) secret random number c
    while True:
        secret_c = random_int(32)

        try:
            inv_secret_c = __modinv(secret_c, n)
            break
        except:
            pass

    # Generates 256-bits random secret number R (first part of authorization token)
    secret_r = random_int(32)
    
    # Encode secret to send
    h = token_hash(secret_r)
    secret = (pow(secret_c, e) * h) % n

    return (secret_r, secret_c, inv_secret_c, secret)

class DesireBaseError(Exception):
    def __init__(self, msg, code):
        self.__code = code
        self.__msg = msg

    def to_json(self):
        return {"type": self.__class__.__name__, "error": self.__msg, "code": self.__code}
