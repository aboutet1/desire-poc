# -*- coding:utf-8 -*-
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from base64 import b64decode, b64encode
from util import to_b64, from_b64, random_int, token_hash, DesireBaseError

class AuthorizationTokenError(DesireBaseError):
    pass

# RSA certificate
with open("desire.pem", "rb") as f:
    rsa = RSA.import_key(f.read())

class AuthorizationToken:
    __LEN_R = 32

    # TODO: put this in a database
    __pin_table = {}

    @staticmethod
    def generate_pin(phone_number):
        h = SHA256.new(data = phone_number.encode()).hexdigest()

        # Reject phone number if pin has already been generated
        if h in AuthorizationToken.__pin_table:
            raise AuthorizationTokenError("PIN code already generated", 1)

        # Create new random PIN and add to table
        pin = to_b64(random_int(8), 8)
        AuthorizationToken.__pin_table[h] = (pin, False)

        return pin

    @staticmethod
    def use_pin(token):
        for h in AuthorizationToken.__pin_table:
            t = AuthorizationToken.__pin_table[h]
            if t[0] == token:
                if not t[1]:
                    AuthorizationToken.__pin_table[h] = (token, True)
                    return
                else:
                    raise AuthorizationTokenError("PIN code already used", 2)
        raise AuthorizationTokenError("PIN code not found", 3)

    @staticmethod
    def check_token(token):
        R = from_b64(token[0])
        sigma = from_b64(token[1])
        theoretical_sigma = pow(token_hash(R), rsa.d, rsa.n)
        
        if theoretical_sigma != sigma:
            raise AuthorizationTokenError("Unauthorized token", 4)

    @staticmethod
    def compute_secret(secret):
        b = from_b64(secret)
        rep = pow(b, rsa.d, rsa.n)
        return to_b64(rep, 256)

    @staticmethod
    def get_public_key():
        return (to_b64(rsa.e, 4), to_b64(rsa.n, 256))
