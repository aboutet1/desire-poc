# -*- coding:utf-8 -*-
""" Desire server implementation """

from flask import Flask
from flask_restful import Resource, Api
from desire_api_register import DesireApiRegister
from desire_api_report import DesireApiReport
from desire_api_status import DesireApiStatus
from desire_api_exposed import DesireApiExposed
from flask_swagger_ui import get_swaggerui_blueprint
from yaml import Loader, load

SWAGGER_URL = '/api/docs'
SWAGGER_PATH = 'swagger.yaml'

if __name__ == "__main__":
    app = Flask(__name__)

    swagger_yml = load(open(SWAGGER_PATH, 'r'), Loader=Loader)

    # Call factory function to create our blueprint
    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL,
        SWAGGER_PATH,
        config = {
            'app_name': "DESIRE server API",
            'spec': swagger_yml,
        },
    )
    app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

    api = Api(app)

    api.add_resource(DesireApiStatus, "/status")
    api.add_resource(DesireApiRegister, "/register")
    api.add_resource(DesireApiReport, "/report")
    api.add_resource(DesireApiExposed, "/exposed")

    app.run(debug=True, host='0.0.0.0', port=8080)
