# -*- coding:utf-8 -*-
from flask import request
from flask_restful import Resource
from authorization_token import AuthorizationTokenError
from status_request import StatusRequestError
from util import DesireBaseError

class ParametersError(DesireBaseError):
    pass

class DesireApiBase(Resource):
    def __init__(self):
        self.args = None

        super().__init__()

    def _check_parameters(self, l):
        if request.method == "GET":
            self.args = dict(request.args.lists())
            
            for a in self.args:
                if a in l:
                    if l[a] == list: 
                        self.args[a] = l[a](self.args[a])
                    else:
                        self.args[a] = l[a](self.args[a][0])
        else:
            self.args = request.get_json()

            if self.args is None:
                raise ParametersError("No JSON data in request", 1)

        for p in l:
            if p not in self.args:
                raise ParametersError("Parameter %s required in parameters" % p, 2)

            if type(self.args[p]) is not l[p]:
                raise ParametersError("Parameter %s should be of type %s" % (p, l[p]), 3)

        for p in self.args:
            if p not in l:
                raise ParametersError("Additional parameter %s unknown" % p, 4)

    def get(self):
        try:
            if not getattr(self, "api_get", False):
                return {"error": "Method not allowed"}, 405
            self._check_parameters(self.get_params)
            return self.api_get()
        except (AuthorizationTokenError, ParametersError, StatusRequestError) as err:
            return err.to_json(), 400
        except Exception as err:
            return {"error": str(err), "code": -1}, 500

    def post(self):
        try:
            if not getattr(self, "api_post", False):
                return {"error": "Method not allowed"}, 405
            self._check_parameters(self.post_params)
            return self.api_post()
        except (AuthorizationTokenError, ParametersError, StatusRequestError) as err:
            return err.to_json(), 400
        except Exception as err:
            return {"error": str(err), "code": -1}, 500

    def put(self):
        print("Right PUT")
        try:
            if not getattr(self, "api_put", False):
                return {"error": "Method not allowed"}, 405
            self._check_parameters(self.put_params)
            return self.api_put()
        except (AuthorizationTokenError, ParametersError, StatusRequestError) as err:
            return err.to_json(), 400
        except Exception as err:
            return {"error": str(err), "code": -1}, 500
