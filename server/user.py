# -*- coding:utf-8 -*-
from util import random_int, to_b64, from_b64
from datetime import datetime

class User:
    __id_table = {}

    def __init__(self, uid = None, key = None):
        # TODO avoid collision with ID generation
        self.id = to_b64(random_int(32), 32)
        self.un = False
        self.sre = datetime.fromtimestamp(0)
        self.lepm = []
        self.ers = 0.0

    def __encrypt(self, key):
        # TODO: do a proper encryption
        return self

    def encrypted_store(self, key = None):
        # TODO: generate proper encryption key
        if key is None:
            key = random_int(32)
        User.__id_table[self.id] = self.__encrypt(key)
        return to_b64(key, 32)

    @staticmethod
    def __decrypt(encrypted, key):
        # TODO: do a proper decryption
        return encrypted

    @staticmethod
    def get_user(uid, key):
        return User.__decrypt(User.__id_table[uid], key)

    def compute_risk(self):
        # TODO: do a proper scoring computation
        self.ers = 1.0 if self.un or (len(self.lepm) != 0) else 0.0

    def at_risk(self):
        # TODO: do a proper thresholding
        self.un = self.ers > 0.5

        return self.un
