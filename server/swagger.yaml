swagger: '2.0'
info:
  description: DESIRE Client API
  version: 1.0.0
  title: DESIRE protocol
  termsOfService: 'https://github.com/3rd-ways-for-EU-exposure-notification/project-DESIRE'
  contact:
    email: desire-contact@inria.fr
  license:
    name: '?'
    url: 'https://github.com/3rd-ways-for-EU-exposure-notification/project-DESIRE'
host: yourdesireserver.org
basePath: /v1
tags: []
schemes:
  - https
paths:
  /register:
    get:
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
      - name: "phone_number"
        in: "query"
        required: true
        type: "string"
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/RegisterGetResponse'
        "400":
          description: bad request
          schema:
            $ref: '#/definitions/BadRequestResponse'
        "500":
          description: server error
          schema:
            $ref: '#/definitions/ServerErrorResponse'
    post:
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: req
          schema:
            $ref: '#/definitions/RegisterPostRequest'
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/RegisterPostResponse'
        "400":
          description: bad request
          schema:
            $ref: '#/definitions/BadRequestResponse'
        "500":
          description: server error
          schema:
            $ref: '#/definitions/ServerErrorResponse'
    put:
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: req
          schema:
            $ref: '#/definitions/RegisterPutRequest'
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/RegisterPutResponse'
        "400":
          description: bad request
          schema:
            $ref: '#/definitions/BadRequestResponse'
        "500":
          description: server error
          schema:
            $ref: '#/definitions/ServerErrorResponse'
      security: []
  /status:
    get:
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
      - name: "id"
        in: "query"
        required: true
        type: "string"
      - name: "key"
        in: "query"
        required: true
        type: "string"
      - name: "pets"
        in: "query"
        required: true
        type: "string"
      responses:
        "200":
          description: "Successful operation"
          schema:
            $ref: '#/definitions/StatusGetResponse'
        "400":
          description: bad request
          schema:
            $ref: '#/definitions/BadRequestResponse'
        "500":
          description: server error
          schema:
            $ref: '#/definitions/ServerErrorResponse'
  /report:
    get:
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
      - name: "r"
        in: "query"
        required: true
        type: "string"
      - name: "sigma"
        in: "query"
        required: true
        type: "string"
      - name: "number_of_tokens"
        in: "query"
        required: true
        type: "string"
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/ReportGetResponse'
        "400":
          description: bad request
          schema:
            $ref: '#/definitions/BadRequestResponse'
        "500":
          description: server error
          schema:
            $ref: '#/definitions/ServerErrorResponse'
    post:
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: req
          schema:
            $ref: '#/definitions/ReportPostRequest'
      responses:
        '200':
          description: "Successful operation"
        "400":
          description: bad request
          schema:
            $ref: '#/definitions/BadRequestResponse'
        "500":
          description: server error
          schema:
            $ref: '#/definitions/ServerErrorResponse'
  /exposed:
    get:
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
      - name: "r"
        in: "query"
        required: true
        type: "string"
      - name: "sigma"
        in: "query"
        required: true
        type: "string"
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/ExposedGetResponse'
        "400":
          description: bad request
          schema:
            $ref: '#/definitions/BadRequestResponse'
        "500":
          description: server error
          schema:
            $ref: '#/definitions/ServerErrorResponse'
securityDefinitions: {}
definitions:
  RegisterPostRequest:
    type: object
    properties:
      pin:
        type: string
      secret:
        type: string
    required:
      - pin
      - secret
  RegisterPutRequest:
    type: object
    properties:
      r:
        type: string
      sigma:
        type: string
    required:
      - r
      - sigma
  KeyList:
    type: array
    items:
      type: string
  RegisterGetResponse:
    type: object
    properties:
      pin:
        type: string
        format: byte
        description: Base64 encoded
      public_key:
         $ref: '#/definitions/KeyList'
  RegisterPostResponse:
    type: object
    properties:
      rep:
        type: string
  RegisterPutResponse:
    type: object
    properties:
      id:
        type: string
      key:
        type: string
  StatusGetResponse:
    type: object
    properties:
      at_risk:
        type: boolean
    required:
      - at_risk
  ReportGetResponse:
    type: object
    properties:
      tokens:
         $ref: '#/definitions/KeyList'
  ReportPostRequest:
    type: object
    properties:
      token:
        type: string
      pet:
        type: string
      datetime:
        type: integer
    required:
      - token
      - pet
      - datetime
  ExposedGetResponse:
    type: object
    properties:
      exposed:
         $ref: '#/definitions/KeyList'
  BadRequestResponse:
    type: object
    properties:
      type:
        type: string
      error:
        type: string
      code:
        type: integer
  ServerErrorResponse:
    type: object
    properties:
      error:
        type: string
      code:
        type: integer
externalDocs:
  description: Find out more about DESIRE
  url: 'https://github.com/3rd-ways-for-EU-exposure-notification/project-DESIRE'
