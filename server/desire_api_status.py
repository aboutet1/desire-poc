# -*- coding:utf-8 -*-
from user import User
from desire_api_base import DesireApiBase
from status_request import StatusRequest
from exposure_list import ExposureList
from util import from_b64

class DesireApiStatus(DesireApiBase):
    def __init__(self):
        self.get_params = {"id": str, "key": str, "pets": list}

    def api_get(self):
        """ Get status """

        uid = self.args["id"]
        key = from_b64(self.args["key"])
        pets = self.args["pets"]

        user = User.get_user(uid, key)

        StatusRequest.check_request(user)    

        for pet in pets:
            exposure = ExposureList.check_exposure(pet)
            if len(exposure) != 0:
                user.lepm.append((pet, exposure))

        user.compute_risk()

        at_risk = user.at_risk()

        user.encrypted_store(key)

        return {"at_risk": at_risk}

