# -*- coding:utf-8 -*-
from util import to_b64, random_int

class ExposureTokenError(Exception):
    def __init__(msg, code):
        self.__code = code
        self.__msg = msg

    def to_json():
        return {"type": self.__class__.__name__, "error": self.__msg, "code": code}

class ExposureList:
    __elist = []

    @staticmethod
    def add_to_list(pet, d):
        ExposureList.__elist.append((pet, d))

    @staticmethod
    def check_exposure(pet):
        ret = []

        for p in ExposureList.__elist:
            if p[0] == pet:
                ExposureList.__elist.remove(p)
                ret.append(p[1])

        return ret

    @staticmethod
    def export_list():
        ret = []

        for p in ExposureList.__elist:
            ret.append(p[0])

        return ret

class ExposureToken:
    __generated_tokens = {}

    @staticmethod
    def get_tokens(n):
        ret = [to_b64(random_int(8), 8) for i in range(n)]

        for i in range(n):
            while ret[i] in ExposureToken.__generated_tokens:
                ret[i] = to_b64(random_int(8), 8)
            ExposureToken.__generated_tokens[ret[i]] = False

        return ret

    @staticmethod
    def use_token(t):
        if t not in ExposureToken.__generated_tokens:
            raise ExposureTokenError("Token does not exist")
        else:
            if ExposureToken.__generated_tokens[t]:
                raise ExposureTokenError("Token already used")
            else:
                ExposureToken.__generated_tokens[t] = True
