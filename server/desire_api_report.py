# -*- coding:utf-8 -*-
from desire_api_base import DesireApiBase
from authorization_token import AuthorizationToken, AuthorizationTokenError
from exposure_list import ExposureToken, ExposureList
from datetime import datetime

class DesireApiReport(DesireApiBase):
    def __init__(self):
        self.get_params = {"r": str, "sigma": str, "number_of_tokens": int}
        self.post_params = {"token": str, "pet": str, "datetime": int}

    def api_get(self):
        """ Get tokens to report exposure """
        AuthorizationToken.check_token([self.args["r"], self.args["sigma"]])

        return {"tokens": ExposureToken.get_tokens(self.args["number_of_tokens"])}

    def api_post(self):
        """ Report token exposure """
        ExposureToken.use_token(self.args["token"])
        ExposureList.add_to_list(self.args["pet"], datetime.fromtimestamp(self.args["datetime"]))

        return {}
