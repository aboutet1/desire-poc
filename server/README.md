# StopCovid DESIRE server implementation

## Requirements

* Python3

* Docker (optional for testing)
  * sudo apt-get install docker.io 

* VirutalEnv (optional)
  * sudo apt-get install python3-venv

* Python packages
  * pip3 install -r requirements.txt

## Create RSA keys

Before starting the server we need to generate a pair of RSA keys for the server, it is done using `gen_rsa_key.py`. This script creates a key file `desire.pem` containing the public and private keys.

```bash
python3 gen_rsa_key.py
```

## Start the server manually

The server is launched using `app.py` script. It provides a REST API on port 8080

```bash
python3 app.py
```

## Testing

1. Before testing

Pytest framework is used for testing, before trying to launch test scripts, you must install it.

```bash
pip3 install pytest
```

2. REST API tests
  * Register procedure can be tested using the `test_register.py` script. This script can be launched using the following command:

```bash
python3 -m pytest test_register.py
```
3. Unit tests

```bash
python3 -m pytest test_authorization_token.py
```

## REST API docs

The REST API documentation is available at the following address:

* http://0.0.0.0:8080/api/docs

It is displayed with Swagger-UI and the specification file `swagger.json` is located at the root
path of the project.

## Build Docker image

```bash
docker build -t desire-server:latest .
```

## Run Docker image

``` bash
docker run -d -p 8080:8080 desire-server
docker container ls
curl http://0.0.0.0:8080/register
docker container kill <container_id>
```
