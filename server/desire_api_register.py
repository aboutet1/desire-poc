# -*- coding:utf-8 -*-
from desire_api_base import DesireApiBase
from authorization_token import AuthorizationToken, AuthorizationTokenError
from user import User

class DesireApiRegister(DesireApiBase):
    def __init__(self):
        self.get_params = {"phone_number": str}
        self.post_params = {"pin": str, "secret": str}
        self.put_params = {"r": str, "sigma": str}

    def api_get(self):
        """ Get registration PIN """
        pin = AuthorizationToken.generate_pin(self.args["phone_number"])
        (e, n) = AuthorizationToken.get_public_key()

        # TODO: should not return pin as a response, it should be sent by SMS
        return {"pin": pin, "public_key": [e, n]}

    def api_post(self):
        """ Create authorization token """
        AuthorizationToken.use_pin(self.args["pin"])
        rep = AuthorizationToken.compute_secret(self.args["secret"])

        return {"rep": rep}

    def api_put(self):
        """ Create effectively a new user """
        AuthorizationToken.check_token([self.args["r"], self.args["sigma"]])

        # Create a new user
        user = User()
        # Store encrypted user data
        key = user.encrypted_store()
        return {"id": user.id, "key": key}
