# -*- coding:utf-8 -*-
import pytest
import requests
from util import to_b64, from_b64, gen_secret

def create_user(host, phone_number):
    r = requests.get(host + "register", params = {"phone_number": phone_number})
    assert(r.status_code == 200)

    pin = r.json()["pin"]
    [public_key_e, public_key_n] = r.json()["public_key"]
    public_key_e = from_b64(public_key_e)
    public_key_n = from_b64(public_key_n)
    
    (secret_r, secret_c, inv_secret_c, secret) = gen_secret(public_key_e, public_key_n)

    r = requests.post(host + "register", json = {"pin": pin, "secret": to_b64(secret, 256)})
    assert(r.status_code == 200)
    
    rep = from_b64(r.json()["rep"])

    secret_r = to_b64(secret_r, 256)
    secret_sigma = to_b64((rep * inv_secret_c) % public_key_n, 256)

    r = requests.put(host + "register", json = {"r": secret_r, "sigma": secret_sigma})
    assert(r.status_code == 200)

    return (r.json()["id"], r.json()["key"], secret_r, secret_sigma)

