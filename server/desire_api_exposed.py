# -*- coding:utf-8 -*-
from desire_api_base import DesireApiBase
from authorization_token import AuthorizationToken, AuthorizationTokenError
from exposure_list import ExposureToken, ExposureList

class DesireApiExposed(DesireApiBase):
    def __init__(self):
        self.get_params = {"r": str, "sigma": str}

    def api_get(self):
        """ Get exposed PETs """
        AuthorizationToken.check_token([self.args["r"], self.args["sigma"]])

        return {"exposed": ExposureList.export_list()}
