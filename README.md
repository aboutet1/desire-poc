This repository contains the prof of concept of Desire as described in the following paper:

@article{10.1145/3480467,
author = {boutet, antoine and Castelluccia, Claude and Cunche, Mathieu and Lauradoux, C\'{e}dric and Roca, Vincent and Baud, Adrien and Raverdy, Pierre-Guillaume},
title = {DESIRE: Leveraging the Best of Centralized and Decentralized Contact Tracing Systems},
year = {2021},
journal = {Digital Threats: Research and Practice},
month = {aug}
}


