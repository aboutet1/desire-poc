package fr.inria.desire.ble

import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.PEAK_THRESHOLD
import fr.inria.desire.ble.models.RTLWindow
import fr.inria.desire.ble.models.RTLWindows
import org.junit.Assert
import org.junit.Test
import org.junit.Assert.*

/**
 */
class RTLWindowsUnitTest {

    @ExperimentalUnsignedTypes
    @Test
    fun fading_isCorrect() {
        {
            // test cannot do fading without values
            val wins: RTLWindows = RTLWindows()

            val win1: RTLWindow = RTLWindow()
            win1.values.add(PEAK_THRESHOLD)

            val win2: RTLWindow = RTLWindow()
            win2.values.add(PEAK_THRESHOLD)
            win2.values.add(PEAK_THRESHOLD + 2)

            val win3: RTLWindow = RTLWindow()
            win3.values.add(PEAK_THRESHOLD)

            val win4: RTLWindow = RTLWindow()

            wins.values.add(win1)
            wins.values.add(win2)
            wins.values.add(win3)
            wins.values.add(win4)

            try {
                val faded = wins.fading()
                assertTrue(faded.qArray[0] == PEAK_THRESHOLD)
                assertTrue(faded.qArray[1] == (PEAK_THRESHOLD + 1))
                assertTrue(faded.qArray[2] == PEAK_THRESHOLD)
                assertTrue(faded.qArray[3] == null)
            } catch (e: Exception) {
                fail()
            }
        }
    }

    @ExperimentalUnsignedTypes
    @Test
    fun cliping_isCorrect() {
        {
            // test cannot do fading without values
            val wins: RTLWindows = RTLWindows()

            val win1: RTLWindow = RTLWindow()
            win1.values.add(PEAK_THRESHOLD)

            val win2: RTLWindow = RTLWindow()
            win2.values.add(PEAK_THRESHOLD)
            win2.values.add(PEAK_THRESHOLD + 2)

            val win3: RTLWindow = RTLWindow()
            win3.values.add(PEAK_THRESHOLD)

            val win4: RTLWindow = RTLWindow()

            wins.values.add(win1)
            wins.values.add(win2)
            wins.values.add(win3)
            wins.values.add(win4)

            try {
                wins.clipping()

                val faded = wins.fading()
                assertTrue(faded.qArray[0] == PEAK_THRESHOLD)
                assertTrue(faded.qArray[1] == PEAK_THRESHOLD)
                assertTrue(faded.qArray[2] == PEAK_THRESHOLD)
                assertTrue(faded.qArray[3] == null)
            } catch (e: Exception) {
                fail()
            }
        }
    }
}
