package fr.inria.desire.ble

import fr.inria.desire.ble.models.RTLData
import fr.inria.desire.ble.models.RTLItem
import org.junit.Test
import org.junit.Assert.*

/**
 */
class RTLDataUnitTest {

    @ExperimentalUnsignedTypes
    @Test
    fun split_empty_isCorrect() {
        val rtldata: RTLData = RTLData()
        rtldata.startTimestamp = 0


        // add to window 1
        val item1: RTLItem = RTLItem()
        item1.rssi = 10
        item1.timestamp = 5000
        rtldata.data.add(item1)

        // add to window 1
        val item2: RTLItem = RTLItem()
        item2.rssi = 20
        item2.timestamp = 6000
        rtldata.data.add(item2)

        // add to window 1 and 2
        val item3: RTLItem = RTLItem()
        item3.rssi = 30
        item3.timestamp = 60 * 1000 + 5000
        rtldata.data.add(item3)

        assertFalse(rtldata.isLongEnough())

        // add to window 14 and 15
        val item4: RTLItem = RTLItem()
        item4.rssi = 10
        item4.timestamp = 14 * 60 * 1000 + 5000
        rtldata.data.add(item4)

        assertTrue(rtldata.isLongEnough())

        val wins = rtldata.split()

        assertTrue(wins.values[0].values.size == 3)
        assertTrue(wins.values[1].values.size == 1)
        assertTrue(wins.values[13].values.size == 1)
        assertTrue(wins.values[14].values.size == 1)
    }
}
