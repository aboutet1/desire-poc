package fr.inria.desire.ble

import fr.inria.desire.ble.settings.ScannerSettings
import org.junit.Test

import org.junit.Assert.*

class ScannerSettingsUnitTest {
    @Test
    fun nearbyDevices_isCorrect() {

        assertEquals(0, ScannerSettings.nearbyDevicesCount())


        ScannerSettings.addDevice("dev1")
        assertEquals(1, ScannerSettings.nearbyDevicesCount())

        ScannerSettings.addDevice("dev2")
        assertEquals(2, ScannerSettings.nearbyDevicesCount())

        ScannerSettings.addDevice("dev2")
        assertEquals(2, ScannerSettings.nearbyDevicesCount())

        ScannerSettings.addDevice("dev3")
        assertEquals(3, ScannerSettings.nearbyDevicesCount())

        Thread.sleep(30000)
        assertEquals(3, ScannerSettings.nearbyDevicesCount())

        ScannerSettings.addDevice("dev4")
        assertEquals(4, ScannerSettings.nearbyDevicesCount())

        Thread.sleep(45000)
        assertEquals(1, ScannerSettings.nearbyDevicesCount())

    }
}
