package fr.inria.desire.ble

import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.models.EBID
import org.junit.Assert
import org.junit.Test
import org.junit.Assert.*

/**
 */
class EBIDUnitTest {

    @ExperimentalUnsignedTypes
    @Test
    fun ebid_isCorrect() {

        val ebid_slice1_long: UByteArray? = ubyteArrayOf(
            0x20u,
            0x21u,
            0x22u,
            0x23u,
            0x24u,
            0x25u,
            0x26u,
            0x27u,
            0x28u,
            0x29u,
            0x20u,
            0x2eu
        )
        val ebid_slice2_long: UByteArray? = ubyteArrayOf(
            0x10u,
            0x11u,
            0x12u,
            0x13u,
            0x14u,
            0x15u,
            0x16u,
            0x17u,
            0x18u,
            0x19u,
            0x20u,
            0xfeu
        )
        val ebid_slice3_short: UByteArray? =
            ubyteArrayOf(0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u)

        val ebidarray: UByteArray = ubyteArrayOf(
            0x20u, 0x21u, 0x22u, 0x23u, 0x24u, 0x25u, 0x26u, 0x27u, 0x28u, 0x29u, 0x20u, 0x2eu,
            0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u, 0x18u, 0x19u, 0x20u, 0xfeu,
            0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u
        )

        val ebid: EBID = EBID.createFromUByteArray(ebidarray)

        val part1: UByteArray? = ebid.getPart(1)
        val part2: UByteArray? = ebid.getPart(2)
        // need to take only the first 8 bytes
        val part3: UByteArray? = ebid.getPart(3)!!.copyOfRange(0, 8)

        // must check equality of the content. Issue with extra space when using assertEquals directly
        part1?.let { ebid_slice1_long!!.contentEquals(it) }?.let { assertTrue(it) }
        part2?.let { ebid_slice2_long!!.contentEquals(it) }?.let { assertTrue(it) }
        part3?.let { ebid_slice3_short!!.contentEquals(it) }?.let { assertTrue(it) }


        val reconstruct: EBID = EBID()
        reconstruct.add(1, part1)
        reconstruct.add(2, part2)
        reconstruct.add(3, part3)

        val res = reconstruct.reconstruct()
        assertNotNull(res)
        assertEquals(reconstruct.toString().trim(), ebid.toString().trim())

    }

    @ExperimentalUnsignedTypes
    @Test
    fun ebid_checkReconstructXor() {
        val ebid_slice1_long: UByteArray? = ubyteArrayOf(
            0x20u,
            0x21u,
            0x22u,
            0x23u,
            0x24u,
            0x25u,
            0x26u,
            0x27u,
            0x28u,
            0x29u,
            0x20u,
            0x2eu
        )
        val ebid_slice2_long: UByteArray? = ubyteArrayOf(
            0x10u,
            0x11u,
            0x12u,
            0x13u,
            0x14u,
            0x15u,
            0x16u,
            0x17u,
            0x18u,
            0x19u,
            0x20u,
            0xfeu
        )
        val ebid_slice3_short: UByteArray? =
            ubyteArrayOf(0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u)

        val ebidarray: UByteArray = ubyteArrayOf(
            0x20u, 0x21u, 0x22u, 0x23u, 0x24u, 0x25u, 0x26u, 0x27u, 0x28u, 0x29u, 0x20u, 0x2eu,
            0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u, 0x18u, 0x19u, 0x20u, 0xfeu,
            0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u
        )

        val tmpEbid: EBID = EBID.createFromUByteArray(ebidarray)


        val tmpEbid2 = EBID()
        tmpEbid2.add(1, tmpEbid.getPart(1))
        tmpEbid2.add(2, tmpEbid.getPart(2))
        tmpEbid2.add(3, null)
        tmpEbid2.add(4, null)
        try {
            val ebid = tmpEbid2.reconstruct()
            assertNull(ebid)
        } catch (e: ArrayIndexOutOfBoundsException) {
        } catch (e: KotlinNullPointerException) {
        }

        val tmpEbid3 = EBID()
        tmpEbid3.add(1, tmpEbid.getPart(1))
        tmpEbid3.add(2, tmpEbid.getPart(2))
        tmpEbid3.add(4, tmpEbid.getPart(4))
        try {
            val ebid = tmpEbid3.reconstruct()
            assertNotNull(ebid)
        } catch (e: ArrayIndexOutOfBoundsException) {
            fail("exception ArrayIndexOutOfBoundsException")
        } catch (e: KotlinNullPointerException) {
            fail("exception KotlinNullPointerException")
        }

        val tmpEbid4 = EBID()
        tmpEbid4.add(1, tmpEbid.getPart(1))
        tmpEbid4.add(3, tmpEbid.getPart(3))
        tmpEbid4.add(4, tmpEbid.getPart(4))
        try {
            val ebid = tmpEbid4.reconstruct()
            assertNotNull(ebid)
        } catch (e: ArrayIndexOutOfBoundsException) {
            fail("exception ArrayIndexOutOfBoundsException")
        } catch (e: KotlinNullPointerException) {
            fail("exception KotlinNullPointerException")
        }

        val tmpEbid1 = EBID()
        tmpEbid1.add(1, tmpEbid.getPart(1))
        tmpEbid1.add(2, tmpEbid.getPart(2))
        tmpEbid1.add(3, tmpEbid.getPart(3))
        tmpEbid3.add(4, null)
        try {
            val ebid = tmpEbid3.reconstruct()
            assertNotNull(ebid)
        } catch (e: ArrayIndexOutOfBoundsException) {
            fail("exception ArrayIndexOutOfBoundsException")
        } catch (e: KotlinNullPointerException) {
            fail("exception KotlinNullPointerException")
        }
    }
}
