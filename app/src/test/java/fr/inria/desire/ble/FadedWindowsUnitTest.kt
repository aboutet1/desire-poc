package fr.inria.desire.ble

import fr.inria.desire.ble.models.FadedWindows
import org.junit.Test
import org.junit.Assert.*
import java.util.*

@ExperimentalUnsignedTypes
class FadedWindowsUnitTest {


    @Test
    fun FadedWindows_creation() {
        val fw_a = FadedWindows()
        assertEquals(0, fw_a.nArray.size)
        assertEquals(0, fw_a.qArray.size)

        fw_a.add(-50, 1u, 1000)
        assertEquals(-50, fw_a.qArray.last())
        assertEquals(1u, fw_a.nArray.last())

        fw_a.add(-40, 2u, 2000)
        fw_a.add(-30, 3u, 3000)
        fw_a.add(-20, 4u, 4000)
        fw_a.add(-10, 5u, 5000)

        assertEquals(5, fw_a.qArray.size)
        assertEquals(5, fw_a.nArray.size)

        val fw_b = FadedWindows(fw_a.qArray, fw_a.nArray, fw_a.tsArray)

        assertEquals(5, fw_b.qArray.size)
        assertEquals(5, fw_b.nArray.size)
        assertEquals(-10, fw_b.qArray.last())
        assertEquals(5u, fw_b.nArray.last())


    }

    @Test
    fun FadedWindows_risk() {
        val fw_a = FadedWindows()
        fw_a.add(-50, 1u, 1000)
        fw_a.add(-40, 2u, 2000)
        fw_a.add(-30, 3u, 3000)
        fw_a.add(-20, 4u, 4000)
        fw_a.add(-10, 5u, 5000)

        val rw = fw_a.risk()
        assertEquals((-50 - fw_a.p0) / fw_a.deltaP[0], rw.pi[0])

    }
}