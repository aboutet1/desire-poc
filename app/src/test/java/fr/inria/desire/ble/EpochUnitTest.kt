package fr.inria.desire.ble

import fr.inria.desire.ble.models.Epoch
import org.junit.Test
import org.junit.Assert.*

class EpochUnitTest {

    @Test
    fun epoch_creation() {
        val startTs: Long = 1000
        val endTs: Long = 10000
        val epoch = Epoch(startTs, endTs)
        assertEquals(startTs, epoch.startTimestamp)
        assertEquals(endTs, epoch.endTimestamp)


    }
}