package fr.inria.desire.ble

import com.n1analytics.paillier.PaillierPrivateKey
import com.n1analytics.paillier.PaillierPublicKey
import com.n1analytics.paillier.cli.PrivateKeyJsonSerialiser
import org.apache.commons.codec.binary.Base64
import org.json.JSONObject
import org.junit.Assert.assertTrue
import org.junit.Test
import java.math.BigInteger
import java.util.*


/**
 */
class JavallierUnitTest {

    @Test
    fun javallier_long_sum_isCorrect() {
        // sum of values should be 49
        val rawNumbers = longArrayOf(0, 0, 1, 3, -5, 50)

        val keypair = PaillierPrivateKey.create(1024)
        val publicKey = keypair.getPublicKey()

        val paillierContext = publicKey.createSignedContext()

        println("Encrypting doubles with public key (e.g., on multiple devices)")

        val encryptedNumbers = rawNumbers.map({ n -> paillierContext.encrypt(n) })

        println("Adding encrypted doubles")

        val encryptedSum = encryptedNumbers.reduce({ acc, n -> acc.add(n) })

        println("Decrypting result:")
        println(keypair.decrypt(encryptedSum).decodeLong())

        val expected: Long = 49
        assertTrue(expected == keypair.decrypt(encryptedSum).decodeLong())
    }

    @Test
    fun javallier_serial_isCorrect() {

        val keypair = PaillierPrivateKey.create(1024)

        // serialise the keypart in json and get the part for the public key
        val serializedPrivateKey = PrivateKeyJsonSerialiser("Desire metadata key")

        keypair.serialize(serializedPrivateKey)

        println("Serialized priv key :  " + serializedPrivateKey.toString())
        val jsonstr = serializedPrivateKey.toString()
        val jsonObj: JSONObject = JSONObject(jsonstr)
        val pubJson = jsonObj.getJSONObject("pub")
        val pubKey = pubJson.getString("n")

        val n = BigInteger(1, Base64.decodeBase64(pubKey as String))

        // recreate the public key from the json encoded one
        val newpubkey = PaillierPublicKey(n)

        // verify that values encoded with new public key can be decoded with priv key
        val rawNumbers = longArrayOf(0, 0, 1, 3, -5, 50)
        val paillierContext = newpubkey.createSignedContext()
        val encryptedNumber = paillierContext.encrypt(50)
        val encryptedNumbers = rawNumbers.map({ n -> paillierContext.encrypt(n) })
        val encryptedSum = encryptedNumbers.reduce({ acc, n -> acc.add(n) })
        println(keypair.decrypt(encryptedSum).decodeLong())
        val expected: Long = 49
        assertTrue(expected == keypair.decrypt(encryptedSum).decodeLong())
    }

    @Test
    fun encode_same_isDifferent() {
        // sum of values should be 49
        val rawNumbers = longArrayOf(0, 0, 1, 3, -5, 50)
        val rawNumbers2 = longArrayOf(0, 0, 1, 3, -5, 50)

        val keypair = PaillierPrivateKey.create(1024)
        val publicKey = keypair.getPublicKey()

        val paillierContext = publicKey.createSignedContext()

        println("Encrypting doubles with public key (e.g., on multiple devices)")

        val encryptedNumbers = rawNumbers.map({ n -> paillierContext.encrypt(n) })
        val encryptedNumbers2 = rawNumbers2.map({ n -> paillierContext.encrypt(n) })

        for (x in 0..(rawNumbers.size - 1)) {
            if (rawNumbers[x].equals(rawNumbers2[x])) {
                println("same")
            } else {
                println("different")
            }
        }
    }

}
