package fr.inria.desire.ble

import fr.inria.desire.ble.models.RiskWindow
import org.junit.Test
import org.junit.Assert.*
import java.util.*

class RiskWindowUnitTest {

    @Test
    fun riskWindow_creation() {
        var piValues = floatArrayOf(0f, 0.1f, 0.2f, 0.314f, 0.8f)
        val rw = RiskWindow(ArrayList(piValues.asList()))
        assertEquals(5, rw.pi.size)
        assertEquals(0.8f, rw.pi.last())
    }

    @Test
    fun riskWindow_add() {
        val rw = RiskWindow()
        assertEquals(0, rw.pi.size)

        rw.add(0f)
        assertEquals(1, rw.pi.size)
        assertEquals(0f, rw.pi.last())

        rw.add(0.1f)
        assertEquals(2, rw.pi.size)
        assertEquals(0.1f, rw.pi.last())
    }
}