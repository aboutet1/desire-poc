package fr.inria.desire.ble

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

@ExperimentalUnsignedTypes
class XorUnitTest {

    @Test
    fun xor_2bytes_isCorrect() {
        val byte1: UByte = 0x34u
        val byte2: UByte = 0xd3u

        val byteXor = byte1 xor byte2

        // test can recover byte1
        val byte1xor = byte2 xor byteXor
        assertTrue(byte1 == byte1xor)

        // test can recover byte2
        val byte2xor = byte1 xor byteXor
        assertTrue(byte2 == byte2xor)

        // test order of xor not an issue
        val byteXor2 = byte2 xor byte1
        assertTrue(byteXor2 == byteXor)
    }

    @Test
    fun xor_3bytes_isCorrect() {
        val byte1: UByte = 0x34u
        val byte2: UByte = 0xd3u
        val byte3: UByte = 0xf1u

        val byteXor = byte1 xor byte2 xor byte3

        // test can recover byte1
        var byte1xor = byte2 xor byte3 xor byteXor
        assertTrue(byte1 == byte1xor)

        // test order of xor not an issue
        byte1xor = byte3 xor byte2 xor byteXor
        assertTrue(byte1 == byte1xor)

        // test incomplete fails
        byte1xor = byte3 xor byte2
        assertFalse(byte1 == byte1xor)

        val byteXor2 = byte3 xor byte2 xor byte1
        assertTrue(byteXor2 == byteXor)
    }


    @Test
    fun xor_array_isCorrect() {
        val arr1: UByteArray = ubyteArrayOf(0x35u, 0x23u, 0xcFu, 0xddu)
        val arr2: UByteArray = ubyteArrayOf(0x35u, 0x23u, 0xcFu, 0xddu)
        val arr3: UByteArray = ubyteArrayOf(0x35u, 0x23u, 0xcFu, 0xddu)


        val arrayXor = arr1 xor arr2 xor arr3

        // test can recover byte1
        val arr1xor = arr2 xor arr3 xor arrayXor
        println("arr1 " + arr1.toString())
        println("arr1xor " + arr1xor.toString())

        assertTrue(arr1.contentEquals(arr1xor))

    }
}

@ExperimentalUnsignedTypes
private infix fun UByteArray.xor(arr2: UByteArray): UByteArray {
    var i = 0
    val res: UByteArray = UByteArray(this.size)
    for (b in this) res[i] = this[i] xor arr2[i++]
    return res
}
