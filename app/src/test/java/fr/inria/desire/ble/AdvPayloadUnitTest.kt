package fr.inria.desire.ble

import fr.inria.desire.ble.models.AdvPayload
import fr.inria.desire.ble.settings.ScannerSettings
import org.junit.Test

import org.junit.Assert.*

class AdvPayloadUnitTest {
    @Test
    fun advPayload_isCorrect() {

        val sid: UInt = 0u
        val cid: UInt = 18u
        val ebid_slice_long: UByteArray = ubyteArrayOf(
            0x10u,
            0x11u,
            0x12u,
            0x13u,
            0x14u,
            0x15u,
            0x16u,
            0x17u,
            0x18u,
            0x19u,
            0x20u,
            0xfeu
        )
        val md_version: UInt = 0xC8u
        val md_gtx: Int = -23

        val payload1 = AdvPayload(
            sid = sid,
            cid = cid,
            ebid_slice = ebid_slice_long,
            md_version = md_version
        )

        val pair = payload1.toUByteArrays()

        val payload2 = AdvPayload.from(pair.first, pair.second)

        assertEquals(payload1.sid, payload2.sid)
        assertEquals(payload1.cid, payload2.cid)
        assertEquals(payload1.md_version, payload2.md_version)
        assertEquals(payload1.ebid_slice.toString().trim(), payload2.ebid_slice.toString().trim())

        assertEquals(payload1.sid, sid)
        assertEquals(payload1.cid, cid)
        assertEquals(payload1.md_version, md_version)
        assertEquals(payload1.ebid_slice.toString().trim(), ebid_slice_long.toString().trim())


        val first: UByteArray = ubyteArrayOf(
            16u,
            16u,
            16u,
            16u,
            16u,
            17u,
            18u,
            19u,
            20u,
            21u,
            22u,
            23u,
            24u,
            25u,
            32u,
            (-2).toUByte()
        )
        val second: UByteArray = ubyteArrayOf(16u, 0u, 0u, 0u)
        val payload3 = AdvPayload.from(first, second)
        val pair2 = payload3.toUByteArrays()

        assertEquals(first.toString().trim(), pair2.first.toString().trim())
        assertEquals(second.toString().trim(), pair2.second.toString().trim())

        val payload4 = AdvPayload.from(pair2.first, pair2.second)

        assertEquals(payload3.sid, payload4.sid)
        assertEquals(payload3.cid, payload4.cid)
        assertEquals(payload3.md_version, payload4.md_version)
        assertEquals(payload3.ebid_slice.toString().trim(), payload4.ebid_slice.toString().trim())
    }


    @Test
    fun advPayloadShort_isCorrect() {

        val sid: UInt = 2u
        val cid: UInt = 18u
        val ebid_slice_short: UByteArray = ubyteArrayOf(
            0x14u,
            0x15u,
            0x16u,
            0x17u,
            0x18u,
            0x19u,
            0x20u,
            0xfeu
        )
        val md_version: UInt = 0xC8u

        val payload1 = AdvPayload(
            sid = sid,
            cid = cid,
            ebid_slice = ebid_slice_short,
            md_version = md_version
        )

        val pair = payload1.toUByteArrays()

        val payload2 = AdvPayload.from(pair.first, pair.second)

        assertEquals(payload1.sid, payload2.sid)
        assertEquals(payload1.cid, payload2.cid)
        assertEquals(payload1.md_version, payload2.md_version)
        assertEquals(payload1.ebid_slice.toString().trim(), payload2.ebid_slice.toString().trim())

        assertEquals(payload1.sid, sid)
        assertEquals(payload1.cid, cid)
        assertEquals(payload1.md_version, md_version)
        assertEquals(payload1.ebid_slice.toString().trim(), ebid_slice_short.toString().trim())
    }
}
