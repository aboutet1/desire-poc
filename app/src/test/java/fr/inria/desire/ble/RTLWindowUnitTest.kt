package fr.inria.desire.ble

import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.PEAK_THRESHOLD
import fr.inria.desire.ble.models.RTLWindow
import org.junit.Assert
import org.junit.Test
import org.junit.Assert.*

/**
 */
class RTLWindowUnitTest {

    @ExperimentalUnsignedTypes
    @Test
    fun clip_isCorrect() {
        {
            // test cannot do fading without values
            val win: RTLWindow = RTLWindow()
            try {
                win.fading()
                fail()
            } catch (e: Exception) {
            }
        }

        {
            // test cannot do fading without values
            val win: RTLWindow = RTLWindow()
            win.values.add(PEAK_THRESHOLD)
            try {
                val pair1 = win.fading()
                win.clipping()
                val pair2 = win.fading()

                assertTrue(pair1.first == PEAK_THRESHOLD)
                assertTrue(pair2.first == PEAK_THRESHOLD)
                assertTrue(pair2.second == 1)
            } catch (e: Exception) {
                fail()
            }
        }

        {
            // test cannot do fading without values
            val win: RTLWindow = RTLWindow()
            win.values.add(PEAK_THRESHOLD)
            win.values.add(PEAK_THRESHOLD + 2)
            try {
                val pair1 = win.fading()
                win.clipping()
                val pair2 = win.fading()

                assertTrue(pair1.first == (PEAK_THRESHOLD + 1))
                assertTrue(pair1.second == 2)

                assertTrue(pair2.first == PEAK_THRESHOLD)
                assertTrue(pair2.second == 2)
            } catch (e: Exception) {
                fail()
            }
        }
    }
}
