package fr.inria.desire.ble

import fr.inria.desire.ble.models.AdvData
import fr.inria.desire.ble.models.AdvPayload
import fr.inria.desire.ble.models.CID
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.ScannerSettings
import org.junit.Test

import org.junit.Assert.*

@ExperimentalUnsignedTypes
class AdvDataUnitTest {

    @Test
    fun advData_isCorrect() {

        val sid: UInt = 0u
        val cid: CID = CID()
        cid.value = 18u

        AdvertiserSettings.tx_bias = -5
        AdvertiserSettings.rx_bias = 23

        val ebid_slice1_long: UByteArray = ubyteArrayOf(
            0x10u,
            0x11u,
            0x12u,
            0x13u,
            0x14u,
            0x15u,
            0x16u,
            0x17u,
            0x18u,
            0x19u,
            0x20u,
            0xfeu
        )
        val ebid_slice2_long: UByteArray = ubyteArrayOf(
            0x10u,
            0x11u,
            0x12u,
            0x13u,
            0x14u,
            0x15u,
            0x16u,
            0x17u,
            0x18u,
            0x19u,
            0x20u,
            0xfeu
        )
        val ebid_slice3_short: UByteArray =
            ubyteArrayOf(0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u)
        val md_version: UInt = Constants.METADATA_VERSION
        val md_gtx: Int = AdvertiserSettings.tx_bias!!

        val ebidarray: UByteArray = ubyteArrayOf(
            0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u, 0x18u, 0x19u, 0x20u, 0xfeu,
            0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u, 0x18u, 0x19u, 0x20u, 0xfeu,
            0x10u, 0x11u, 0x12u, 0x13u, 0x14u, 0x15u, 0x16u, 0x17u
        )

        val ebid: EBID = EBID.createFromUByteArray(ebidarray)

        val payload1 = AdvPayload(
            sid = 0u,
            cid = cid.value,
            ebid_slice = ebid_slice1_long,
            md_version = md_version
        )

        val payload2 = AdvPayload(
            sid = 1u,
            cid = cid.value,
            ebid_slice = ebid_slice2_long,
            md_version = md_version
        )

        val payload3 = AdvPayload(
            sid = 2u,
            cid = cid.value,
            ebid_slice = ebid_slice3_short,
            md_version = md_version
        )

        val pair1 = payload1.toUByteArrays()
        val pair2 = payload2.toUByteArrays()
        val pair3 = payload3.toUByteArrays()

        var newPayload1 = AdvPayload.from(pair1.first, pair1.second)
        var newPayload2 = AdvPayload.from(pair2.first, pair2.second)
        var newPayload3 = AdvPayload.from(pair3.first, pair3.second)


        assertEquals(payload1.sid, newPayload1.sid)
        assertEquals(payload1.cid, newPayload1.cid)
        assertEquals(payload1.md_version, newPayload1.md_version)
        assertEquals(
            payload1.ebid_slice.toString().trim(),
            newPayload1.ebid_slice.toString().trim()
        )

        assertEquals(payload2.sid, newPayload2.sid)
        assertEquals(payload2.cid, newPayload2.cid)
        assertEquals(payload2.md_version, newPayload2.md_version)
        assertEquals(
            payload2.ebid_slice.toString().trim(),
            newPayload2.ebid_slice.toString().trim()
        )

        assertEquals(payload3.sid, newPayload3.sid)
        assertEquals(payload3.cid, newPayload3.cid)
        assertEquals(payload3.md_version, newPayload3.md_version)
        assertEquals(
            payload3.ebid_slice.toString().trim(),
            newPayload3.ebid_slice.toString().trim()
        )

        var advData: AdvData = AdvData(ebid, cid)
        var newPayload4 = advData.getPayloads(0)
        var newPayload5 = advData.getPayloads(1)
        var newPayload6 = advData.getPayloads(2)


        assertEquals(payload1.sid, newPayload4.sid)
        assertEquals(payload1.cid, newPayload4.cid)
        assertEquals(payload1.md_version, newPayload4.md_version)
        assertEquals(
            payload1.ebid_slice.toString().trim(),
            newPayload4.ebid_slice.toString().trim()
        )

        assertEquals(payload2.sid, newPayload5.sid)
        assertEquals(payload2.cid, newPayload5.cid)
        assertEquals(payload2.md_version, newPayload5.md_version)
        assertEquals(
            payload2.ebid_slice.toString().trim(),
            newPayload5.ebid_slice.toString().trim()
        )

        assertEquals(payload3.sid, newPayload6.sid)
        assertEquals(payload3.cid, newPayload6.cid)
        assertEquals(payload3.md_version, newPayload6.md_version)
        // need to take only the first 8 bytes
        assertEquals(
            payload3.ebid_slice.toString().trim(),
            newPayload6.ebid_slice.copyOfRange(0, 8).toString().trim()
        )

    }
}
