package fr.inria.desire.ble.services

import android.bluetooth.le.*
import android.os.ParcelUuid
import android.util.Log
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.managers.LogManager
import fr.inria.desire.ble.settings.ScannerSettings
import fr.inria.desire.ble.interfaces.Startable
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.models.AdvPayload
import fr.inria.desire.ble.models.ContactedEbid
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.RTLData
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.ArrayList

@ExperimentalUnsignedTypes
class ScannerServices(
    scanner: BluetoothLeScanner,
    mask: String,
    scannerUIUpdate: ((Int, String) -> Unit)
) : Startable {
    private var mBluetoothLeScanner: BluetoothLeScanner = scanner
    private var shortMask: String = mask

    private var nbAdvReceived: Int = 0
    private var nbAdvReceivedMinute: Int = 0
    private var nbAdvReceivedMinuteTemp: Int = 0
    private var timestampStartScanner: Long = 0

    private var uiUpdateCallback: ((Int, String) -> Unit)? = scannerUIUpdate
    override var started: Boolean = false

    private val connectLock = ReentrantLock()

    override fun start() {

        Log.i(TAG, "Start scanning")
        Log.i(TAG, "  - new shortMask $shortMask")

        // record info in csv
        LogManager.record(
            recordType = LogManager.RECORD_TYPE.SCAN,
            map = mutableMapOf(
                "message" to "scan started"
            )
        )

        val settings = ScanSettings.Builder()
            .setScanMode(ScannerSettings.scanMode)
            .setReportDelay(ScannerSettings.reportDelay)
            .build()
        val filters = ArrayList<ScanFilter>()
        // filters not set, as filter mask is not properly implemented on at least one of the test
        // phones.
        timestampStartScanner = System.currentTimeMillis()
        ScannerSettings.scannerStartTs = System.currentTimeMillis()
        mBluetoothLeScanner.startScan(filters, settings, mScanCallback)
    }

    fun processResult(result: ScanResult) {

        var rng = Random()
        var rngVal = rng.nextInt(100)
        if (rngVal < GlobalSettings.packetLoss) {
            Log.i(TAG, " Packet lost.")
            return
        }
        // TODO: change foe the proper stocovid shortmask
        val shortUuid = result.scanRecord?.serviceUuids?.get(0).toString().substring(4, 8)
        if (!shortUuid.equals(Constants.UUID_EBID_SERVICE_SHORTMASK)) {
            return
        }

        val serviceUuid: ParcelUuid? = result.scanRecord?.serviceUuids?.get(0)!!
        val serviceData = result.scanRecord?.serviceData
        val parcelData = serviceData!!.get(serviceUuid!!)!!.toUByteArray()

        Log.v(TAG, "scn serviceUuid " + serviceUuid)
        Log.v(TAG, "scn serviceData " + serviceData.toString())
        Log.v(TAG, "scn parcelData " + parcelData.toString())
        Log.v(TAG, "scn parcelData size " + parcelData!!.size.toString())

        // TODO: for the third slice, should only copy 12 (8+4) and not 16 for data - need to
        //  change on the advertisement side also
        val data = parcelData.copyOfRange(0, 16)
        val metadata = parcelData.copyOfRange(16, 20)

        val payload = AdvPayload.from(data.toUByteArray(), metadata.toUByteArray())

        Log.v(TAG, "scn data array " + data.toString())
        Log.v(TAG, "scn metadata array " + metadata.toString())

        Log.v(TAG, "scn parcelData cid " + payload.cid.toString())
        Log.v(TAG, "scn parcelData sid " + payload.sid.toString())
        Log.v(TAG, "scn parcelData ebid slice " + payload.ebid_slice.toString())
        Log.v(TAG, "scn parcelData version " + payload.md_version.toString())

        // val corrected_powwer = payload.md_gtx + AdvertiserSettings.rx_bias

        Log.v(
            TAG,
            "ReceivedReceived a scan a scan result rssi=${result.rssi} rx_bias=${AdvertiserSettings.rx_bias}"
        )


        ScannerSettings.addDevice(payload.cid.toString())
        if (!ScannerSettings.seenEBID.containsKey(payload.cid.toString())) {
            Log.v(TAG, "New seen device with cid " + payload.cid.toString())
            ScannerSettings.seenEBID.set(
                payload.cid.toString(),
                ContactedEbid(EBID())
            )
        }

        // record info in csv
        LogManager.record(
            recordType = LogManager.RECORD_TYPE.SCAN,
            map = mutableMapOf(
                "message" to "part received $payload.sid $payload.cid"
            )
        )

        // add part to temp EBID and try to reconstruct
        val contactedEbid = ScannerSettings.seenEBID.get(payload.cid.toString())
        if (contactedEbid != null) {
            contactedEbid.seen()
            val tmpEbid = contactedEbid.ebid
            if (!tmpEbid.complete) {
                // add slice to ebid using slice number (1..3)
                tmpEbid.add(payload.sid.toInt() + 1, payload.ebid_slice)
                if (tmpEbid.canBeReconstructed()) {
                    try {
                        tmpEbid.reconstruct()
                        Log.v(
                            TAG,
                            "New EBID constructed " + tmpEbid.toString() + " in " + (System.currentTimeMillis() - ScannerSettings.scannerStartTs) + "ms (" + System.currentTimeMillis() + " - " + ScannerSettings.scannerStartTs + ")"
                        )
                        contactedEbid.timeToDiscover =
                            System.currentTimeMillis() - ScannerSettings.scannerStartTs
                        // record info in csv
                        LogManager.record(
                            recordType = LogManager.RECORD_TYPE.SCAN,
                            map = mutableMapOf(
                                "message" to "New device discovered",
                                "newEbid" to tmpEbid.toString()
                            )
                        )
                    } catch (e: KotlinNullPointerException) {
                        Log.w(TAG, "failed to reconstruct for now (null pointer)" + e.message)
                    }
                }
            } else {
                if (!ScannerSettings.EBIDdata.containsKey(tmpEbid)) {
                    val tmprtl = RTLData()
                    tmprtl.startTimestamp = System.currentTimeMillis()
                    tmprtl.computeObf(GlobalSettings.localEbid, tmpEbid)
                    ScannerSettings.EBIDdata.set(tmpEbid, tmprtl)

                }
                val rtl = ScannerSettings.EBIDdata.get(tmpEbid)
                rtl?.add(
                    result.rssi - AdvertiserSettings.rx_bias - rtl.obf,
                    System.currentTimeMillis()
                )
            }
        }

    }


    private val mScanCallback: ScanCallback = object : ScanCallback() {

        // single scan result
        override fun onScanResult(callbackType: Int, result: ScanResult?) {

            // filtering of results according to puuid
            // assume that all the service data is received in the same order as when sending it
            // the service uuid is then the thrid items in the uuids
            if (result != null &&
                result.scanRecord != null &&
                result.scanRecord!!.serviceUuids != null &&
                result.scanRecord!!.serviceUuids!!.size >= 2
            ) {
                Log.v(TAG, "Received a scan result (single)")
                val puuid = result.scanRecord?.serviceUuids?.get(2).toString()
                if (puuid.length < 8 || puuid.substring(0, 7)
                        .toLowerCase(Locale.ROOT)
                        .replaceRange(7, 7, "0") != shortMask
                ) return
            } else return


            nbAdvReceived += 1
            nbAdvReceivedMinuteTemp += 1
            if ((System.currentTimeMillis() - timestampStartScanner) > 60000) {
                nbAdvReceivedMinute = nbAdvReceivedMinuteTemp
                nbAdvReceivedMinuteTemp = 0
                timestampStartScanner = System.currentTimeMillis()
            }

            processResult(result)
            Log.i(TAG, " - seen devices : ${ScannerSettings.seenEBID}")
            ScannerSettings.nearbyDevicesCount()
            uiUpdateCallback?.invoke(1, "$nbAdvReceived ($nbAdvReceivedMinute last minute)")
        }

        override fun onBatchScanResults(results: List<ScanResult?>?) {
            super.onBatchScanResults(results)

            if (results != null) {
                Log.v(TAG, "Received a scan result (batch)")

                for (result: ScanResult? in results) {

                    Log.v(TAG, " - processing one of the batch results " + result.toString())

                    // filtering of results according to puuid
                    // assume that all the service data is received in the same order as when sending it
                    // the service uuid is then the thrid items in the uuids
                    if (result != null
                        && result.scanRecord != null
                        && result.scanRecord!!.serviceUuids != null
                    // && result.scanRecord!!.serviceUuids!!.size >= 3
                    ) {
                        val puuid = result.scanRecord?.serviceUuids?.get(0).toString()
                        //if (puuid.length < 8 || !puuid.substring(0, 7)
                        //        .toLowerCase(Locale.ROOT)
                        //        .replaceRange(7, 7, "0")
                        //        .equals(shortMask)
                        //) {
                        //    continue
                        //}
                    } else {
                        continue
                    }

                    nbAdvReceived += 1
                    nbAdvReceivedMinuteTemp += 1

                    processResult(result)

                    Log.i(TAG, " - seen devices : ${ScannerSettings.seenEBID}")
                }
            }

            if ((System.currentTimeMillis() - timestampStartScanner) > 60000) {
                nbAdvReceivedMinute = nbAdvReceivedMinuteTemp
                nbAdvReceivedMinuteTemp = 0
                timestampStartScanner = System.currentTimeMillis()
            }

            // record info in csv
            LogManager.record(
                recordType = LogManager.RECORD_TYPE.LOG,
                map = mutableMapOf(
                    "message" to "addr received $nbAdvReceived ($nbAdvReceivedMinute last minute)"
                )
            )

            ScannerSettings.nearbyDevicesCount()
            uiUpdateCallback?.invoke(1, "$nbAdvReceived ($nbAdvReceivedMinute last minute)")
        }

        override fun onScanFailed(errorCode: Int) {
            Log.e(TAG, "Discovery onScanFailed: $errorCode")
            super.onScanFailed(errorCode)
        }
    }
}
