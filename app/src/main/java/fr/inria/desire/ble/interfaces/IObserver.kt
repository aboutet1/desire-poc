package fr.inria.desire.ble.interfaces

interface IObserver {
    fun update()
}