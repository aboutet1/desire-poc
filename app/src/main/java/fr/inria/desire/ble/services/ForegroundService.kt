package fr.inria.desire.ble.services

import android.R
import android.app.*
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import fr.inria.desire.ble.interfaces.Startable
import fr.inria.desire.ble.settings.GlobalSettings
import java.util.*
import kotlin.collections.ArrayList

class ForegroundService : Service() {

    private val mHandler: Handler? = Handler()
    private lateinit var mRunnable: Runnable
    private val CHANNEL_ID = "ForegroundService-BLEMultiAdv"
    private val startableArray: ArrayList<Startable> = ArrayList()

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        Log.d("Service", "OnCreate")
        Toast.makeText(this, "Creating Notification", Toast.LENGTH_SHORT).show()
        var rndIndex = 0
        val rng = Random()
        do {
            rndIndex = rng.nextInt(9999)
        } while (GlobalSettings.serviceIdTable.contains(rndIndex))
        GlobalSettings.serviceIdTable.add(rndIndex)
        createNotificationChannel()
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Foreground Sticker")
            .setTicker("Foreground Sticker")
            .setContentText("Foreground Service Running")
            .setSmallIcon(R.drawable.ic_notification_overlay)
            .setOngoing(true).build()
        startForeground(rndIndex, notification)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val idActionsToRun = intent.getIntegerArrayListExtra("listToStart")
        Log.d("Service", "Start Command")
        if (idActionsToRun != null) {
            for (id in idActionsToRun) {
                Log.d("Service", "Action Ok")
                startableArray.add(GlobalSettings.startableArray[id])
            }
            mRunnable = Runnable {
                runStartable()
            }

            mHandler?.post(
                mRunnable // Runnable
            )
            return START_STICKY
        } else {
            Log.d("Service", "Action NOk")
            stopSelf()
            return START_NOT_STICKY
        }
    }

    private fun runStartable() {
        for (action in startableArray) {
            if (!action.started) {
                action.start()
                action.started = true
            }
        }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        println("onTaskRemoved called")
        super.onTaskRemoved(rootIntent)
        //do something you want
        //stop service
        this.stopSelf()
    }

    override fun onDestroy() {
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID, "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }
}