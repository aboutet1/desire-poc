package fr.inria.desire.ble.models

import kotlin.math.ln
import kotlin.math.max
import kotlin.math.min

@ExperimentalUnsignedTypes
class FadedWindows constructor(
    var qArray: ArrayList<Int?>,
    var nArray: ArrayList<UInt>,
    var tsArray: ArrayList<Long>
) {
    constructor() : this(ArrayList<Int?>(), ArrayList<UInt>(), ArrayList<Long>())

    val p0: Float = -66f
    val deltaP = intArrayOf(39, 27, 23, 21, 20, 19, 18, 17, 16, 15)
    fun add(q: Int?, n: UInt?, ts: Long) {
        if (q != null && n != null) {
            qArray.add(q)
            if (n > 10u)
                nArray.add(10u)
            else
                nArray.add(n)
            tsArray.add(ts)
        } else {
            qArray.add(null)
            nArray.add(0u)
            tsArray.add(ts)
        }
    }

    fun risk(): RiskWindow {
        var rw = RiskWindow()
        for (i in 0 until qArray.size) {
            if (qArray[i] != null) {
                val piTemp = max(0f, min(1.0f, (qArray[i]!! - p0) / deltaP[nArray[i].toInt() - 1]))
                rw.add(piTemp)
            } else {
                // #TODO
            }
        }
        return rw
    }

    override fun toString(): String {
        var ret = "{\n"
        for (i in 0 until qArray.size) {
            ret += "\t{" + qArray[i] + "," + nArray[i] + "," + tsArray[i] + "}\n"
        }
        ret += "}\n"
        return ret
    }

    fun asTriple(): List<Triple<Int?, UInt, Long>> {
        val ret = ArrayList<Triple<Int?, UInt, Long>>()
        for (i in 0 until qArray.size) {
            ret.add(Triple(qArray[i], nArray[i], tsArray[i]))
        }
        return ret
    }

}