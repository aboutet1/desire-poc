package fr.inria.desire.ble.backend


object Backend {
    var id: String = ""
    var key: String = ""
    var secret_sigma: String = ""
    var secret_r: String = ""
    const val baseUrl = "https://sed-webtests.paris.inria.fr/"

    const val apiStatus = "ble/status"
    const val apiReport = "ble/report"
    const val apiRegister = "ble/register"
    const val apiExposed = "ble/exposed"
}