package fr.inria.desire.ble.managers

import android.transition.Fade
import android.util.Log
import fr.inria.desire.ble.models.*

@ExperimentalUnsignedTypes
object EpochDataManager {
    var epochsData: MutableList<Epoch> = ArrayList()

    // add a new epoch data at the end of the list
    fun addEpochData(localEbid: EBID, data: Map<EBID, RTLData>, startTs: Long) {
        val epoch = Epoch(startTs, System.currentTimeMillis())
        for ((distEbid, rtl) in data) {
            if (rtl.isLongEnough()) {
                val pet = PET.fromEBID(distEbid)!!
                epoch.add(Pair(rtl, pet))
            }
        }
        epoch.process()
        val sentData = epoch.getRequestData()
        for (d in sentData) {
            Log.d(
                "Exposed",
                "- ETL : ${d.first.etl.contentToString()}\n - RTL : ${d.first.rtl.contentToString()}\n - ${d.second}"
            )
        }
        epochsData.add(epoch)
    }

    fun getExposedData(): List<Pair<PET, Int>> {
        // #TODO : discard old data and retrieve only relevant data
        val ret = ArrayList<Pair<PET, Int>>()
        for (epoch in epochsData) {
            val tmp = epoch.getExposedData()
            ret.addAll(tmp)
        }
        return ret
    }

    fun getRequestData(): List<Pair<PET, FadedWindows>> {
        // #TODO : discard old data and retrieve only relevant data
        val ret = ArrayList<Pair<PET, FadedWindows>>()
        for (epoch in epochsData) {
            val tmp = epoch.getRequestData()
            ret.addAll(tmp)
        }
        return ret
    }


    // provides the data to send for status requests/covid notification for epochs that starts strictly before the provided timestamp
    fun getEpochDataBefore(timestamp: Long) {
    }

    // remove any epoch data that starts strictly before the provided timestamp
    fun clearEpochDataBefore(timestamp: Long) {
        epochsData.removeIf { it.startTimestamp < timestamp }
    }

}


