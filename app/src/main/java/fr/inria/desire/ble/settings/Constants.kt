package fr.inria.desire.ble.settings

import java.util.*

object Constants {
    const val METADATA_VERSION: UInt = 0xC8u

    // Official 16-bits INRIA GATT SERVICE UUID (BLE_SERVICE_UUID)
    val GATT_EBID_SERVICE: UUID = UUID.fromString("0000fd64-0000-1000-8000-00805f9b34fb")
    const val GATT_CID_TEMPLACE_SERVICE = "0000xxxx-0000-1000-8000-00805F9B34FB"
    const val GATT_EBID_SERVICE_SHORTMASK = "0000fd64"

    // custom 16-bits UUID for Gatt characteristics
    val GATT_EBID_CHARA: UUID = UUID.fromString("0000468A-0000-1000-8000-00805f9b34fb")
    // official uuid for uuid characteritics (BLE_CHARACTERISTIC_UUID)
    // val GATT_EBID_CHARA: UUID = UUID.fromString("a8f12d00-ee67-478b-b95f-65d599407756")

    // const val sidTemplate = "0000%02x%02x-0000-1000-8000-00805F9B34FB"


    // val UUID_EBID_SERVICE: UUID = UUID.fromString("0000fd64-0000-1000-8000-00805f9b34fb")
    val UUID_EBID_SERVICE: UUID = UUID.fromString("00006666-0000-1000-8000-00805f9b34fb")
    const val UUID_EBID_SERVICE_SHORTMASK = "6666"

    const val EBID_SIZE: Int = 32
    const val EBID_SLICES_NB: Int = 3
    const val EBID_SLICES_XOR_NB = EBID_SLICES_NB + 1

    val uuidCidTemplate = "0000%s%s-0000-1000-8000-00805F9B34FB"

}