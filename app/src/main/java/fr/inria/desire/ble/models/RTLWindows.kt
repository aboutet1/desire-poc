package fr.inria.desire.ble.models

class RTLWindows() {

    val values: MutableList<RTLWindow> = ArrayList()
    val fadedValues: FadedWindows = FadedWindows()
    var processed = false


    fun clipping() {
        for (index in 0 until values.size) {
            val win: RTLWindow = values[index]
            win.clipping()
        }
    }

    fun fading(): FadedWindows {
        for (index in 0 until values.size) {
            val win: RTLWindow = values[index]
            try {
                val pair = win.fading()
                fadedValues.add(pair.first, pair.second.toUInt(), win.ts)
            } catch (e: Exception) {
                fadedValues.add(null, null, win.ts)
            }
        }

        return fadedValues
    }

    fun process() {
        if (!processed) {
            this.clipping()
            this.fading()
        }
        processed = true
    }
}
