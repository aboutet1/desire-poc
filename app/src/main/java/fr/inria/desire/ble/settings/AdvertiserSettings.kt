package fr.inria.desire.ble.settings

import android.bluetooth.le.AdvertisingSetParameters

object AdvertiserSettings {
    const val ADV_MODE_MULTISERVICE = 0
    const val ADV_MODE_ROTATION = 1
    const val ADV_MODE_CONNECTED = 2

    var started = false

    // transmission rx/tx correction for this phone
    var tx_bias: Int = 0
    var rx_bias: Int = 0
    var tx_power: Int = 0

    var txPowerLevelPos: Int = 3
    var txPowerLevel: Int = 0
        get() = when (txPowerLevelPos) {
            5 -> AdvertisingSetParameters.TX_POWER_MAX
            4 -> AdvertisingSetParameters.TX_POWER_HIGH
            3 -> AdvertisingSetParameters.TX_POWER_MEDIUM
            2 -> AdvertisingSetParameters.TX_POWER_LOW
            1 -> AdvertisingSetParameters.TX_POWER_ULTRA_LOW
            else -> AdvertisingSetParameters.TX_POWER_MIN
        }

    var intervalPos: Int = 3
    var interval: Int = 0
        get() = when (intervalPos) {
            4 -> AdvertisingSetParameters.INTERVAL_MAX
            3 -> AdvertisingSetParameters.INTERVAL_HIGH
            2 -> AdvertisingSetParameters.INTERVAL_MEDIUM
            1 -> AdvertisingSetParameters.INTERVAL_LOW
            else -> AdvertisingSetParameters.INTERVAL_MIN
        }

    var partsInterval: Int = 20
}
