package fr.inria.desire.ble.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import fr.inria.desire.ble.R
import fr.inria.desire.ble.backend.*
import fr.inria.desire.ble.interfaces.IObserver
import fr.inria.desire.ble.models.ContactedEbid
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.GlobalSettings
import fr.inria.desire.ble.settings.ScannerSettings
import org.json.JSONObject
import java.math.BigInteger
import java.util.*
import kotlin.collections.ArrayList


const val TAG = "DESIRE"

@ExperimentalUnsignedTypes
class StatusActivity : AppCompatActivity(),
    IObserver {

    private lateinit var textExposureStatus: TextView
    private lateinit var textRiskScore: TextView
    private lateinit var textAdvStatus: TextView
    private lateinit var textScannerStatus: TextView
    private lateinit var textEbid: TextView
    private lateinit var textCurrentDevices: TextView
    private lateinit var textCreatedPets: TextView
    private lateinit var requestBtn: Button
    private lateinit var ebidsList: LinearLayout


    init {
        instance = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)

        val setting_btn = findViewById(R.id.btn_settings) as Button
        setting_btn.setOnClickListener {
            val myIntent = Intent(this, SettingsActivity::class.java)
            this.startActivity(myIntent)
        }

        val declaration_btn = findViewById(R.id.btn_report) as Button
        declaration_btn.setOnClickListener {
            this.declare()
        }

        requestBtn = findViewById(R.id.btn_request_status) as Button

        requestBtn.setOnClickListener {
            this.request()
        }
        textExposureStatus = findViewById(R.id.exposure_status)
        textRiskScore = findViewById(R.id.risk_mode)
        textAdvStatus = findViewById(R.id.adv_status)
        textScannerStatus = findViewById(R.id.scanner_status)
        textEbid = findViewById(R.id.current_ebid)
        textCurrentDevices = findViewById(R.id.text_nb_near_device)
        textCreatedPets = findViewById(R.id.text_nb_created_pet)

        ebidsList = findViewById<View>(R.id.ll_discovered_ebid) as LinearLayout

        ScannerSettings.add(this)
        GlobalSettings.add(this)
        backendAccess()

        initIDs()
        initBias()

        updateUI()
    }

    override fun onResume() {
        super.onResume()
        updateUI()
    }

    override fun update() {
        runOnUiThread(Runnable {
            updateUI()
        })
    }

    private fun updateUI() {
        textExposureStatus.setText(GlobalSettings.exposureStatus)
        textRiskScore.setText(if (GlobalSettings.riskScore == GlobalSettings.CENTRALIZED) "Centralized" else "Decentralized")
        requestBtn.setText(if (GlobalSettings.riskScore == GlobalSettings.CENTRALIZED) "Exposure Status Request" else "Get Exposed PETs List")
        textAdvStatus.setText(if (AdvertiserSettings.started) "On" else "Off")
        textScannerStatus.setText(if (ScannerSettings.started) "On" else "Off")

        if (GlobalSettings.isAdvDataInitialised()) {
            val pair = GlobalSettings.advData.get()
            val text: String =
                "#" + GlobalSettings.ebidcidChanges + " " + pair.second.value.toString() + " " + pair.first
            textEbid.setText(text)
        }
        textCurrentDevices.setText(
            ScannerSettings.nearbyDevicesCount().toString()
        )
        textCreatedPets.setText(GlobalSettings.createdPET.size.toString())

        displayDiscoveredEbid()
    }

    private fun displayDiscoveredEbid() {
        ebidsList.removeAllViews()

        ScannerSettings.seenEBID.forEach { (key, value) -> addEbidElement(key, value) }
    }

    private fun addEbidElement(cid: String, ebid: ContactedEbid) {
        val tv = TextView(this)
        tv.text =
            "EBID ok=${ebid.ebid.complete} cid=${cid} discovery time : ${ebid.timeToDiscover} \n ${ebid.ebid}"
        tv.layoutParams =
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        ebidsList.addView(tv)
    }

    private fun backendAccess() {
        val number = Random().nextInt(99999999)
        val phone: String = String.format("%08d", number);

        Log.d(TAG, "Register new phone number " + phone)
        Toast.makeText(applicationContext, "Register new phone number " + phone, Toast.LENGTH_SHORT)
            .show()

        registerGetApi(phone)
        // wait some time for callbacks
        //registerPostApi("Qk4mDu4GsjY=")
        // wait some time for callbacks to set the rep value
        // use REP value instead of hardcoded integer below
        //registerPutApi( BigInteger("15927203581760077296488239914232451659745548679880308712830168397212903498722345923280183979426599801654929697083808547985163001102553543026028467006904862777940584447819328436996848104837015214682322188982591652061055915801910461860994494181403454908131737834329234842211602599756824484957614265864618127283187044734286529612981320111309826087751434071224354339011722049168816164314794129796669291030623294287097226986722946690784707401446126004559805826815532618851013134444793199319461958821376259150873313456189416052986835116309391745904320775454609161696270827822791615946636671446461530888580519895019007242778") )
    }

    private fun declare() {
        reportGetApi(GlobalSettings.createdPET.size)
    }

    private fun request() {
        if (GlobalSettings.riskScore == GlobalSettings.CENTRALIZED) {
            val pets = ArrayList<String>()
            for (pet in GlobalSettings.createdPET) {
                pets.add(pet.rtl.fold("", { str, it -> str + "%02x".format(it) }))
            }
            statusGetApi(Backend.id, pets, Backend.key)
        } else {
            exposedGetApi()
        }
    }

    private fun initIDs() {
        GlobalSettings.updateCidEbid()
    }

    private fun initBias() {

        val jsonString = resources.openRawResource(R.raw.config_bias)
            .bufferedReader().use { it.readText() }

        val jsonObj = JSONObject(jsonString)
        val configJson = jsonObj.getJSONArray("config")

        for (i in 0..configJson!!.length() - 1) {
            val name = configJson.getJSONObject(i).getString("name")
            if (name.equals("ble.calibration")) {
                val biasJson = configJson.getJSONObject(i).getJSONArray("value")

                for (j in 0..biasJson!!.length() - 1) {
                    val model = biasJson.getJSONObject(j).getString("device_handset_model")
                    val tx_RSS_correction_factor =
                        biasJson.getJSONObject(j).getInt("tx_RSS_correction_factor")
                    val rx_RSS_correction_factor =
                        biasJson.getJSONObject(j).getInt("rx_RSS_correction_factor")

                    if (model.equals("DEFAULT") && AdvertiserSettings.tx_bias == null) {
                        AdvertiserSettings.tx_bias = tx_RSS_correction_factor
                        AdvertiserSettings.rx_bias = rx_RSS_correction_factor
                        Log.d(
                            TAG,
                            "Set default tx/rx RSS correction of  " + tx_RSS_correction_factor + " " + rx_RSS_correction_factor
                        )
                    } else if (model.equals(Build.MODEL)) {
                        AdvertiserSettings.tx_bias = tx_RSS_correction_factor
                        AdvertiserSettings.rx_bias = rx_RSS_correction_factor
                        Log.d(
                            TAG,
                            "Set " + Build.MODEL + " phone's tx/rx RSS correction of  " + tx_RSS_correction_factor + " " + rx_RSS_correction_factor
                        )
                    }
                }
            }
        }
    }

    // companion object for backend callbacks - UI updates
    // the error callback is common to all listeners
    // TODO : manage multiple requests
    companion object RegListener : RegisterListener, ReportListener, StatusListener,
        ExposedListener {

        // to be initialized in the constructor of the class
        private var instance: StatusActivity? = null

        // register callbacks

        override fun getResponse(response: RegisterResponseGet) {
            Log.i(
                TAG,
                "RegisterListener - getResponse pin=" + response.pin + " key" + response.public_key.toString()
            )

            registerPostApi(response.pin.toString())
        }

        override fun postResponse(response: RegisterResponsePost) {
            Log.i(TAG, "RegisterListener - postResponse " + response.rep)

            val decodedBytes = BigInteger(1, Base64.getDecoder().decode(response.rep))
            Log.i(TAG, "RegisterListener - postResponse rep value " + decodedBytes.toString())

            registerPutApi(BigInteger(decodedBytes.toString()))

        }

        override fun putResponse(response: RegisterResponsePut) {
            Log.i(TAG, "RegisterListener - putResponse")

            Toast.makeText(
                instance?.applicationContext,
                "Phone and new user registered",
                Toast.LENGTH_SHORT
            ).show()
            Backend.id = response.id.toString()
            Backend.key = response.key.toString()
        }

        //
        // report callbacks

        override fun getResponse(response: ReportResponseGet) {
            Log.i(
                TAG,
                "ReportListener - getResponse tokens=" + response.tokens
            )
            if (response.tokens != null) {
                for ((cpt, token) in response.tokens!!.withIndex()) {
                    val currentPET = GlobalSettings.createdPET[cpt]
                    reportPostApi(currentPET, token)
                }
                Toast.makeText(
                    instance?.applicationContext,
                    "Infected Node Reported",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        override fun postResponse(response: ReportResponsePost) {
            Log.i(TAG, "ReportListener - postResponse")
        }

        //
        // status callbacks

        override fun response(response: StatusResponseGet) {
            Log.i(
                TAG,
                "StatusListener - getResponse at_risk=" + response.at_risk
            )
            GlobalSettings.exposureStatus =
                if (response.at_risk != null && response.at_risk!!) "At Risk" else "Unknown"
        }

        //
        //Exposed callback

        override fun getResponse(response: ExposedResponseGet) {
            Log.i(
                TAG,
                "ExposedListener - getResponse exposed=" + response.exposedList
            )
            var cptSame = 0
            if (response.exposedList != null) {
                for (pet in GlobalSettings.createdPET) {
                    val petString = pet.rtl.fold("", { str, it -> str + "%02x".format(it) })
                    for (exposedPet in response.exposedList!!) {
                        if (petString == exposedPet) {
                            cptSame++
                            break
                        }
                    }
                }
                // TODO better at_risk computation
                if (cptSame > 0) {
                    GlobalSettings.exposureStatus = "At Risk"
                }
            }
        }


        override fun error(code: Int) {
            Log.i(TAG, "RegisterListener - error")
        }
    }


}