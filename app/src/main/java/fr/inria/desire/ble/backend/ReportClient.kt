package fr.inria.desire.ble.backend

import android.util.Log
import com.google.gson.annotations.SerializedName
import fr.inria.desire.ble.activities.TAG
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ReportClientAPI {
    @GET(Backend.apiReport)
    fun getReport(
        @Query("r") r: String,
        @Query("sigma") sigma: String,
        @Query("number_of_tokens") number_of_tokens: Int
    ): Call<ReportResponseGet>

    @Headers("Content-Type: application/json")
    @POST(Backend.apiReport)
    fun postReport(@Body postReportData: PostReportData): Call<ReportResponsePost>
}

interface ReportListener {
    fun getResponse(response: ReportResponseGet)
    fun postResponse(response: ReportResponsePost)
    fun error(code: Int)
}

class ReportResponseGet {
    @SerializedName("tokens")
    var tokens: Array<String>? = null
}

class ReportResponsePost {
}

class PostReportData {
    @SerializedName("token")
    var token: String? = null

    @SerializedName("pet")
    var pet: String? = null

    @SerializedName("datetime")
    var datetime: Long? = null
}


class ReportClient(val caller: ReportListener) {
    var service: ReportClientAPI

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(Backend.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(ReportClientAPI::class.java)
    }


    fun getReport(r: String, sigma: String, number_of_tokens: Int) {

        val call = service.getReport(r, sigma, number_of_tokens)
        call.enqueue(object : Callback<ReportResponseGet> {
            override fun onResponse(
                call: Call<ReportResponseGet>,
                response: Response<ReportResponseGet>
            ) {
                if (response.code() == 200) {
                    val reportResponse = response.body()!!
                    Log.i(TAG, "repost response : " + reportResponse.toString())
                    caller.getResponse(reportResponse)
                } else {
                    Log.i(
                        TAG,
                        "getReport unable to proceed - error code : " + response.code().toString()
                    )
                    caller.error(response.code())
                }
            }

            override fun onFailure(call: Call<ReportResponseGet>?, t: Throwable?) {
                Log.w(TAG, "getReport call failure")
                caller.error(-1)
            }
        })
    }

    fun postReport(token: String, pet: String, datetime: Long) {

        val postReportData: PostReportData = PostReportData()
        postReportData.token = token
        postReportData.pet = pet
        postReportData.datetime = datetime

        val call = service.postReport(postReportData)
        call.enqueue(object : Callback<ReportResponsePost> {
            override fun onResponse(
                call: Call<ReportResponsePost>,
                response: Response<ReportResponsePost>
            ) {
                if (response.code() == 200) {
                    val reportResponse = response.body()!!
                    Log.i(TAG, "repost response : " + reportResponse.toString())
                    caller.postResponse(reportResponse)
                } else {
                    Log.i(
                        TAG,
                        "postReport unable to proceed - error code : " + response.code().toString()
                    )
                    caller.error(response.code())
                }
            }

            override fun onFailure(call: Call<ReportResponsePost>?, t: Throwable?) {
                Log.w(TAG, "postReport call failure")
                caller.error(-1)
            }
        })
    }

}