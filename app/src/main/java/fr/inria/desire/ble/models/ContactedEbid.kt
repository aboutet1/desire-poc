package fr.inria.desire.ble.models

class ContactedEbid(val ebid: EBID) {
    var timeToDiscover: Long = 0
    val firstSeen: Long = System.currentTimeMillis()
    var lastSeen: Long = System.currentTimeMillis()
    var nbSeen: Int = 1

    fun seen() {
        lastSeen = System.currentTimeMillis()
        nbSeen++
    }

    override fun toString(): String {
        return "$firstSeen - $lastSeen : $nbSeen"
    }
}
