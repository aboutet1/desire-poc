package fr.inria.desire.ble.models

import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.Constants


@ExperimentalUnsignedTypes
data class AdvData constructor(
    private var ebid: EBID,
    private var cid: CID
) {

    // need to ensure that access to the EBID and CID are always performed synchronously
    // i.e., that one thread updating ebid then cid is not interleved with another thread
    // accessing the ebid and the cid (thus accessing the ebid of T+1 while accessing the CID of T
    @Synchronized
    fun update(ebid: EBID, cid: CID) {
        this.ebid = ebid
        this.cid = cid
    }

    @Synchronized
    fun get(): Pair<EBID, CID> {
        return Pair(ebid, cid)
    }

    @Synchronized
    fun getPayloads(sliceNb: Int): AdvPayload {
        if (sliceNb < 0 || sliceNb >= (Constants.EBID_SLICES_NB + 1)) {
            throw ArrayIndexOutOfBoundsException("EBID has $Constants.EBID_SLICES_NB slices, wrong slice number.")
        }

        val ebidSlice: UByteArray? = getEbidSlice(sliceNb)
        val mdVersion: UInt = Constants.METADATA_VERSION

        val res: AdvPayload = AdvPayload(sliceNb.toUInt(), cid.value, ebidSlice!!, mdVersion)
        return res
    }

    private fun getEbidSlice(sliceNb: Int): UByteArray? {
        return when (sliceNb) {
            0 -> {
                this.ebid.getPart(1)
            }
            1 -> {
                this.ebid.getPart(2)
            }
            2 -> {
                this.ebid.getPart(3)
            }
            3 -> {
                // xor slice
                this.ebid.getPart(4)
            }
            else -> { // Note the block
                null
            }
        }
    }
}