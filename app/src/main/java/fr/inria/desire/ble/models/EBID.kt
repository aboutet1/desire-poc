package fr.inria.desire.ble.models

import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.roundToInt

@ExperimentalUnsignedTypes
class EBID() {

    var ebid: UByteArray? = null
        private set
    private var ebidParts: HashMap<Int, UByteArray> = HashMap()
    private var partSize: ArrayList<Int> = ArrayList()
    var complete: Boolean = false
        private set

    companion object Factory {
        fun createFromUByteArray(fullEbid: UByteArray): EBID {
            val ebid = EBID()
            ebid.ebid = fullEbid

            // ebid.generateSplitParts()
            ebid.generateFixedParts()

            ebid.complete = true
            return ebid
        }
    }

    fun canBeReconstructed(): Boolean {
        var nbParts = 0
        for (i in 1..4) {
            if (ebidParts[i] != null)
                nbParts++
        }
        return nbParts >= Constants.EBID_SLICES_NB
    }

    fun generate() {
        CryptoManager.generateKeys()
        ebid = CryptoManager.publicKey!!.asUByteArray()
        generateFixedParts()
        complete = true
    }

    private fun generateFixedParts() {
        ebidParts = HashMap()

        var start = 0
        var end = 12
        ebidParts[1] = ebid!!.copyOfRange(start, end)
        start = 12
        end = 24
        ebidParts[2] = ebid!!.copyOfRange(start, end)
        start = 24
        end = 32
        ebidParts[3] = ebid!!.copyOfRange(start, end)

        val ebidParts3ForXor = ebid!!.copyOfRange(start, end) + ubyteArrayOf(0u, 0u, 0u, 0u)
        val partXor = ebidParts[2]!! xor ebidParts3ForXor xor ebidParts[1]!!

        ebidParts[4] = partXor
    }

    fun getPart(id: Int): UByteArray? {
        return ebidParts[id]
    }

    fun add(id: Int, part: UByteArray?) {
        if (part != null && !ebidParts.containsKey(id)) {
            if (id >= 1 && id <= 4) {
                ebidParts[id] = part.copyOf()
            }
        }
    }

    fun reconstruct(): UByteArray? {
        if (complete) {
            return ebid
        }
        // check enough parts
        if (!canBeReconstructed()) {
            return null
        }
        // reconstruct if needed
        if (ebidParts[1] == null) {
            val ebidParts3ForXor = ebidParts[3]!! + ubyteArrayOf(0u, 0u, 0u, 0u)
            ebidParts[1] = ebidParts[2]!! xor ebidParts3ForXor xor ebidParts[4]!!
            System.out.println("part 1 size " + ebidParts[3]!!.size)
        }
        if (ebidParts[2] == null) {
            val ebidParts3ForXor = ebidParts[3]!! + ubyteArrayOf(0u, 0u, 0u, 0u)
            ebidParts[2] = ebidParts[1]!! xor ebidParts3ForXor xor ebidParts[4]!!
            System.out.println("part 2 size " + ebidParts[3]!!.size)
        }
        if (ebidParts[3] == null) {
            val ebidParts3FromXor = ebidParts[1]!! xor ebidParts[2]!! xor ebidParts[4]!!
            ebidParts[3] = ebidParts3FromXor.copyOfRange(0, 8)
            System.out.println("part 3 size " + ebidParts[3]!!.size)
        }
        ebid = UByteArray(Constants.EBID_SIZE)
        var count = 0
        for (i in 1..3) {
            ebidParts[i]!!.copyInto(ebid!!, count)
            count += ebidParts[i]!!.size
            partSize.add(ebidParts[i]!!.size)
        }
        complete = true
        return ebid
    }

    override fun toString(): String {
        var tmp = ""
        if (complete) {
            tmp += "["
            for (b in ebid!!) {
                tmp += b.toInt().toString() + " , "
            }
            tmp += "]"
            return tmp
        }
        for (part in ebidParts) {
            tmp += part.key.toString() + " : "
            for (b in part.value) {
                tmp += b.toInt().toString() + " , "
            }
            tmp += "\n"
        }
        return tmp
    }

    operator fun compareTo(other: EBID): Int {
        if (this.ebid != null && other.ebid != null) {
            for (i in this.ebid!!.indices) {
                if (this.ebid!![i] > other.ebid!![i])
                    return 1
                else if (this.ebid!![i] < other.ebid!![i])
                    return -1
            }
        }
        return 0
    }
}

@ExperimentalUnsignedTypes
private infix fun UByteArray.xor(arr2: UByteArray): UByteArray {
    var i = 0
    val res: UByteArray = UByteArray(this.size)
    for (b in this) res[i] = this[i] xor arr2[i++]
    return res
}