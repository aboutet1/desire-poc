package fr.inria.desire.ble.interfaces

interface IObservable {
    val observers: ArrayList<IObserver>

    fun add(observer: IObserver) {
        observers.add(observer)
    }

    fun remove(observer: IObserver) {
        observers.remove(observer)
    }

    fun dataChanged() {
        observers.forEach { it.update() }
    }
}