package fr.inria.desire.ble.services

import android.bluetooth.*
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertisingSet
import android.bluetooth.le.AdvertisingSetCallback
import android.bluetooth.le.AdvertisingSetParameters
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.interfaces.Startable
import fr.inria.desire.ble.managers.LogManager
import fr.inria.desire.ble.models.AdvData
import fr.inria.desire.ble.models.CID
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings
import java.util.*

@ExperimentalUnsignedTypes
class AdvertiserConnection(
    var bluetoothManager: BluetoothManager,
    private var btAdapter: BluetoothAdapter
) : Startable {

    private var bluetoothGattServer: BluetoothGattServer? = null
    private lateinit var currentAdvertisingSet: AdvertisingSet
    private lateinit var ctx: Context

    fun init(ctx: Context) {
        this.ctx = ctx
    }

    override var started: Boolean = false

    override fun start() {
        startAdvertising()
        startServer(ctx)
    }

    private fun startAdvertising() {

        val btAdvertiser = btAdapter.bluetoothLeAdvertiser

        // still need to send CID in uuids to filter already seen devices on the scanner side
        val pair = GlobalSettings.advData.get()
        val cidBytes = ubyteArrayOf(
            (pair.second.value shr 24 and 0xffu).toUByte(),
            (pair.second.value shr 16 and 0xffu).toUByte(),
            (pair.second.value shr 8 and 0xffu).toUByte(),
            (pair.second.value shr 0 and 0xffu).toUByte()
        )

        val cidPart1 =
            String.format(
                Constants.uuidCidTemplate,
                cidBytes[0].toString(16),
                cidBytes[1].toString(16)
            )
        val cidPart2 =
            String.format(
                Constants.uuidCidTemplate,
                cidBytes[2].toString(16),
                cidBytes[3].toString(16)
            )

        val parameters = AdvertisingSetParameters.Builder()
            .setLegacyMode(true)
            .setInterval(AdvertiserSettings.interval)
            .setConnectable(true)
            .setScannable(true)
            .setTxPowerLevel(AdvertiserSettings.txPowerLevel)
            .setPrimaryPhy(BluetoothDevice.PHY_LE_1M)
            .setSecondaryPhy(BluetoothDevice.PHY_LE_2M)
        val data = AdvertiseData.Builder()
            .setIncludeTxPowerLevel(true)
            .setIncludeDeviceName(true)
            .addServiceUuid(ParcelUuid(Constants.GATT_EBID_SERVICE))
            .addServiceUuid(ParcelUuid(UUID.fromString(cidPart1)))
            .addServiceUuid(ParcelUuid(UUID.fromString(cidPart2)))
            .build()

        //Callbacks
        val callback: AdvertisingSetCallback = object : AdvertisingSetCallback() {
            override fun onAdvertisingSetStarted(
                advertisingSet: AdvertisingSet?,
                txPower: Int,
                status: Int
            ) {
                Log.i(
                    TAG, "onAdvertisingSetStarted(): txPower:" + txPower + " , status: "
                            + status + " aa: " + advertisingSet
                )
                AdvertiserSettings.tx_power = txPower

                // record info in csv
                LogManager.record(
                    recordType = LogManager.RECORD_TYPE.ADV,
                    map = mutableMapOf(
                        "myTx" to txPower
                    )
                )

                if (advertisingSet != null) {
                    currentAdvertisingSet = advertisingSet
                } else {
                    Log.e(TAG, "Callback did not provide AdvertisingSet ! ")
                }

            }

            override fun onAdvertisingSetStopped(advertisingSet: AdvertisingSet?) {
                Log.i(TAG, "onAdvertisingSetStopped():")
            }

        }
        btAdvertiser.startAdvertisingSet(parameters.build(), data, null, null, null, callback)
    }

    private fun startServer(ctx: Context) {
        bluetoothGattServer = bluetoothManager.openGattServer(ctx, gattServerCallback)
        bluetoothGattServer?.addService(createEBIDService())
            ?: Log.w(TAG, "Unable to create GATT server")
        Log.d(TAG, "GATT SERVER")
        for (bledev in bluetoothManager.getConnectedDevices(BluetoothProfile.GATT_SERVER)) {
            Log.d(TAG, bledev.address + " " + bledev.name + " " + bledev.toString())
        }
        Log.d(TAG, "GATT")
        for (bledev in bluetoothManager.getConnectedDevices(BluetoothProfile.GATT)) {
            Log.d(TAG, bledev.address + " " + bledev.name + " " + bledev.toString())
        }
    }

    private fun createEBIDService(): BluetoothGattService {
        val service = BluetoothGattService(
            Constants.GATT_EBID_SERVICE,
            BluetoothGattService.SERVICE_TYPE_PRIMARY
        )

        val ebidCharacteristic = BluetoothGattCharacteristic(
            Constants.GATT_EBID_CHARA,
            BluetoothGattCharacteristic.PROPERTY_READ,
            BluetoothGattCharacteristic.PERMISSION_READ
        )
        service.addCharacteristic(ebidCharacteristic)
        return service
    }

    private val gattServerCallback = object : BluetoothGattServerCallback() {
        override fun onConnectionStateChange(device: BluetoothDevice, status: Int, newState: Int) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "BluetoothDevice CONNECTED: $device")
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "BluetoothDevice DISCONNECTED: $device")
            }
        }

        override fun onCharacteristicReadRequest(
            device: BluetoothDevice?,
            requestId: Int,
            offset: Int,
            characteristic: BluetoothGattCharacteristic
        ) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic)
            Log.i(TAG, "onCharacteristicReadRequest " + characteristic.uuid.toString())


            val pair = GlobalSettings.advData.get()
            val ebidValue: UByteArray = pair.first.ebid!!
            val ebidSize: UByteArray = ubyteArrayOf(pair.first.ebid!!.size.toUByte())
            val metadataSize = ubyteArrayOf(4u)
            val metadata = ubyteArrayOf(0u, 0u, 0u, 0u)

            val fullValue = ebidSize + ebidValue + metadataSize + metadata

            //check
            if (offset > fullValue.size) {
                bluetoothGattServer?.sendResponse(
                    device,
                    requestId,
                    BluetoothGatt.GATT_SUCCESS,
                    0,
                    byteArrayOf(0)
                )
                return
            }
            val size = fullValue.size - offset
            val response = UByteArray(size)
            for (i in offset until fullValue.size) {
                response[i - offset] = fullValue[i]
            }
            bluetoothGattServer?.sendResponse(
                device,
                requestId,
                BluetoothGatt.GATT_SUCCESS,
                0,
                response.asByteArray()
            )
            return
        }
    }


    fun initAdvertisementData() {


        val pair = GlobalSettings.advData.get()
        val cidBytes = ubyteArrayOf(
            (pair.second.value shr 24 and 0xffu).toUByte(),
            (pair.second.value shr 16 and 0xffu).toUByte(),
            (pair.second.value shr 8 and 0xffu).toUByte(),
            (pair.second.value shr 0 and 0xffu).toUByte()
        )

        val cidPart1 =
            String.format(
                Constants.uuidCidTemplate,
                cidBytes[0].toString(16),
                cidBytes[1].toString(16)
            )
        val cidPart2 =
            String.format(
                Constants.uuidCidTemplate,
                cidBytes[2].toString(16),
                cidBytes[3].toString(16)
            )

        synchronized(GlobalSettings.advDataLock) {
            val data = AdvertiseData.Builder()
                .setIncludeTxPowerLevel(true)
                .setIncludeDeviceName(true)
                .addServiceUuid(ParcelUuid(Constants.GATT_EBID_SERVICE))
                .addServiceUuid(ParcelUuid(UUID.fromString(cidPart1)))
                .addServiceUuid(ParcelUuid(UUID.fromString(cidPart2)))
                .build()
            currentAdvertisingSet.setAdvertisingData(data)
        }


        Log.i(TAG, "Rotate adv data with")
        Log.i(TAG, "  - EBID " + pair.first.ebid.toString())
        Log.i(TAG, "  - CID " + pair.second.toString())


        LogManager.record(
            recordType = LogManager.RECORD_TYPE.ADV,
            map = mutableMapOf(
                "message" to "Update EBID and CID in advertizer(s) " + pair.first.ebid.toString() + " " + pair.second.value.toString()
            )
        )

    }
}