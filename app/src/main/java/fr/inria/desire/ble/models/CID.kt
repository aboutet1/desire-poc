package fr.inria.desire.ble.models

import android.util.Log
import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.roundToInt


// mask to remove the first two bytes 00111111
val MASK_CID_BYTE0: UByte = 0x3Fu

// extension function for bytearray to generate UInt from 4 bytes
@ExperimentalUnsignedTypes
fun UByteArray.getUIntAt(idx: Int) =
    ((this[idx].toUInt() and 0xFFu) shl 24) or
            ((this[idx + 1].toUInt() and 0xFFu) shl 16) or
            ((this[idx + 2].toUInt() and 0xFFu) shl 8) or
            (this[idx + 3].toUInt() and 0xFFu)

@ExperimentalUnsignedTypes
fun ByteArray.getUIntAt(idx: Int) =
    ((this[idx].toUInt() and 0xFFu) shl 24) or
            ((this[idx + 1].toUInt() and 0xFFu) shl 16) or
            ((this[idx + 2].toUInt() and 0xFFu) shl 8) or
            (this[idx + 3].toUInt() and 0xFFu)


@ExperimentalUnsignedTypes
class CID() {
    var value: UInt = 0u

    companion object Factory {
        fun createFromByteArray(fullCid: UByteArray): CID {
            val newcid = CID()
            fullCid[0] = (fullCid[0].toUByte() and MASK_CID_BYTE0)
            newcid.value = fullCid.getUIntAt(0)
            return newcid
        }
    }

    fun generate() {
        // generate 32 random bits
        var cidBytes = ByteArray(4)
        Random().nextBytes(cidBytes)

        // clear the first two bits
        cidBytes[0] = (cidBytes[0].toUByte() and MASK_CID_BYTE0).toByte()
        this.value = cidBytes.getUIntAt(0)
    }
}