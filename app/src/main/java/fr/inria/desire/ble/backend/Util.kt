package fr.inria.desire.ble.backend

import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import java.security.MessageDigest
import kotlin.random.Random

fun sha256(input: ByteArray): ByteArray {
    val md = MessageDigest.getInstance("SHA-256")
    val digest = md.digest(input)
    return digest
}

fun sha256hex(base: String): String? {
    return try {
        val digest = MessageDigest.getInstance("SHA-256")
        val hash = digest.digest(base.toByteArray(charset("UTF-8")))
        val hexString = StringBuffer()
        for (i in hash.indices) {
            val hex = Integer.toHexString(0xff and hash[i].toInt())
            if (hex.length == 1) hexString.append('0')
            hexString.append(hex)
        }
        hexString.toString()
    } catch (ex: java.lang.Exception) {
        throw RuntimeException(ex)
    }
}

// Big Endian
fun int32toByteArray(value: Int): ByteArray {
    val bytes = ByteArray(4)
    bytes[3] = (value and 0xFFFF).toByte()
    bytes[2] = ((value ushr 8) and 0xFFFF).toByte()
    bytes[1] = ((value ushr 16) and 0xFFFF).toByte()
    bytes[0] = ((value ushr 24) and 0xFFFF).toByte()
    return bytes
}

fun uint32toByteArray(value: UInt): ByteArray {
    val bytes = ByteArray(4)
    bytes[3] = (value and 0xFFFFu).toByte()
    bytes[2] = ((value shr 8) and 0xFFFFu).toByte()
    bytes[1] = ((value shr 16) and 0xFFFFu).toByte()
    bytes[0] = ((value shr 24) and 0xFFFFu).toByte()
    return bytes
}

fun ByteArray.toHexString() =
    joinToString("") { (0xFF and it.toInt()).toString(16).padStart(2, '0') }

fun token_hash(input: BigInteger): BigInteger {
    val intBytes = input.toByteArray() //uint32toByteArray(input.toInt().toUInt())
    val intBytes32 = ByteArray(32)
    intBytes.copyInto(intBytes32, (32 - intBytes.size), 0, intBytes.size)
    val digestBytes = sha256(intBytes32)
    val finalInt = BigInteger(1, digestBytes)
    return finalInt
}


// Generates an inversible (modulo n) secret random number c
fun gen_secret(e: Int, n: BigInteger): Array<Any> {
    var secret_c: BigInteger = BigInteger.ZERO
    var inv_secret_c: BigInteger = BigInteger.ZERO

    while (true) {
        secret_c = Random.nextInt().toBigInteger()
        try {
            inv_secret_c = modinv(secret_c, n)
            break
        } catch (e: Exception) {
        }
    }

    // Generates 256-bits random secret number R (first part of authorization token)
    val secret_r = Random.nextInt().toBigInteger().abs()
    // Encode secret to send - decompose all operations
    val h = token_hash(secret_r).toBigDecimal()
    val tmp1 = (secret_c.toBigDecimal().pow(e) * h).toBigInteger()
    val tmp2 = tmp1.mod(n)
    val secret = tmp2
    return arrayOf(secret_r, secret_c, inv_secret_c, secret)
}

fun fdiv(i: BigInteger, d: BigInteger): BigInteger {
    val big0 = BigDecimal(i)
    val big1 = BigDecimal(d)
    val big2 = big0.divide(big1, 0, RoundingMode.FLOOR);
    return big2.toBigInteger();
}

fun egcd(a: BigInteger, b: BigInteger): Triple<BigInteger, BigInteger, BigInteger> {
    if (a == BigInteger.ZERO) {
        return Triple(b, BigInteger.ZERO, BigInteger.ONE)
    } else {
        var (g, y, x) = egcd(b % a, a)
        return Triple(g, x - fdiv(b, a) * y, y)
    }
}

// Utility functions to compute inverse modulo n
fun modinv(a: BigInteger, m: BigInteger): BigInteger {
    var (g, x, y) = egcd(a, m)
    if (g != BigInteger.ONE) {
        throw Exception("modular inverse does not exist")
    } else {
        try {
            var tmp1 = x.mod(m)
            return tmp1
        } catch (e: Exception) {
            throw e
        }
    }
}

