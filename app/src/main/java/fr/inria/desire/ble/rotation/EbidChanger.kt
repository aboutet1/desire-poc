package fr.inria.desire.ble.rotation

import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.managers.LogManager
import fr.inria.desire.ble.models.PET
import fr.inria.desire.ble.settings.GlobalSettings
import fr.inria.desire.ble.settings.ScannerSettings

// alarm callback class for part rotation
@ExperimentalUnsignedTypes
class EbidChanger {
    companion object {
        lateinit var caller: RotationManager
    }

    // alarm callback for EBID/CID rotation - may be started as scanner or advertiser (only conce)
    // First compute the PET to send to the server
    // then update the CID and EBID and then update the advertisement data if required
    fun onEbidAlarm() {

        Log.i(TAG, "Rotating EBID/SID alarm received")

        if (GlobalSettings.generatePET) {
            Log.i("PET", "Listing all EBID seen to search long contact..")
            for (contactEntry in ScannerSettings.seenEBID) {
                val contact = contactEntry.value
                val duration = contact.lastSeen - contact.firstSeen
                if (duration >= (GlobalSettings.longContactDuration * 60 * 1000)) {
                    Log.i("PET", "Long contact found with ${contactEntry.key}")
                    var pet: ByteArray? = null
                    if (contact.ebid.ebid != null) {
                        Log.d("PET", "ebid : ${contact.ebid}")
                        pet = CryptoManager.generatePet(
                            contact.ebid.ebid!!.asByteArray(),
                            ByteArray(0)
                        )
                    }
                    if (pet != null) {
                        GlobalSettings.createdPET.add(
                            PET(
                                pet,
                                pet
                            )
                        )

                        LogManager.record(
                            recordType = LogManager.RECORD_TYPE.SCAN,
                            map = mutableMapOf(
                                "message" to "pet generated"
                            )
                        )
                    } else
                        Log.w("PET", "error creating PET for ${contactEntry.key}")
                }
            }
        }

        // update CID/BID
        GlobalSettings.updateCidEbid()

        // update the advertizement data for services/connection mode
        if (!GlobalSettings.connectedMode) {
            for (advertiser in caller.advertizers) {
                advertiser.initAdvertisementData()
            }
        } else {
            caller.connAdvertizer.initAdvertisementData()
        }
    }

}