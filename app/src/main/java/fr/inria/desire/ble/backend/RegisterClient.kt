package fr.inria.desire.ble.backend

import android.util.Log
import com.google.gson.annotations.SerializedName
import fr.inria.desire.ble.activities.TAG
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface RegisterClientAPI {
    @GET(Backend.apiRegister)
    fun getRegister(@Query("phone_number") phone_number: String): Call<RegisterResponseGet>

    @Headers("Content-Type: application/json")
    @POST(Backend.apiRegister)
    fun postRegister(@Body postRegisterData: PostRegisterData): Call<RegisterResponsePost>

    @Headers("Content-Type: application/json")
    @PUT(Backend.apiRegister)
    fun putRegister(@Body putRegisterData: PutRegisterData): Call<RegisterResponsePut>
}

interface RegisterListener {
    fun getResponse(response: RegisterResponseGet)
    fun postResponse(response: RegisterResponsePost)
    fun putResponse(response: RegisterResponsePut)
    fun error(code: Int)
}


class RegisterResponseGet {
    @SerializedName("pin")
    var pin: String? = null

    @SerializedName("public_key")
    var public_key: List<String>? = null
}

class RegisterResponsePost {
    @SerializedName("rep")
    var rep: String? = null
}

class RegisterResponsePut {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("key")
    var key: String? = null
}

class PostRegisterData {
    @SerializedName("pin")
    var pin: String? = null

    @SerializedName("secret")
    var secret: String? = null
}

class GetRegisterData {
    @SerializedName("phone_number")
    var phone_number: String? = null
}

class PutRegisterData {
    @SerializedName("r")
    var r: String? = null

    @SerializedName("sigma")
    var sigma: String? = null
}


class RegisterClient(val caller: RegisterListener) {
    var service: RegisterClientAPI

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(Backend.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(RegisterClientAPI::class.java)
    }

    fun putRegister(r: String, sigma: String) {

        val putRegisterData: PutRegisterData = PutRegisterData()
        putRegisterData.r = r
        putRegisterData.sigma = sigma

        val call = service.putRegister(putRegisterData)
        call.enqueue(object : Callback<RegisterResponsePut> {
            override fun onResponse(
                call: Call<RegisterResponsePut>,
                response: Response<RegisterResponsePut>
            ) {
                if (response.code() == 200) {
                    val registerResponse = response.body()!!
                    Log.i(TAG, "putRegister response : " + registerResponse.toString())
                    caller.putResponse(registerResponse)
                } else {
                    Log.i(
                        TAG,
                        "putRegister unable to proceed - error code : " + response.code().toString()
                    )
                    caller.error(response.code())
                }
            }

            override fun onFailure(call: Call<RegisterResponsePut>, t: Throwable) {
                Log.w(TAG, "putRegister call failure")
                caller.error(-1)
            }
        })
    }

    fun postRegister(pin: String, secret: String) {

        val postRegisterData: PostRegisterData = PostRegisterData()
        postRegisterData.pin = pin
        postRegisterData.secret = secret

        val call = service.postRegister(postRegisterData)
        call.enqueue(object : Callback<RegisterResponsePost> {
            override fun onResponse(
                call: Call<RegisterResponsePost>,
                response: Response<RegisterResponsePost>
            ) {
                if (response.code() == 200) {
                    val registerResponse = response.body()!!
                    Log.i(TAG, "postRegister response : " + registerResponse.toString())
                    caller.postResponse(registerResponse)
                } else {
                    Log.i(
                        TAG,
                        "postRegister unable to proceed - error code : " + response.code()
                            .toString()
                    )
                    caller.error(response.code())
                }
            }

            override fun onFailure(call: Call<RegisterResponsePost>, t: Throwable) {
                Log.w(TAG, "postRegister call failure")
                caller.error(-1)
            }
        })
    }

    fun getRegister(phone_number: String) {
        val call = service.getRegister(phone_number)
        call.enqueue(object : Callback<RegisterResponseGet> {
            override fun onResponse(
                call: Call<RegisterResponseGet>,
                response: Response<RegisterResponseGet>
            ) {
                if (response.code() == 200) {
                    val registerResponse = response.body()!!
                    Log.i(TAG, "getRegister response : " + registerResponse.toString())
                    caller.getResponse(registerResponse)
                } else {
                    Log.i(
                        TAG,
                        "getRegister unable to proceed - error code : " + response.code()
                            .toString() + " " + response.message().toString()
                    )
                    caller.error(response.code())
                }
            }

            override fun onFailure(call: Call<RegisterResponseGet>, t: Throwable) {
                Log.w(TAG, "getRegister call failure")
                caller.error(-1)
            }
        })
    }

}