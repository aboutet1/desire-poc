package fr.inria.desire.ble.services

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertisingSet
import android.bluetooth.le.AdvertisingSetCallback
import android.bluetooth.le.AdvertisingSetParameters
import android.os.ParcelUuid
import android.util.Log
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.managers.LogManager
import fr.inria.desire.ble.interfaces.Startable
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.models.AdvData
import fr.inria.desire.ble.models.AdvPayload
import fr.inria.desire.ble.models.CID
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings
import java.util.*

@ExperimentalUnsignedTypes
class AdvertiserServices(
    adapter: BluetoothAdapter,
    uuid: String
) : Startable {
    private var btAdapter: BluetoothAdapter = adapter
    private var uuidService: String = uuid
    private var serviceSliceIndex: Int? = null
    private var doInit: Boolean = true

    private lateinit var currentAdvertisingSet: AdvertisingSet
    private lateinit var dataAdv: AdvertiseData
    private lateinit var parametersAdv: AdvertisingSetParameters

    init {
        if (doInit) {
            initAdvertisementData()
        }
    }

    constructor(
        adapter: BluetoothAdapter,
        uuid: String,
        doInit: Boolean,
        serviceIndex: Int
    ) : this(adapter, uuid) {
        serviceSliceIndex = serviceIndex
        if (doInit) {
            initAdvertisementData()
        }
    }

    override var started: Boolean = false

    override fun start() {
        Log.i(TAG, "Start an advertiser ")

        Log.i(TAG, "  - new uuidService $uuidService")
        Log.i(TAG, "  - and advData " + GlobalSettings.advData.toString())

        // record info in csv
        LogManager.record(
            recordType = LogManager.RECORD_TYPE.ADV,
            map = mutableMapOf(
                "message" to "advertise start $uuidService"
            )
        )

        val btAdvertiser = btAdapter.bluetoothLeAdvertiser

        parametersAdv = AdvertisingSetParameters.Builder()
            .setLegacyMode(true)
            .setInterval(AdvertiserSettings.interval)
            .setTxPowerLevel(AdvertiserSettings.txPowerLevel)
            .setPrimaryPhy(BluetoothDevice.PHY_LE_1M)
            .setSecondaryPhy(BluetoothDevice.PHY_LE_2M)
            .build()

        val pUuid = ParcelUuid(UUID.fromString(uuidService))

        val sliceIndex: Int
        if (this.serviceSliceIndex == null) {
            sliceIndex = GlobalSettings.currentRotationPart
            Log.i(TAG, "Adv start with in rotation mode")
        } else {
            sliceIndex = this.serviceSliceIndex!!
            Log.i(TAG, "Adv start with in multi-service mode")
        }

        synchronized(GlobalSettings.advDataLock) {

            val payload: AdvPayload = GlobalSettings.advData.getPayloads(sliceIndex)

            Log.v(TAG, "adv sliceIndex (starts at 1..) $sliceIndex")
            Log.v(TAG, "adv parcelData cid " + payload.cid.toString())
            Log.v(TAG, "padv arcelData sid " + payload.sid.toString())
            Log.v(TAG, "adv parcelData ebid slice " + payload.ebid_slice.toString())
            Log.v(TAG, "adv parcelData version " + payload.md_version.toString())

            val arrays = payload.toUByteArrays()
            val serviceData: UByteArray = arrays.first + arrays.second

            Log.v(TAG, "adv first " + arrays.first.toString())
            Log.v(TAG, "adv second " + arrays.second.toString())
            Log.v(TAG, "adv service data $serviceData")
            Log.v(TAG, "adv service data size " + serviceData.size)

            dataAdv = AdvertiseData.Builder()
                .setIncludeTxPowerLevel(true)
                .setIncludeDeviceName(false)
                .addServiceUuid(pUuid)
                .addServiceData(pUuid, serviceData.asByteArray())
                .build()
        }

        //Callbacks
        val callback = object : AdvertisingSetCallback() {
            override fun onAdvertisingSetStarted(
                advertisingSet: AdvertisingSet?,
                txPower: Int,
                status: Int
            ) {
                // stores the Tx power level for computing the metdata gtx param
                Log.i(
                    TAG, "onAdvertisingSetStarted(): txPower:" + txPower + " , status: "
                            + status + " aa: " + advertisingSet
                )
                AdvertiserSettings.tx_power = txPower

                // record info in csv
                LogManager.record(
                    recordType = LogManager.RECORD_TYPE.ADV,
                    map = mutableMapOf(
                        "myTx" to txPower
                    )
                )

                if (advertisingSet != null) {
                    Log.e(TAG, "Callback AdvertisingSet set")
                    currentAdvertisingSet = advertisingSet
                } else {
                    Log.e(TAG, "ERROR? : Callback did not provide AdvertisingSet ! ")
                }

            }

            override fun onAdvertisingDataSet(
                advertisingSet: AdvertisingSet?,
                status: Int
            ) {
                Log.i(TAG, "onAdvertisingDataSet(): status=$status")
            }

            override fun onAdvertisingParametersUpdated(
                advertisingSet: AdvertisingSet?,
                txPower: Int,
                status: Int
            ) {
                Log.i(TAG, "onAdvertisingParametersUpdated(): status=$status")
            }

            override fun onAdvertisingSetStopped(advertisingSet: AdvertisingSet?) {
                Log.i(TAG, "onAdvertisingSetStopped():")
            }
        }

        //
        Log.i(TAG, "Start the advertising set")
        btAdvertiser.startAdvertisingSet(parametersAdv, dataAdv, null, null, null, callback)
    }

    fun rotatePart() {

        Log.i(
            TAG,
            "Start rotatePart, with part to advertize is ${GlobalSettings.currentRotationPart}"
        )

        // record info in csv
        LogManager.record(
            recordType = LogManager.RECORD_TYPE.ADV,
            map = mutableMapOf(
                "message" to "rotate part, now $uuidService"
            )
        )

        val pUuid = ParcelUuid(UUID.fromString(uuidService))
        synchronized(GlobalSettings.advDataLock) {
            try {
                val payload: AdvPayload =
                    GlobalSettings.advData.getPayloads(GlobalSettings.currentRotationPart)

                Log.v(TAG, "adv indexToUse (starts at 1..) ${GlobalSettings.currentRotationPart}")
                Log.v(TAG, "adv parcelData cid " + payload.cid.toString())
                Log.v(TAG, "adv parcelData sid " + payload.sid.toString())
                Log.v(TAG, "adv parcelData ebid slice " + payload.ebid_slice.toString())
                Log.v(TAG, "adv parcelData version " + payload.md_version.toString())

                val arrays = payload.toUByteArrays()
                val serviceData: UByteArray = arrays.first + arrays.second

                Log.v(TAG, "adv first " + arrays.first.toString())
                Log.v(TAG, "adv second " + arrays.second.toString())
                Log.v(TAG, "adv service data $serviceData")
                Log.v(TAG, "adv service data size " + serviceData.size)

                dataAdv = AdvertiseData.Builder()
                    .setIncludeTxPowerLevel(true)
                    .setIncludeDeviceName(false)
                    .addServiceUuid(pUuid)
                    .addServiceData(pUuid, serviceData.asByteArray())
                    .build()

                currentAdvertisingSet.setAdvertisingData(dataAdv)

            } catch (e: Exception) {
                Log.e(TAG, "ERROR in setAdvertisingData " + e.message)
            }
        }

    }

    /**
     *
     */
    fun initAdvertisementData() {
        Log.i(TAG, "Start updateEbid with")
        Log.i(TAG, "  - uuidService $uuidService")

        // StopCovid service UUID
        val pUuid = ParcelUuid(UUID.fromString(uuidService))

        // use index as either the service number or the rotation part to send
        val indexToUse = if (this.serviceSliceIndex == null) {
            1 + GlobalSettings.currentRotationPart
        } else {
            this.serviceSliceIndex!!
        }

        Log.i(TAG, "updateSidEbid - update advertising set")

        synchronized(GlobalSettings.advDataLock) {
            try {
                val payload: AdvPayload? = GlobalSettings.advData.getPayloads(indexToUse)

                Log.v(TAG, "indexToUse (starts at 1..) ${GlobalSettings.currentRotationPart}")
                Log.v(TAG, "parcelData cid " + payload!!.cid.toString())
                Log.v(TAG, "parcelData sid " + payload.sid.toString())
                Log.v(TAG, "parcelData ebid slice " + payload.ebid_slice.toString())
                Log.v(TAG, "parcelData version " + payload.md_version.toString())

                val arrays = payload.toUByteArrays()
                val serviceData: UByteArray = arrays.first + arrays.second

                Log.v(TAG, "first " + arrays.first.toString())
                Log.v(TAG, "second " + arrays.second.toString())
                Log.v(TAG, "adv service data $serviceData")
                Log.v(TAG, "adv service data size " + serviceData.size)

                dataAdv = AdvertiseData.Builder()
                    .setIncludeTxPowerLevel(true)
                    .setIncludeDeviceName(false)
                    .addServiceUuid(pUuid)
                    .addServiceData(pUuid, serviceData.asByteArray())
                    .build()
                currentAdvertisingSet.setAdvertisingData(dataAdv)
            } catch (e: Exception) {
                Log.e(TAG, "ERROR in setAdvertisingData " + e.message)
            }
        }

    }

}