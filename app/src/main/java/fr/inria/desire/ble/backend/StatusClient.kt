package fr.inria.desire.ble.backend

import android.util.Log
import com.google.gson.annotations.SerializedName
import fr.inria.desire.ble.activities.TAG
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface StatusClientAPI {
    @GET(Backend.apiStatus)
    fun getStatus(
        @Query("id") id: String,
        @Query("key") key: String,
        @Query("pets") pets: List<String>
    ): Call<StatusResponseGet>
}

interface StatusListener {
    fun response(response: StatusResponseGet)
    fun error(code: Int)
}

class StatusResponseGet {
    @SerializedName("at_risk")
    var at_risk: Boolean? = null
}

class StatusClient(val caller: StatusListener) {
    var service: StatusClientAPI

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(Backend.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(StatusClientAPI::class.java)
    }


    fun getStatus(id: String, key: String, pets: List<String>) {

        val call = service.getStatus(id, key, pets)
        call.enqueue(object : Callback<StatusResponseGet> {
            override fun onResponse(
                call: Call<StatusResponseGet>,
                response: Response<StatusResponseGet>
            ) {
                if (response.code() == 200) {
                    val statusResponse = response.body()!!
                    Log.i(TAG, "status response : " + statusResponse.toString())
                    caller.response(statusResponse)
                } else {
                    Log.i(
                        TAG,
                        "getStatus unable to proceed - error code : " + response.code().toString()
                    )
                    caller.error(response.code())
                }
            }

            override fun onFailure(call: Call<StatusResponseGet>?, t: Throwable?) {
                Log.w(TAG, "getStatus call failure")
                caller.error(-1)
            }
        })
    }

}