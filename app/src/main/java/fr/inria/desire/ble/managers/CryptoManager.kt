package fr.inria.desire.ble.managers

import android.util.Log
import de.frank_durr.ecdh_curve25519.ECDHCurve25519
import fr.inria.desire.ble.activities.TAG
import java.security.MessageDigest
import java.security.SecureRandom

object CryptoManager {
    init {
        // Load native library ECDH-Curve25519-Mobile implementing Diffie-Hellman key
        // exchange with elliptic curve 25519.
        try {
            System.loadLibrary("ecdhcurve25519")
            //Log.i(TAG, "Loaded ecdhcurve25519 library.")
        } catch (e: UnsatisfiedLinkError) {
            //Log.e(TAG, "Error loading ecdhcurve25519 library: " + e.message)
        }
    }

    var secretKey: ByteArray? = null
    var publicKey: ByteArray? = null

    fun generateKeys() {
        val random = SecureRandom()
        secretKey = ECDHCurve25519.generate_secret_key(random)
        // Create public key.
        publicKey =
            ECDHCurve25519.generate_public_key(secretKey)
    }

    fun getKeys(): Pair<ByteArray, ByteArray> {
        return Pair(secretKey!!.clone(), publicKey!!.clone())
    }

    fun setKeys(secretKey: ByteArray, publicKey: ByteArray) {
        this.secretKey = secretKey.clone()
        this.publicKey = publicKey.clone()
    }

    fun generatePet(bobPublicKey: ByteArray, prefix: ByteArray): ByteArray? {
        if (secretKey != null && publicKey != null) {
            val md = MessageDigest.getInstance("SHA-256")
            return md.digest(
                prefix + ECDHCurve25519.generate_shared_secret(
                    secretKey,
                    bobPublicKey
                )
            )
        }
        return null
    }


    fun fakeEBID(): ByteArray {
        val random = SecureRandom()
        return ECDHCurve25519.generate_public_key(ECDHCurve25519.generate_secret_key(random))
    }
}