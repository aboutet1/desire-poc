package fr.inria.desire.ble.models

import java.lang.Math.log
import kotlin.math.exp
import kotlin.math.ln
import kotlin.math.roundToInt

const val PEAK_THRESHOLD: Int = 0
var a = 10 / ln(10.0f)

class RTLWindow() {

    val values: MutableList<Int> = ArrayList()
    var clippedValue = 0
    var ts: Long = 0
    fun clipping() {
        for (index in 0 until values.size) {
            if (values[index] > PEAK_THRESHOLD) {
                values[index] = PEAK_THRESHOLD
                clippedValue++
            }
        }
    }

    fun fading(): Pair<Int, Int> {
        var q = 0.0f
        val n: Int = values.size

        if (values.size == 0) {
            throw Exception("No values defined")
        }

        for (index in 0 until values.size) {
            q += exp(values[index] / a)
        }
        q /= n
        q = a * ln(q)

        return Pair(q.roundToInt(), n)
    }
}