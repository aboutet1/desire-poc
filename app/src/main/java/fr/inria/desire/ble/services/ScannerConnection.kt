package fr.inria.desire.ble.services

import android.bluetooth.*
import android.bluetooth.BluetoothDevice.*
import android.bluetooth.BluetoothGatt.GATT_SUCCESS
import android.bluetooth.le.*
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.ParcelUuid
import android.util.Log
import fr.inria.desire.ble.settings.ScannerSettings
import fr.inria.desire.ble.interfaces.Startable
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.models.CID
import fr.inria.desire.ble.models.ContactedEbid
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.RTLData
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.BlockingDeque
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.ArrayList
import kotlin.concurrent.withLock


const val SCAN_ALL_LOOP_DELAY: Long = 10000
const val SCAN_SINGLE_LOOP_DELAY: Long = 200
const val ONE_SECOND: Long = 1000

const val CHARACTERISTIC_READ_DELAY: Long = 100

const val SCNA_SINGLE_STOP_DELAY: Long = 100

@ExperimentalUnsignedTypes
class ScannerConnection(
    private var btAdapter: BluetoothAdapter,
    private var ctx: Context,
    private var uuidMask: String,
    scannerUIUpdate: ((Int, String) -> Unit)
) : Startable {
    private var uiUpdateCallback: ((Int, String) -> Unit)? = scannerUIUpdate

    private var mBluetoothLeScanner: BluetoothLeScanner = btAdapter.bluetoothLeScanner

    private var nbAdvReceived: Int = 0


    private var bleHandler: Handler = Handler()

    private lateinit var mRunnable: Runnable

    private var characHandler: Handler = Handler()

    private var scanLoop: Boolean = false

    private val btLock = ReentrantLock()
    private val btCondition = btLock.newCondition()
    private var btIsUsed: Boolean = false


    private val connectLock = ReentrantLock()
    private val connectCondition = connectLock.newCondition()
    private var connectIsUsed: Boolean = false

    // temporary (scan period) list of devices around to check
    private val discoveredDevices: BlockingDeque<SeenDevice> = LinkedBlockingDeque()

    // temporary
    private lateinit var currentConnectedDevice: SeenDevice

    private val localCallbackAll: DeviceScanCallback =
        DeviceScanCallback(
            discoveredDevices,
            "ALL"
        )
    private val localCallbackSingle: DeviceScanCallback =
        DeviceScanCallback(
            discoveredDevices,
            "SINGLE"
        )
    override var started: Boolean = false
    //
    //
    //

    override fun start() {

        if (!scanLoop)
            scanLoop = true

        GlobalScope.launch {
            loopScan()
        }
        GlobalScope.launch {
            loopConnect()
        }
    }

    fun stopScanner() {
        scanLoop = false
    }

    //
    //
    //


    suspend fun loopScan() {
        Log.v(TAG, "Start scan loop task")

        while (scanLoop) {
            Log.d(TAG, "Do scan loop")

            // debug msgs
            displayDiscoveredDevices()
            displayContactedDevices()

            reserveBluetooth()
            synchronized(discoveredDevices) {
                discoveredDevices.clear()
            }
            scanForAll()
            // wait for 10
            delay(SCAN_ALL_LOOP_DELAY)
            releaseBluetooth()
            delay(ONE_SECOND)
        }
    }

    //
    suspend fun loopConnect() {

        delay(ONE_SECOND)

        while (scanLoop) {
            Log.i(TAG, "Do connect loop")

            reserveBluetooth()
            // start from the first device in the newly doscovered list
            batchConnectProcessing()
            releaseBluetooth()

            delay(SCAN_SINGLE_LOOP_DELAY)
        }
    }

    suspend private fun batchConnectProcessing() {

        // only if items in the list, otherwise skip
        // takeFirst is blocking. need to prevent locking connect/bluetoooth
        if (discoveredDevices.size > 0) {
            val tmpDevice = discoveredDevices.takeFirst()
            val tmpCID = tmpDevice.cid
            val device = tmpDevice.device

            ScannerSettings.addDevice(tmpCID)
            // make sure no other connect-related task happens
            reserveConnect()

            Log.i(TAG, "processing device ${device?.address} with cid ${tmpCID}")

            if (!ScannerSettings.seenEBID.containsKey(tmpCID)) {
                Log.d(TAG, "New device, connecting to : ${device?.address} for service ${tmpCID}")
                currentConnectedDevice = tmpDevice

                if (device?.type == 0) {
                    Log.d(TAG, "unknown device (not in cache), scan with address and no delay")
                    scanForDevice(device.address.toString())
                    releaseConnect()
                    return
                } else {
                    Log.i(TAG, "Device in cache")
                }

                scanningMode = 0
                mBluetoothLeScanner.stopScan(localCallbackSingle)

                Log.v(TAG, "Create and launch gatt connection handler")
                val mHandler = Handler(ctx.mainLooper)
                mHandler.postDelayed({
                    Log.v(TAG, "Start gatt connection")

                    val bluetoothGatt = when {
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                            Log.d(TAG, "connectgatt (M)")

                            val adapterCachedDevice: BluetoothDevice =
                                btAdapter.getRemoteDevice(currentConnectedDevice.device?.toString())

                            adapterCachedDevice.connectGatt(
                                ctx.applicationContext,
                                false,
                                btleGattCallback,
                                TRANSPORT_LE
                            )
                        }
                        else -> {
                            Log.d(TAG, "connectgatt (pre-M)")
                            val adapterCachedDevice: BluetoothDevice =
                                btAdapter.getRemoteDevice(currentConnectedDevice.device?.toString())
                            adapterCachedDevice?.connectGatt(
                                ctx.applicationContext,
                                false,
                                btleGattCallback
                            )
                        }
                    }
                    // no need to connect as already done in the connectGatt. If the gatt object
                    // is stored, and later reused, the connect API may be used to reconnect to the gatt server
                }, 10)

            } else {
                Log.d(TAG, "already contacted, update timestamp")
                // update last seen
                ScannerSettings.seenEBID[tmpCID]?.seen()
                releaseConnect()
            }
        }
    }

    // companion object to handle all Gatt callbacks
    private val btleGattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            var discoverServicesRunnable: Runnable?
            Log.d(TAG, "onConnectionStateChange : status $status newState $newState")

            if (status == GATT_SUCCESS) {
                Log.v(TAG, "onConnectionStateChange : GATT_SUCCESS")

                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Log.v(TAG, "onConnectionStateChange : STATE_CONNECTED")

                    val bondstate: Int = currentConnectedDevice.device!!.bondState

                    if (bondstate == BOND_NONE || bondstate == BOND_BONDED) {

                        Log.v(TAG, "onConnectionStateChange : BOND_NONE BOND_BONDED")

                        // Connected to device, now proceed to discover it's services but delay a bit if needed
                        var delayWhenBonded = 0
                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                            delayWhenBonded = 1000
                        }
                        val delay = if (bondstate == BOND_BONDED) delayWhenBonded else 0
                        discoverServicesRunnable = Runnable {
                            Log.d(TAG, "discovering services")
                        }
                        val result = gatt.discoverServices()
                        if (!result) {
                            Log.e(TAG, "discoverServices failed to start")
                            discoverServicesRunnable = null
                        }
                        bleHandler.postDelayed(discoverServicesRunnable!!, 0)
                    } else if (bondstate == BOND_BONDING) {
                        // Bonding process in progress, let it complete
                        Log.v(TAG, "onConnectionStateChange : BOND_BONDING (wait to complete)")
                    } else {
                        Log.d(TAG, "bonong error ? ignore for now")
                    }

                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    // We successfully disconnected on our own request
                    Log.v(TAG, "successfully disconnected on our own request")
                    gatt.close()
                    releaseConnect()
                } else {
                    // We're CONNECTING or DISCONNECTING, ignore for now
                    Log.v(TAG, "CONNECTING or DISCONNECTING, ignore for now")
                }
            } else {
                Log.d(TAG, "onConnectionStateChange : error happened...figure out what happened")
                // An error happened...figure out what happened!
                gatt.close()
                releaseConnect()
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {

            Log.d(TAG, "onServicesDiscovered : status $status gatt $gatt")
            if (gatt == null) {
                Log.e(TAG, "ERROR: Gatt is 'null', ignoring read request")
                return
            }

            Log.d(TAG, "onServicesDiscovered : nb services " + gatt.getServices().size)
            if (gatt.services.size == 0) {
                gatt.disconnect()
                return
            }

            for (service in gatt.getServices()) {
                Log.d(TAG, "onServicesDiscovered : services " + service.uuid)
            }

            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(
                    TAG,
                    "onServicesDiscovered readCharacteristic : EBID_SERVICE ${Constants.GATT_EBID_SERVICE} EBID_CHARA ${Constants.GATT_EBID_CHARA}"
                )

                // prepare to read the CHARA characteristics for the EBID service
                val service: BluetoothGattService = gatt.getService(Constants.GATT_EBID_SERVICE)
                var characteristics = service.characteristics
                Log.d(TAG, "onServicesDiscovered : nb characteristics " + characteristics.size)
                for (charac in characteristics) {
                    Log.d(TAG, "onServicesDiscovered : char " + charac.uuid)
                }

                val characteristic: BluetoothGattCharacteristic =
                    service.getCharacteristic(Constants.GATT_EBID_CHARA)

                if (characteristic == null) {
                    Log.e(TAG, "ERROR: characteristic is 'null', ignoring read request")
                    gatt.disconnect()
                    return
                }

                // prepare and launch the thread for the characteristics read
                mRunnable = Runnable {
                    Log.d(TAG, "onServicesDiscovered launch runnable for readCharacteristic")
                    gatt.readCharacteristic(characteristic)
                }
                characHandler.postDelayed(
                    mRunnable,
                    CHARACTERISTIC_READ_DELAY
                )
            } else {
                Log.d(TAG, "error on service discovery")
                gatt.disconnect()
            }
        }

        // Result of a characteristic read operation
        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            Log.d(TAG, "onCharacteristicRead : status $status gatt $gatt")
            if (gatt != null) {
                Log.d(TAG, "onCharacteristicRead gatt not null\n")
                if (status == BluetoothGatt.GATT_SUCCESS) {

                    Log.d(TAG, "onCharacteristicRead Read Characteristic success\n")
                    Log.d(TAG, "onCharacteristicRead gatt device address ${gatt.device.address}")
                    Log.d(
                        TAG,
                        "onCharacteristicRead currentConnectSidevice device address ${currentConnectedDevice.device?.address}"
                    )
                    Log.d(TAG, "onCharacteristicRead gatt device address ${gatt.device.address}")

                    val cid = currentConnectedDevice.cid
                    Log.d(TAG, "onCharacteristicRead .. use cid ${cid}")

                    val fulldata = characteristic!!.value.asUByteArray()
                    val ebidSize = fulldata[0].toInt()
                    val ebidValue: UByteArray = fulldata.copyOfRange(1, 1 + ebidSize)
                    val metadataSize = fulldata[1 + ebidSize].toInt()
                    val metadata = fulldata.copyOfRange(2 + ebidSize, 2 + ebidSize + metadataSize)

                    Log.d(TAG, "onCharacteristicRead .. ebidValue " + ebidValue.toString())
                    Log.d(TAG, "onCharacteristicRead .. metadata " + metadata.toString())

                    Log.d(
                        TAG,
                        "onCharacteristicRead .. EBID " + EBID.createFromUByteArray(ebidValue)
                    )

                    ScannerSettings.seenEBID.set(
                        currentConnectedDevice.cid,
                        ContactedEbid(
                            EBID.createFromUByteArray(
                                ebidValue
                            )
                        )
                    )

                    Log.d(
                        TAG,
                        "Characteristic address ${gatt.device.address} Value : " + ScannerSettings.seenEBID[cid].toString()
                    )
                    nbAdvReceived++
                    uiUpdateCallback?.invoke(1, "$nbAdvReceived connection created")
                    gatt.disconnect()
                } else {
                    Log.w(TAG, "onCharacteristicRead Read Characteristic failed !")
                    gatt.disconnect()
                }
            } else {
                Log.e(TAG, "onCharacteristicRead gatt null !")
            }
        }


    }

    //
    //
    //

    var scanningMode = 0

    // launch scan for all devices
    suspend private fun scanForAll() {
        synchronized(mBluetoothLeScanner) {
            Log.i(TAG, "Scan 10 sec for all devices")

            if (scanningMode != 1) {

                mBluetoothLeScanner.flushPendingScanResults(localCallbackAll)
                mBluetoothLeScanner.stopScan(localCallbackAll)

                val settings = ScanSettings.Builder()
                    .setScanMode(ScannerSettings.scanMode)
                    .setReportDelay(SCAN_ALL_LOOP_DELAY)
                    //.setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                    //.setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                    //.setNumOfMatches(ScanSettings.MATCH_NUM_FEW_ADVERTISEMENT)
                    .build()
                val filter = ScanFilter.Builder()
                    .setServiceUuid(ParcelUuid(Constants.GATT_EBID_SERVICE))
                    .build()
                val filters = ArrayList<ScanFilter>()
                filters.add(filter)
                mBluetoothLeScanner.startScan(filters, settings, localCallbackAll)
                scanningMode = 1
            }
        }
    }

    // launch scan for a specific device
    suspend private fun scanForDevice(address: String) {
        synchronized(mBluetoothLeScanner) {
            Log.i(TAG, "Scan 0 sec for specific device ${address}")

            if (scanningMode != 2) {
                mBluetoothLeScanner.stopScan(localCallbackSingle)

                val settings = ScanSettings.Builder()
                    .setScanMode(ScannerSettings.scanMode)
                    .setReportDelay(0)
                    //.setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                    //.setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                    //.setNumOfMatches(ScanSettings.MATCH_NUM_FEW_ADVERTISEMENT)
                    .build()
                val filter = ScanFilter.Builder()
                    .setDeviceAddress(address)
                    .build()
                val filters = ArrayList<ScanFilter>()
                filters.add(filter)
                mBluetoothLeScanner.startScan(filters, settings, localCallbackSingle)

                scanningMode = 2
            }
        }
    }

    // primary scan callback - store results and process asynchronously
    // for both all/single device discovery modes
    class DeviceScanCallback(
        val discoveredDevices: BlockingDeque<SeenDevice>,
        val name: String
    ) : ScanCallback() {
        //Connection Mode should be Batch only - 60s minimum
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)

            Log.v(TAG, "onScanResult ${name}")
            if (result != null) {
                processBTScanResult(result)
            }
            Log.v(TAG, "onScanResult completed")
        }

        override fun onBatchScanResults(results: List<ScanResult?>?) {
            super.onBatchScanResults(results)

            Log.v(TAG, "onBatchScanResults ${name}")
            if (results != null) {
                for (result in results) {
                    if (result != null) {
                        processBTScanResult(result)
                    }
                }
            }
            Log.v(TAG, "onBatchScanResults completed")
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            // error code 1: SCAN_FAILED_ALREADY_STARTED
            Log.e(TAG, "onScanFailed ${name}: $errorCode")
        }

        private fun processBTScanResult(result: ScanResult) {

            Log.v(TAG, "processScanResult")

            // requires a device with at least 3 uuids. One UUID if for the StopCovid service, the others
            // UUID is for the generated CID (so that we do not connect multiple times for the same
            // device
            // type can be 0 or 2, if 0 (unknown, then another scan will be done later).
            if (result.scanRecord?.serviceUuids != null && result.scanRecord?.serviceUuids!!.size >= 3) {
                Log.d(
                    TAG,
                    "process scan result device ${result.device.address} type ${result.device.type} uuids: " + result.scanRecord?.serviceUuids?.toString()
                )

                // assume UUID received in the same order (EBID first, CID second/third)
                val puuid = result.scanRecord?.serviceUuids?.get(0).toString()
                if (puuid.length < 8 || !puuid.substring(0, 8)
                        .toLowerCase(Locale.ROOT)
                        .equals(Constants.GATT_EBID_SERVICE_SHORTMASK)
                ) {
                    Log.v(
                        TAG,
                        "processScanResult: filtering out - bad uuid " + puuid.substring(0, 8)
                            .toLowerCase(Locale.ROOT)
                    )
                    return
                }

                val byte0 = result.scanRecord?.serviceUuids?.get(1).toString().substring(4, 6)
                val byte1 = result.scanRecord?.serviceUuids?.get(1).toString().substring(6, 8)
                val byte2 = result.scanRecord?.serviceUuids?.get(2).toString().substring(4, 6)
                val byte3 = result.scanRecord?.serviceUuids?.get(2).toString().substring(6, 8)


                val cidBytes = ubyteArrayOf(
                    byte0.toUByte(16),
                    byte1.toUByte(16),
                    byte2.toUByte(16),
                    byte3.toUByte(16)
                )
                val seenCID = CID.createFromByteArray(cidBytes)

                // create the device with the second uuid as identifier
                val newDevice =
                    SeenDevice(
                        seenCID.value.toString(),
                        result.device
                    )

                Log.v(TAG, "processScanResult seen device " + seenCID.value.toString())

                synchronized(discoveredDevices) {
                    if (!discoveredDevices.contains(newDevice)) {
                        discoveredDevices.add(newDevice)
                        if (ScannerSettings.seenEBID.containsKey(newDevice.cid)) {
                            val tmpEbid = ScannerSettings.seenEBID[newDevice.cid]!!.ebid
                            if (!ScannerSettings.EBIDdata.containsKey(tmpEbid)) {
                                val tmprtl = RTLData()
                                tmprtl.computeObf(GlobalSettings.localEbid, tmpEbid)
                                tmprtl.startTimestamp = System.currentTimeMillis()
                                ScannerSettings.EBIDdata[tmpEbid] = tmprtl

                            }
                            val rtl = ScannerSettings.EBIDdata[tmpEbid]
                            rtl?.add(
                                result.rssi - AdvertiserSettings.rx_bias - rtl.obf,
                                System.currentTimeMillis()
                            )
                        }

                        Log.d(
                            TAG,
                            "processScanResult: not yet in the list type is ${newDevice.device?.type}, add it. new size is " + discoveredDevices.size
                        )
                    } else {
                        Log.d(
                            TAG,
                            "processScanResult: already in the list .... type is ? " + newDevice.device?.type
                        )
                    }
                }

            } else {
                Log.v(TAG, "processScanResult: Invalid result, discard (no uuid)")
            }
        }

    }


    //
    //
    //

    private fun reserveBluetooth() {
        Log.v(TAG, "synchro reserveBluetooth")
        btLock.withLock {
            while (btIsUsed) {
                btCondition.await()
            }
            btIsUsed = true
        }
    }

    private fun releaseBluetooth() {
        Log.v(TAG, "synchro releaseBluetooth")
        btLock.withLock {
            btIsUsed = false
            btCondition.signalAll()
        }
    }

    private fun reserveConnect() {
        Log.v(TAG, "synchro reserveConnect")
        connectLock.withLock {
            while (connectIsUsed) {
                connectCondition.await()
            }
            connectIsUsed = true
        }
    }

    private fun releaseConnect() {
        Log.v(TAG, "synchro releaseConnect")
        connectLock.withLock {
            connectIsUsed = false
            connectCondition.signalAll()
        }
    }

    //
    //
    //

    private fun displayDiscoveredDevices() {
        synchronized(discoveredDevices) {
            Log.i(TAG, "## Display discovered devices")
            for (device in discoveredDevices) {
                Log.i(
                    TAG,
                    "- device ${device.device} sid ${device.cid}"
                )
            }
        }
    }

    private fun displayContactedDevices() {
        synchronized(discoveredDevices) {
            Log.i(TAG, "## Display contacted devices")
            for (device in ScannerSettings.seenEBID) {
                Log.i(
                    TAG,
                    "- device ${device} value ${device.value}"
                )
            }
        }
    }

    //
    //
    //

    class SeenDevice(val cid: String, val device: BluetoothDevice?) {
        override fun equals(other: Any?): Boolean {
            if (other is SeenDevice) {
                var same =
                    ((other.cid == this.cid) && (other.device?.address == this.device?.address))

                if (same) {
                    if (other.device?.type != this.device?.type) {
                        if (other.device?.type != 0) {
                            Log.w(
                                TAG,
                                "SeenDevice initially same but type different and new not unknow"
                            )
                            same = false
                        }
                    }
                }
                return same
            }
            return false
        }

        override fun hashCode(): Int {
            var result = cid.hashCode()
            result = 31 * result + (device?.hashCode() ?: 0)
            return result
        }
    }

    //
    //
    //


}