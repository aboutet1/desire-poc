package fr.inria.desire.ble.interfaces


interface Startable {

    var started: Boolean
    fun start()

}