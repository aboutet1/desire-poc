package fr.inria.desire.ble.managers

import android.app.KeyguardManager
import android.content.Context
import android.icu.text.SimpleDateFormat
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.BatteryManager
import android.os.Build
import android.os.PowerManager
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import fr.inria.desire.ble.BuildConfig
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.settings.GlobalSettings
import java.io.File
import java.lang.reflect.InvocationTargetException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.full.declaredMemberProperties
import java.util.NoSuchElementException as NoSuchElementException1


const val SETTINGS_FILENAME: String = "settings.csv"

// singleton to handle the writing in stat/log files
object LogManager : LifecycleObserver {
    private lateinit var appContext: Context
    private lateinit var expDirStr: String

    private var connectInfo: Connectivity =
        Connectivity

    private lateinit var kgMgr: KeyguardManager
    private lateinit var powerManager: PowerManager
    private lateinit var bm: BatteryManager

    private var appState = "foreground"

    enum class RECORD_TYPE {
        LOG, SCAN, ADV
    }

    enum class FIELDS {
        deviceModel,
        deviceOsVersion,
        batterySaverMode,
        deviceDisplayActive,
        deviceLocked,
        appState,
        deviceOrientation,
        appVersion,
        bleVersion,
        bleDevicesConnected,
        filteringInfo,
        receivedMessageEbid,
        receivedMessageTx,
        receivedMessageRawRssi,
        receivedMessageCalibratedRssi,
        myEbid,
        myTx,
        myRx,
        wifiState,
        wifiUsed,
        testName,

        deviceManufacturer,
        runName,
        phoneTimestamp,
        phoneDateTime,
        applicationLifecycle,
        uuids,
        deviceBaseband,
        deviceOsVersionFull,
        isConnected,
        mobileState,
        recordType,
        batteryLevel,
        chargingState,
        batteryLevelFine,
        message,
        newEbid,
        seenEbid
    }

    fun isActivityVisible(): String {
        return appState
        // this returns created/started,
        // return ProcessLifecycleOwner.get().lifecycle.currentState.name
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        //App in background
        appState = "background"
        Log.v(TAG, "************* backgrounded")
        Log.v(TAG, "************* ${isActivityVisible()}")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        // App in foreground
        appState = "foreground"
        Log.v(TAG, "************* foregrounded")
        Log.v(TAG, "************* ${isActivityVisible()}")
    }

    // initialize the logger
    fun init(context: Context): MutableList<String> {
        appContext = context
        val expNames: MutableList<String> = ArrayList()

        kgMgr = appContext.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        powerManager = appContext.getSystemService(Context.POWER_SERVICE) as PowerManager
        bm = appContext.getSystemService(Context.BATTERY_SERVICE) as BatteryManager

        // check for the experiments root folder, if not available, create it
        val expDir = File(context.getExternalFilesDir(null), "Experiments")
        if (expDir.isDirectory) {
            expDir.walk()
                .sortedBy { it.isDirectory }
                .forEach { expNames.add(it.name.substringAfterLast("/", "")) }
        } else {
            // assumes file does not exist. create the directory
            expDir.mkdirs()
            // also create the default experiment
            val expDefaultDir = File(expDir, GlobalSettings.expName)
            expDefaultDir.mkdirs()
            expNames.add(GlobalSettings.expName)
        }


        // stores the absolute path to the experiments root folder
        expDirStr = expDir.absolutePath

        // returns a list of all the experiments
        return expNames
    }

    // the experiment name should only contain lower case alphanumeric characters. all other
    // characters will be removed before creating the folder and use only lower case
    fun createExperiment(expName: String): Boolean {
        var ok = false

        val currentExpDir = File(
            expDirStr,
            expName.filter { it.isLetterOrDigit() }.toLowerCase(Locale.getDefault())
        )

        when {
            currentExpDir.isDirectory -> {
                /* already exist */
            }
            currentExpDir.isFile -> {
                /* conflicting file */
            }
            else -> {
                /* create */
                ok = currentExpDir.mkdirs()
            }
        }

        val settingsFile = File(
            currentExpDir,
            SETTINGS_FILENAME
        )

        // if file does not exist yet, create it when wrtting the csv header line
        if (!settingsFile.exists()) {
            val settingsString: String =
                "timestamp" + ',' +
                        "expName" + ',' +
                        "expRun" + ',' +
                        "riskScore" + ',' +
                        "contactThreshold" + ',' +
                        "AdvPartMode" + ',' +
                        "AdvTxPower" + ',' +
                        "AdvInterval" + ',' +
                        "EbidRotationInterval" + ',' +
                        "PartRotationInterval" + ',' +
//                        "AdvNbServices" + ',' +
                        "AdvReportDelay" + ',' +
                        "ScanMode" + ',' +
                        "ScanReportDelay"

            settingsFile.writeText(settingsString + System.getProperty("line.separator"));
        }
        return ok
    }

    // the experiment name should only contain lower case alphanumeric characters. all other
    // characters will be removed before creating the folder and use only lower case
    fun initExperimentRun(
        expName: String,
        expRun: String,
        settingsMap: MutableMap<String, String>
    ): Boolean {
        var ok = false

        val currentExpDir = File(
            expDirStr,
            expName.filter { it.isLetterOrDigit() }.toLowerCase(Locale.getDefault())
        )

        when {
            currentExpDir.isDirectory -> {
                val currentExpRun = File(
                    currentExpDir,
                    expName.filter { it.isLetterOrDigit() }.toLowerCase(Locale.getDefault()) +
                            "_" +
                            expRun.filter { it.isLetterOrDigit() }
                                .toLowerCase(Locale.getDefault()) +
                            ".csv"
                )

                val settingsFile = File(
                    currentExpDir,
                    SETTINGS_FILENAME
                )

                val settingsString: String =
                    System.currentTimeMillis().toString() + ',' +
                            expName.filter { it.isLetterOrDigit() }
                                .toLowerCase(Locale.getDefault()) + ',' +
                            expRun.filter { it.isLetterOrDigit() }
                                .toLowerCase(Locale.getDefault()) + ',' +
                            settingsMap.get("riskScore") + ',' +
                            settingsMap.get("contactThreshold") + ',' +
                            settingsMap.get("AdvPartMode") + ',' +
                            settingsMap.get("AdvTxPower") + ',' +
                            settingsMap.get("AdvInterval") + ',' +
                            settingsMap.get("EbidRotationInterval") + ',' +
                            settingsMap.get("PartRotationInterval") + ',' +
//                            settingsMap.get("AdvNbServices") + ',' +
                            settingsMap.get("AdvReportDelay") + ',' +
                            settingsMap.get("ScanMode") + ',' +
                            settingsMap.get("ScanReportDelay")
                settingsFile.appendText(settingsString + System.getProperty("line.separator"));

                if (!currentExpRun.exists()) {
                    ok = initHeader(
                        currentExpRun
                    )
                }
            }
            else -> {
                // error with invalid experiment name
            }
        }

        return ok
    }

    private fun initHeader(currentExpRun: File): Boolean {
        val ok: Boolean = currentExpRun.createNewFile()
        if (ok) {
            val headerString: String =
                FIELDS.values().joinToString(separator = ",") { it -> "\'${it.name}\'" }
            currentExpRun.writeText(headerString + System.getProperty("line.separator"));
        }
        return ok
    }

    fun record(
        recordType: RECORD_TYPE,
        map: MutableMap<String, Any?>
    ): Boolean {

        try {

            // set record info
            map.set(FIELDS.phoneTimestamp.name, System.currentTimeMillis().toString())
            map.set(
                FIELDS.phoneDateTime.name,
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(Calendar.getInstance().time)
            )
            map.set(FIELDS.recordType.name, recordType.toString())

            // set device info
            map.set(FIELDS.deviceModel.name, Build.DEVICE)
            map.set(FIELDS.deviceOsVersion.name, Build.VERSION.SDK_INT.toString())
            map.set(FIELDS.deviceOsVersionFull.name, System.getProperty("os.version"))
            map.set(FIELDS.deviceBaseband.name, Build.getRadioVersion())
            map.set(FIELDS.deviceLocked.name, kgMgr.isKeyguardLocked.toString())
            map.set(
                FIELDS.deviceOrientation.name,
                getDeviceOrientation()
            )
            map.set(FIELDS.batterySaverMode.name, powerManager.isPowerSaveMode.toString())
            map.set(FIELDS.deviceDisplayActive.name, powerManager.isInteractive.toString())

            map.set(
                FIELDS.wifiState.name, Connectivity.isConnectedWifi(
                    appContext
                ).toString()
            )
            map.set(FIELDS.wifiUsed.name, "")
            map.set(
                FIELDS.isConnected.name, Connectivity.isConnected(
                    appContext
                ).toString()
            )
            map.set(
                FIELDS.mobileState.name, Connectivity.isConnectedMobile(
                    appContext
                ).toString()
            )

            map.set(FIELDS.deviceManufacturer.name, Build.MANUFACTURER)

            map.set(
                FIELDS.batteryLevel.name,
                bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY).toString()
            )
            map.set(FIELDS.chargingState.name, bm.isCharging.toString())
            map.set(
                FIELDS.batteryLevelFine.name,
                bm.getLongProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER).toString()
            )

            // set app info
            map.set(
                FIELDS.appState.name,
                isActivityVisible()
            )
            map.set(FIELDS.appVersion.name, BuildConfig.VERSION_CODE.toString())
            map.set(FIELDS.applicationLifecycle.name, Build.MANUFACTURER)

            // set experiments info
            map.set(FIELDS.testName.name, GlobalSettings.expName)
            map.set(FIELDS.runName.name, GlobalSettings.expRun)

            // info provided
            // map.set(FIELDS.myTx.name, "")
            // map.set(FIELDS.myRx.name, "")
            // map.set(FIELDS.bleDevicesConnected.name, "")
            // map.set(FIELDS.filteringInfo.name, "")
            // map.set(FIELDS.receivedMessageEbid.name, "")
            // map.set(FIELDS.receivedMessageTx.name, "")
            // map.set(FIELDS.receivedMessageRawRssi.name, "")
            // map.set(FIELDS.receivedMessageCalibratedRssi.name, "")
            // map.set(FIELDS.myEbid.name, GlobalSettings.advData!!.ebid.toString())
            // map.set(FIELDS.uuids.name, "")

            // create record entry from the map and save it to the csv file
            val newRecord =
                Measure(map);
            writeCsvLine(
                GlobalSettings.expName,
                GlobalSettings.expRun,
                newRecord.toCsvLine()
            )
        } catch (ex: java.lang.Exception) {
        }

        return true
    }

    private fun getDeviceOrientation(): String {
        // simple code that does not take may be innacurate in some cases. check for more details:
        // https://stackoverflow.com/questions/10380989/how-do-i-get-the-current-orientation-activityinfo-screen-orientation-of-an-a
        val orientation = appContext.getResources().getConfiguration().orientation
        return if (orientation == 1) "portrait" else "landscape"
    }


    private fun writeCsvLine(expName: String, expRun: String, csvLine: String) {
        val currentExpDir = File(
            expDirStr,
            expName.filter { it.isLetterOrDigit() }.toLowerCase(Locale.getDefault())
        )
        val currentExpRun = File(
            currentExpDir,
            expName.filter { it.isLetterOrDigit() }.toLowerCase(Locale.getDefault()) +
                    "_" +
                    expRun.filter { it.isLetterOrDigit() }.toLowerCase(Locale.getDefault()) +
                    ".csv"
        )
        currentExpRun.appendText(csvLine + System.getProperty("line.separator"));
    }


    data class Measure(val map: MutableMap<String, Any?>) {
        var deviceModel: String by map
        var deviceOsVersion: String by map
        var batterySaverMode: String by map
        var deviceDisplayActive: String by map
        var deviceLocked: String by map
        var appState: String by map
        var deviceOrientation: String by map
        var appVersion: String by map
        var bleVersion: String by map
        var bleDevicesConnected: String by map
        var filteringInfo: String by map
        var receivedMessageEbid: String by map
        var receivedMessageTx: String by map
        var receivedMessageRawRssi: String by map
        var receivedMessageCalibratedRssi: String by map
        var myEbid: String by map
        var myTx: String by map
        var myRx: String by map
        var wifiState: String by map
        var wifiUsed: String by map
        var testName: String by map
        var deviceManufacturer: String by map
        var runName: String by map
        var phoneTimestamp: String by map
        var phoneDateTime: String by map
        var applicationLifecycle: String by map
        var uuids: String by map
        var deviceBaseband: String by map
        var deviceOsVersionFull: String by map
        var isConnected: String by map
        var mobileState: String by map
        var recordType: String by map
        var batteryLevel: String by map
        var chargingState: String by map
        var batteryLevelFine: String by map
        var message: String by map
        var newEbid: String by map
        var seenEbid: String by map


        // generate a csv string from the measure's data
        fun toCsvLine(): String {

            // use reflexion API to get all the properties for the csv
            val current = this

            var nameToReturn = ""
            val str = FIELDS.values()
                .joinToString(separator = ",") { prop ->
                    try {
                        val property =
                            Measure::class.declaredMemberProperties.first { it.name == prop.name }
                        nameToReturn = ""
                        nameToReturn = "\"" + property.get(current).toString() + "\""
                    } catch (e: NoSuchElementException1) {
                    } catch (e: InvocationTargetException) {
                    }

                    nameToReturn
                }
            return str
        }
    }

    object Connectivity {
        // if no connectivity at all, NetworkInfo returned is null
        fun getNetworkInfo(context: Context): NetworkInfo? {
            val cm =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo
        }

        fun isConnected(context: Context): Boolean {
            val info =
                getNetworkInfo(
                    context
                )
            return info != null && info.isConnected
        }

        fun isConnectedWifi(context: Context): Boolean {
            val info =
                getNetworkInfo(
                    context
                )
            return info != null && info.isConnected && info.type == ConnectivityManager.TYPE_WIFI
        }

        fun isConnectedMobile(context: Context): Boolean {
            val info =
                getNetworkInfo(
                    context
                )
            return info != null && info.isConnected && info.type == ConnectivityManager.TYPE_MOBILE
        }

    }
}