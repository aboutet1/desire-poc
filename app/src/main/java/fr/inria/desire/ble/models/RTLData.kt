package fr.inria.desire.ble.models

import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.GlobalSettings

const val WINDOW_LENGTH: Int = 120
const val WINDOW_STEP: Int = 60
const val NB_WINDOWS: Int = 15

const val MIN_CONTACT_TIME: Long = 1000 * 60 * 10


@ExperimentalUnsignedTypes
class RTLData() {
    // start timestamp for this epoch data
    var startTimestamp: Long = 0

    // assumes all the items are in chronological order
    val data: MutableList<RTLItem> = ArrayList()
    val windowedData: RTLWindows = RTLWindows()
    var obf: Int = 0

    fun computeObf(localEbid: EBID, distEbid: EBID): Int {
        if (localEbid > distEbid) {
            obf = localEbid.ebid!![0].toInt()
            obf = (obf shl 8) or localEbid.ebid!![1].toInt()
        } else {
            obf = distEbid.ebid!![0].toInt()
            obf = (obf shl 8) or distEbid.ebid!![1].toInt()
        }
        obf %= GlobalSettings.OBFMax
        return obf
    }

    // returs true is window/exposuire time is too small
    fun isLongEnough(): Boolean {
        val deltaContact = data.get(data.size - 1).timestamp - data.get(0).timestamp
        return deltaContact >= MIN_CONTACT_TIME
    }


    // split RTL table of rssi into windows of average rssi value
    fun split(): RTLWindows {
        val currentSplitTimestamp = startTimestamp
        for (index: Int in 1..NB_WINDOWS) {
            // window start based on number of steps * step time
            // window end based on number of steps * step time +
            val curWindowStart: Long = startTimestamp + (index - 1) * (WINDOW_STEP * 1000)
            val curWindowEnd: Long =
                startTimestamp + (index - 1) * (WINDOW_STEP * 1000) + (WINDOW_LENGTH * 1000) - 1

            val win: RTLWindow = RTLWindow()
            win.ts = curWindowStart
            for (item: RTLItem in data) {
                if (item.after(curWindowStart) && item.before(curWindowEnd)) {
                    win.values.add(item.rssi - AdvertiserSettings.rx_bias)
                }
            }
            windowedData.values.add(win)
        }

        return windowedData
    }

    fun add(item: RTLItem) {
        data.add(item)
    }

    fun add(rssi: Int, ts: Long) {
        data.add(RTLItem(rssi, ts))
    }

}


class RTLItem(var rssi: Int, var timestamp: Long) {

    constructor() : this(0, System.currentTimeMillis())

    fun before(ts: Long): Boolean {
        return this.timestamp <= ts
    }

    fun after(ts: Long): Boolean {
        return this.timestamp >= ts
    }

    fun on(ts: Long): Boolean {
        return this.timestamp == ts
    }

}