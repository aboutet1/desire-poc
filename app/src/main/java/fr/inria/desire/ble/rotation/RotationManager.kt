package fr.inria.desire.ble.rotation

import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.interfaces.Startable
import fr.inria.desire.ble.services.AdvertiserServices
import fr.inria.desire.ble.services.AdvertiserConnection
import fr.inria.desire.ble.settings.AdvertiserSettings
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@ExperimentalUnsignedTypes
class RotationManager : Startable {
    var rotationEbidInterval: Int = 15
    var advertizers: MutableList<AdvertiserServices> = ArrayList()
    lateinit var connAdvertizer: AdvertiserConnection
    var isEbidRotation: Boolean = false
    var isPartRotation: Boolean = false
    var ebidChanger: EbidChanger? = null
    var partChanger: PartRotationChanger? = null
    override var started: Boolean = false

    fun init() {
        PartRotationChanger.caller = this
        EbidChanger.caller = this
    }

    override fun start() {
        started = true

        if (isEbidRotation) {
            ebidChanger = EbidChanger()
            GlobalScope.launch {
                loopEbid()
            }
        }
        if (isPartRotation) {
            partChanger = PartRotationChanger()
            GlobalScope.launch {
                loopPart()
            }
        }
    }

    private suspend fun loopPart() {
        while (started) {
            delay((AdvertiserSettings.partsInterval * 1000).toLong())
            partChanger?.onPartAlarm()
        }

    }

    private suspend fun loopEbid() {
        while (started) {
            delay((rotationEbidInterval * 60 * 1000).toLong())
            ebidChanger?.onEbidAlarm()
        }
    }

}