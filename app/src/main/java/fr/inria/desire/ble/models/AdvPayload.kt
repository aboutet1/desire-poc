package fr.inria.desire.ble.models

import fr.inria.desire.ble.settings.AdvertiserSettings


@ExperimentalUnsignedTypes
data class AdvPayload @ExperimentalUnsignedTypes constructor(
    val sid: UInt,
    val cid: UInt,
    val ebid_slice: UByteArray,
    val md_version: UInt
) {

    init {
        // TODO add checks
        // check sid is 0, 1 or 2
        // check that sid is only 30 bits
        // check that ebid_slice is 8 or 12 bytes
        // check that md_reserved is 2 bytes and equal to 0
    }

    companion object {

        private const val LONG_EBID_PART_SIZE = 12
        private const val SHORT_EBID_PART_SIZE = 8
        private const val DATA_BYTE_SIZE = 16
        private const val METADATA_BYTE_SIZE = 4

        private const val EBID_OFFSET = 4
        private const val RESERVED_OFFSET = 1
        private const val PADDING_SHORT_SLICE = 4


        // 3 bytes of reserved empty data
        private val md_reserved: UByteArray = ubyteArrayOf(0x00u, 0x00u, 0x00u)

        fun from(data: UByteArray, metadata: UByteArray): AdvPayload {

            require(data.size >= DATA_BYTE_SIZE) { "Expecting data byte array of $DATA_BYTE_SIZE  bytes. Got ${data.size}." }
            require(metadata.size >= METADATA_BYTE_SIZE) { "Expecting metadata byte array of $METADATA_BYTE_SIZE bytes. Got ${metadata.size}." }

            // first extract sid/sid from incoming data
            val sid: UInt = ((data[0]).toUInt() shr 6)
            val cid =
                (((data[0]).toUInt() and 0x3fu) shl 24) + ((data[1]).toUInt() shl 16) + ((data[2]).toUInt() shl 8) + ((data[3]).toUInt())
            val ebidSlice: UByteArray

            // then extract slice data
            if (sid.equals(0u) or sid.equals(1u)) {
                // 1st and nd slices are full slices
                ebidSlice = UByteArray(LONG_EBID_PART_SIZE)
                data.copyInto(ebidSlice, 0, 4, 4 + LONG_EBID_PART_SIZE)
            } else if (sid.equals(2u)) {
                // third slice is short one, but padding in front
                ebidSlice = UByteArray(SHORT_EBID_PART_SIZE)
                data.copyInto(
                    ebidSlice,
                    0,
                    4 + PADDING_SHORT_SLICE,
                    4 + PADDING_SHORT_SLICE + SHORT_EBID_PART_SIZE
                )
            } else if (sid.equals(3u)) {
                // fourth slice is long xor
                ebidSlice = UByteArray(LONG_EBID_PART_SIZE)
                data.copyInto(ebidSlice, 0, 4, 4 + LONG_EBID_PART_SIZE)
            } else {
                ebidSlice = UByteArray(LONG_EBID_PART_SIZE)
            }

            // process metadata
            val mdVersion: UInt = (metadata[0]).toUInt()

            // return payload object
            return AdvPayload(
                sid = sid,
                cid = cid,
                ebid_slice = ebidSlice,
                md_version = mdVersion
            )
        }

        fun fromOrNull(data: UByteArray, metadata: UByteArray): AdvPayload? {
            return try {
                from(data, metadata)
            } catch (e: IllegalArgumentException) {
                null
            }
        }
    }

    fun toUByteArrays(): Pair<UByteArray, UByteArray> {

        val data = UByteArray(DATA_BYTE_SIZE)
        val metadata = UByteArray(METADATA_BYTE_SIZE)

        // copy sid in first 2 bits and cid in next 30 bits
        data[0] = (sid shl 6).toUByte() or ((cid shr 24).toUByte() and 0x3Fu)
        data[1] = ((cid shr 16).toUByte() and 0xffu)
        data[2] = ((cid shr 8).toUByte() and 0xffu)
        data[3] = ((cid).toUByte() and 0xffu)
        // copy ebid in rest of data frame (maybe only 8 bytes are used)

        // if third slice (short) add padding in front
        if (sid.equals(2u)) {
            // third slice is short one, but padding in front
            val padding: UByteArray = ubyteArrayOf(0x00u, 0x00u, 0x00u, 0x00u)
            padding.copyInto(data, EBID_OFFSET)
            ebid_slice.copyInto(data, EBID_OFFSET + PADDING_SHORT_SLICE)
        } else {
            // other slices or xor, copu full length
            ebid_slice.copyInto(data, EBID_OFFSET)
        }

        metadata[0] = ((md_version).toUByte() and 0xffu)
        md_reserved.copyInto(metadata, RESERVED_OFFSET)

        return Pair(data, metadata)
    }
}