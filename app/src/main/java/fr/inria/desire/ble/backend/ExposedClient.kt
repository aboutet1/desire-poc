package fr.inria.desire.ble.backend

import android.util.Log
import com.google.gson.annotations.SerializedName
import fr.inria.desire.ble.activities.TAG
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ExposedClientAPI {
    @GET(Backend.apiExposed)
    fun getExposed(
        @Query("r") r: String,
        @Query("sigma") sigma: String
    ): Call<ExposedResponseGet>
}

interface ExposedListener {
    fun getResponse(response: ExposedResponseGet)
    fun error(code: Int)
}

class ExposedResponseGet {
    @SerializedName("exposed")
    var exposedList: List<String>? = null
}


class ExposedClient(val caller: ExposedListener) {
    var service: ExposedClientAPI

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(Backend.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(ExposedClientAPI::class.java)
    }


    fun getExposed(r: String, sigma: String) {

        val call = service.getExposed(r, sigma)
        call.enqueue(object : Callback<ExposedResponseGet> {
            override fun onResponse(
                call: Call<ExposedResponseGet>,
                response: Response<ExposedResponseGet>
            ) {
                if (response.code() == 200) {
                    val exposedResponse = response.body()!!
                    Log.i(TAG, "Exposed response : " + exposedResponse.toString())
                    caller.getResponse(exposedResponse)
                } else {
                    Log.i(
                        TAG,
                        "getExposed unable to proceed - error code : " + response.code().toString()
                    )
                    caller.error(response.code())
                }
            }

            override fun onFailure(call: Call<ExposedResponseGet>?, t: Throwable?) {
                Log.w(TAG, "getExposed call failure")
                caller.error(-1)
            }
        })
    }

}