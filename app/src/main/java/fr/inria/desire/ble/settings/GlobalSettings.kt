package fr.inria.desire.ble.settings

import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.interfaces.IObservable
import fr.inria.desire.ble.interfaces.IObserver
import fr.inria.desire.ble.interfaces.Startable
import fr.inria.desire.ble.managers.EpochDataManager
import fr.inria.desire.ble.managers.LogManager
import fr.inria.desire.ble.models.AdvData
import fr.inria.desire.ble.models.CID
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.PET
import fr.inria.desire.ble.rotation.RotationManager
import java.util.*
import kotlin.collections.ArrayList

object GlobalSettings : IObservable {

    const val OBFMax: Int = 100
    const val CENTRALIZED = 0
    const val DECENTRALIZED = 1

    var initialized = false
    var riskScore = CENTRALIZED

    var exposureStatus: String = "Unknown"
        set(value) {
            field = value
            this.dataChanged()
        }
    var generatePET: Boolean = true
    var longContactDuration: Int = 15 //default: 15 minutes

    var expName: String = "default"
        get() = field
        set(value) {
            field = value
        }
    var expRun: String = "run"
        get() = field
        set(value) {
            field = value
        }

    var connectedMode: Boolean = false


    lateinit var rotationManager: RotationManager
    public fun isROTATIONMGRInitialised(): Boolean {
        return ::rotationManager.isInitialized
    }

    val advDataLock: MutableMap<Int, String> = mutableMapOf()
    var ebidcidChanges: Int = 0
    lateinit var advData: AdvData
    public fun isAdvDataInitialised(): Boolean {
        return ::advData.isInitialized
    }

    public fun updateCidEbid() {
        if (initialized) {
            //saving Epoch data
            EpochDataManager.addEpochData(localEbid, ScannerSettings.EBIDdata, epochStart)
            ScannerSettings.EBIDdata.clear()
        }
        epochStart = System.currentTimeMillis()
        //clearing current Epoch data


        val cid: CID = CID()
        cid.generate()
        val ebid: EBID = EBID()
        ebid.generate()
        localEbid = ebid

        ebidcidChanges++

        Log.i(TAG, "Updating EBID and CID")
        Log.i(TAG, "  - new EBID " + ebid.ebid)
        Log.i(TAG, "  - new EBID " + ebid)
        Log.i(TAG, "  - new CID " + cid.value.toString())

        advData = AdvData(ebid, cid)
        initialized = true
        LogManager.record(
            recordType = LogManager.RECORD_TYPE.ADV,
            map = mutableMapOf(
                "message" to "Updated EBID and CID $ebid.ebid $cid.value"
            )
        )
    }

    lateinit var localEbid: EBID

    // which slice to use - value from 0 to ebidslices (excluded)
    var currentRotationPart: Int = 0
    var epochStart = System.currentTimeMillis()
    var createdPET: ArrayList<PET> = ArrayList()
    val startableArray: ArrayList<Startable> = ArrayList()
    val serviceIdTable: ArrayList<Int> = ArrayList()
    var packetLoss: Int = 0
        get() = field
        set(value) {
            if (value < 0)
                field = 0
            else if (value > 100)
                field = 100
            else
                field = value
        }
    override val observers: ArrayList<IObserver> = ArrayList()
}