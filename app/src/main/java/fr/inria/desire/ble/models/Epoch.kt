package fr.inria.desire.ble.models

import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.settings.AdvertiserSettings

@ExperimentalUnsignedTypes
class Epoch(val startTimestamp: Long, val endTimestamp: Long) {

    var data: MutableList<Pair<RTLData, PET>> = ArrayList()
    var processed: Boolean = false

    fun before(ts: Long): Boolean {
        return ts <= startTimestamp
    }

    fun after(ts: Long): Boolean {
        return ts >= endTimestamp
    }

    fun contains(ts: Long): Boolean {
        return (ts in startTimestamp..endTimestamp)
    }

    fun add(p: Pair<RTLData, PET>): Boolean {
        for (item: RTLItem in p.first.data) {
            if (item.before(startTimestamp) || item.after(endTimestamp)) {
                return false
            }
        }
        data.add(p)
        return true
    }

    fun process() {
        // #TODO : Process RTLData (clipping, fading)
        for (p in data) {
            p.first.split().process()
        }
        processed = true
    }

    fun getRequestData(): List<Pair<PET, FadedWindows>> {
        val res = ArrayList<Pair<PET, FadedWindows>>()
        for (d in data) {
            res.add(Pair(d.second, d.first.windowedData.fadedValues))
        }
        return res
    }

    fun getExposedData(): List<Pair<PET, Int>> {
        val res = ArrayList<Pair<PET, Int>>()
        for (d in data) {
            res.add(
                Pair(
                    d.second,
                    AdvertiserSettings.tx_power - AdvertiserSettings.tx_bias - d.first.obf
                )
            )
        }
        return res
    }


}