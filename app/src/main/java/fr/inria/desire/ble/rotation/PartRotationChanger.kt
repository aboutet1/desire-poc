package fr.inria.desire.ble.rotation

import android.util.Log
import fr.inria.desire.ble.activities.TAG
import fr.inria.desire.ble.managers.LogManager
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings

@ExperimentalUnsignedTypes
class PartRotationChanger {
    companion object {
        lateinit var caller: RotationManager
    }

    fun onPartAlarm() {

        // record info in csv
        LogManager.record(
            recordType = LogManager.RECORD_TYPE.LOG,
            map = mutableMapOf(
                "message" to "Received EBID rotating part alarm with current part " + (1 + GlobalSettings.currentRotationPart).toString()
            )
        )

        // increment part to send - use the number of services
        GlobalSettings.currentRotationPart =
            (GlobalSettings.currentRotationPart + 1) % Constants.EBID_SLICES_XOR_NB

        Log.i(
            TAG,
            " - new rotation part number " + (GlobalSettings.currentRotationPart).toString()
        )

        // should be only one advertizer
        for (advertiser in caller.advertizers) {
            Log.i(TAG, " - call advertizer")
            advertiser.rotatePart()
        }
    }

}