package fr.inria.desire.ble.activities

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ProcessLifecycleOwner
import fr.inria.desire.ble.*
import fr.inria.desire.ble.managers.LogManager
import fr.inria.desire.ble.rotation.RotationManager
import fr.inria.desire.ble.services.*
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.Constants
import fr.inria.desire.ble.settings.GlobalSettings
import fr.inria.desire.ble.settings.ScannerSettings
import kotlin.collections.ArrayList

private const val REQUEST_ENABLE_BT = 1

@ExperimentalUnsignedTypes
class SettingsActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {
    private lateinit var mBluetoothLeScanner: BluetoothLeScanner
    private lateinit var btAdapter: BluetoothAdapter
    private lateinit var btManager: BluetoothManager


    private lateinit var spinnerScannerMode: Spinner
    private lateinit var spinnerInterval: Spinner
    private lateinit var spinnerTxPower: Spinner
    private lateinit var spinnerAdvertiserMode: Spinner
    private lateinit var spinnerRiskScore: Spinner

    private lateinit var mAdvertiseButton: Button
    private lateinit var mScanButton: Button
    private lateinit var setExpnaeButton: Button
    private lateinit var mEditStatus: EditText

    //private lateinit var mEditAdvertiseNbService: EditText
    private lateinit var mEditScanReportDelay: EditText
    private lateinit var editRotationEbidInterval: EditText
    private lateinit var editRotationPartInterval: EditText
    private lateinit var editLongContact: EditText
    private lateinit var editNbFakePet: EditText
    private lateinit var editFakeLoss: EditText

    private lateinit var scannerServices: ScannerServices
    private lateinit var scannerConnection: ScannerConnection
    private lateinit var expLogger: LogManager

    // Checks if a volume containing external storage is available
    // for read and write.
    private fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    // Checks if a volume containing external storage is available to at least read.
    private fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in
                setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
    }


    private fun checkPermissionForExternalStorage(): Boolean {
        val result = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return (result == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermissionForExternalStorage(requestCode: Int) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            Toast.makeText(
                this.getApplicationContext(),
                "External Storage permission needed. Please allow in App Settings for additional functionality.",
                Toast.LENGTH_LONG
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                requestCode
            )
        }
    }

    private fun requestPermissionForLocation(requestCode: Int) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                this.applicationContext,
                "Coarse location permission needed. Please allow in App Settings for additional functionality.",
                Toast.LENGTH_LONG
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                requestCode
            )
        }
    }

    private fun checkPermissionForNetworkState(): Boolean {
        val result = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_NETWORK_STATE
        )
        return (result == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermissionForNetworkState(requestCode: Int) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_NETWORK_STATE
            )
        ) {
            Toast.makeText(
                this.getApplicationContext(),
                "Network state access is required. Please allow in App Settings for additional functionality.",
                Toast.LENGTH_LONG
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_NETWORK_STATE),
                requestCode
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        val setting_btn = findViewById(R.id.btn_back) as Button
        setting_btn.setOnClickListener {
            this.finish()
        }

        // check if Bluetooth adapter is on. If not, ask user permission to activate it
        // otherwise, finalize the activity setup
        btManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        btAdapter = btManager.adapter
        if (!btAdapter.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(
                enableIntent,
                REQUEST_ENABLE_BT
            )
        } else {
            completeActivityUpdate()
        }

        if (!checkPermissionForExternalStorage()) {
            requestPermissionForExternalStorage(1)
        }

        requestPermissionForLocation(2)

        if (!checkPermissionForNetworkState()) {
            requestPermissionForNetworkState(3)
        }

        if (isExternalStorageWritable()) {
            Log.e(TAG, "isExternalStorageWritable TRUE")
        } else {
            Log.e(TAG, "isExternalStorageWritable FALSE")
        }

        if (isExternalStorageReadable()) {
            Log.e(TAG, "isExternalStorageReadable TRUE")

        } else {
            Log.e(TAG, "isExternalStorageReadable FALSE")
        }

        // create default experiment and run
        expLogger = LogManager
        LogManager.init(this.applicationContext)
        // and register it for foreground/background notifications
        ProcessLifecycleOwner.get().lifecycle.addObserver(expLogger)

    }

    override fun onClick(v: View) {
        //
        if (v.id == R.id.advertise_btn) {
            advertise()
        }
        //
        if (v.id == R.id.scan_btn) {
            scan()
        }
        if (v.id == R.id.set_expname_btn) {
            createAndSetExperimentName()
        }
    }

    private fun createAndSetExperimentName() {

        val settingsMap: MutableMap<String, String> = mutableMapOf()
        settingsMap.set("AdvPartMode", spinnerAdvertiserMode.selectedItem.toString())
        settingsMap.set("AdvTxPower", spinnerTxPower.selectedItem.toString())
        settingsMap.set("AdvInterval", spinnerInterval.selectedItem.toString())
//        settingsMap.set("AdvNbServices", mEditAdvertiseNbService.text.toString())
        settingsMap.set("AdvReportDelay", mEditScanReportDelay.text.toString())
        settingsMap.set("ScanMode", spinnerScannerMode.selectedItem.toString())
        settingsMap.set("ScanReportDelay", mEditScanReportDelay.text.toString())

        settingsMap.set("EbidRotationInterval", editRotationEbidInterval.text.toString())
        settingsMap.set("PartRotationInterval", editRotationPartInterval.text.toString())

        settingsMap.set("riskScore", spinnerRiskScore.selectedItem.toString())
        settingsMap.set("contactThreshold", editLongContact.text.toString())

        GlobalSettings.expRun = "run" + (System.currentTimeMillis() / 1000).toString()
        LogManager.createExperiment(GlobalSettings.expName)
        LogManager.initExperimentRun(
            GlobalSettings.expName,
            GlobalSettings.expRun,
            settingsMap
        )

        if (!AdvertiserSettings.started)
            mAdvertiseButton.isEnabled = true
        if (!ScannerSettings.started)
            mScanButton.isEnabled = true
    }

    private fun scan() {
        val intentScan = Intent(this, ForegroundService::class.java)
        val startableList = ArrayList<Int>()

        val listener: ((Int, String) -> Unit) = { value: Int, msg: String ->
            Log.i(TAG, msg)
        }

        if (GlobalSettings.connectedMode) {
            scannerConnection =
                ScannerConnection(
                    btAdapter,
                    this,
                    Constants.UUID_EBID_SERVICE.toString(), // serviceMask,
                    listener
                )
            GlobalSettings.startableArray.add(scannerConnection)
            startableList.add(GlobalSettings.startableArray.size - 1)
        } else {
            scannerServices =
                ScannerServices(
                    mBluetoothLeScanner,
                    Constants.UUID_EBID_SERVICE.toString(), // serviceShortMask,
                    listener
                )
            GlobalSettings.startableArray.add(scannerServices)
            startableList.add(GlobalSettings.startableArray.size - 1)
        }

        // make sure the EBID/CID rotation manager is launched once
        synchronized(GlobalSettings.rotationManager) {
            if (!GlobalSettings.rotationManager.isEbidRotation) {
                GlobalSettings.rotationManager.isEbidRotation = true
                GlobalSettings.startableArray.add(GlobalSettings.rotationManager)
            }
        }

        intentScan.putIntegerArrayListExtra("listToStart", startableList)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intentScan)
        } else {
            startService(intentScan)
        }
        ScannerSettings.started = true
        mScanButton.isEnabled = false
        setExpnaeButton.isEnabled = false
    }


    private fun advertise() {
        Log.i(TAG, "Launch advertiser(s)")

        val intentAdv = Intent(this, ForegroundService::class.java)
        val startableList = ArrayList<Int>()

        Log.i(TAG, "advertiser mode is " + this.spinnerAdvertiserMode.selectedItem.toString())
        when {

            // connected/gatt mode
            this.spinnerAdvertiserMode.selectedItem.toString().equals("Connected") -> {
                //Connection Advertisement
                GlobalSettings.rotationManager.connAdvertizer =
                    AdvertiserConnection(
                        btManager,
                        btAdapter
                    )
                GlobalSettings.rotationManager.connAdvertizer.init(this)
                GlobalSettings.startableArray.add(GlobalSettings.rotationManager.connAdvertizer)
                startableList.add(GlobalSettings.startableArray.size - 1)
            }

            // multi services
            this.spinnerAdvertiserMode.selectedItem.toString().equals("Multi-service") -> {
                Log.i(TAG, "start multi service")

                for (indexEbidPart in 1..(Constants.EBID_SLICES_XOR_NB)) {
                    // TODO here assume first one will init the adv data
                    // should be done in order
                    val doInit = (indexEbidPart == 1)
                    val advertizer =
                        AdvertiserServices(
                            btAdapter,
                            Constants.UUID_EBID_SERVICE.toString(), // String.format(pUuidTemplate, indexEbidPart),
                            doInit,
                            indexEbidPart - 1
                        )

                    GlobalSettings.startableArray.add(advertizer)
                    startableList.add(GlobalSettings.startableArray.size - 1)
                    GlobalSettings.rotationManager.advertizers.add(advertizer)
                }
            }

            // part rotation
            this.spinnerAdvertiserMode.selectedItem.toString().equals("Rotation") -> {
                Log.i(TAG, "start rotation")

                // start advertizing part
                val advertizer = AdvertiserServices(
                    btAdapter,
                    Constants.UUID_EBID_SERVICE.toString() // String.format( pUuidTemplate, 1 + AdvertiserSettings.currentRotationPart)
                )
                advertizer.start()
                GlobalSettings.rotationManager.isPartRotation = true
                GlobalSettings.rotationManager.advertizers.add(advertizer)
            }

        }

        // make sure the EBID/CID rotation manager is launched once
        synchronized(GlobalSettings.rotationManager) {
            if (!GlobalSettings.rotationManager.isEbidRotation) {
                GlobalSettings.rotationManager.isEbidRotation = true
                GlobalSettings.startableArray.add(GlobalSettings.rotationManager)
            }
        }

        startableList.add(GlobalSettings.startableArray.size - 1)
        intentAdv.putIntegerArrayListExtra("listToStart", startableList)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intentAdv)
        } else {
            startService(intentAdv)
        }
        AdvertiserSettings.started = true
        // update UI
        mAdvertiseButton.isEnabled = false
        setExpnaeButton.isEnabled = false
    }

    private fun completeActivityUpdate() {
        // init Receiver manager ONLY IF NOT INITILIZED ALREADY
        if (!GlobalSettings.isROTATIONMGRInitialised()) {
            GlobalSettings.rotationManager = RotationManager()
            GlobalSettings.rotationManager.init()
        }

        // get scanner once the adatper is on
        mBluetoothLeScanner = btAdapter.bluetoothLeScanner

        // init UI elements
        spinnerAdvertiserMode = findViewById(R.id.spinner_advertiser_modes)
        ArrayAdapter.createFromResource(
            this,
            R.array.adv_mode_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerAdvertiserMode.adapter = adapter
        }
        spinnerAdvertiserMode.onItemSelectedListener = this
        spinnerAdvertiserMode.setSelection(AdvertiserSettings.ADV_MODE_MULTISERVICE) // Multi-services

        spinnerScannerMode = findViewById(R.id.spinner_scanner_mode)
        ArrayAdapter.createFromResource(
            this,
            R.array.scan_mode_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerScannerMode.adapter = adapter
        }
        spinnerScannerMode.onItemSelectedListener = this
        spinnerScannerMode.setSelection(ScannerSettings.scanModePos) // ScanSettings.SCAN_MODE_BALANCED

        spinnerInterval = findViewById(R.id.spinner_advertiser_interval_bt)
        ArrayAdapter.createFromResource(
            this,
            R.array.interval_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerInterval.adapter = adapter
        }
        spinnerInterval.onItemSelectedListener = this
        spinnerInterval.setSelection(AdvertiserSettings.intervalPos) // AdvertisingSetParameters.INTERVAL_LOW

        spinnerTxPower = findViewById(R.id.spinner_advertiser_tx_power)
        ArrayAdapter.createFromResource(
            this,
            R.array.tx_power_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerTxPower.adapter = adapter
        }
        spinnerTxPower.onItemSelectedListener = this
        spinnerTxPower.setSelection(AdvertiserSettings.txPowerLevelPos) // AdvertisingSetParameters.TX_POWER_MEDIUM

        spinnerRiskScore = findViewById(R.id.spinner_risk_score)
        ArrayAdapter.createFromResource(
            this,
            R.array.risk_score_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerRiskScore.adapter = adapter
        }
        spinnerRiskScore.onItemSelectedListener = this
        spinnerRiskScore.setSelection(GlobalSettings.riskScore)

        mAdvertiseButton = findViewById(R.id.advertise_btn)
        mScanButton = findViewById(R.id.scan_btn)
        setExpnaeButton = findViewById(R.id.set_expname_btn)

        mAdvertiseButton.setOnClickListener(this)
        mScanButton.setOnClickListener(this)
        setExpnaeButton.setOnClickListener(this)

        mEditStatus = findViewById(R.id.exp_name_etext)
        mEditStatus.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    GlobalSettings.expName = p0.toString()
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }
            }
        )
        mEditStatus.setText(GlobalSettings.expName)

        /*
        mEditAdvertiseNbService = findViewById(R.id.edit_advertiser_number)
        mEditAdvertiseNbService.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0 != null && p0.isNotEmpty()) {
                        AdvertiserSettings.nbServices = p0.toString().toInt()
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }
            }
        )
        mEditAdvertiseNbService.setText(AdvertiserSettings.nbServices.toString())
        */

        mEditScanReportDelay = findViewById(R.id.edit_report_delay)
        mEditScanReportDelay.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0 != null && p0.isNotEmpty()) {
                        ScannerSettings.reportDelay = p0.toString().toLong()
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }
            }
        )
        mEditScanReportDelay.setText(ScannerSettings.reportDelay.toString())

        editRotationEbidInterval = findViewById(R.id.edit_advertiser_rotation_interval)
        editRotationEbidInterval.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0 != null && p0.isNotEmpty()) {
                        GlobalSettings.rotationManager.rotationEbidInterval = p0.toString().toInt()
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }
            }
        )
        editRotationEbidInterval.setText(GlobalSettings.rotationManager.rotationEbidInterval.toString())

        editRotationPartInterval = findViewById(R.id.edit_advertiser_rotation)
        editRotationPartInterval.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0 != null && p0.isNotEmpty()) {
                        AdvertiserSettings.partsInterval = p0.toString().toInt()
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }
            }
        )
        editRotationPartInterval.setText(AdvertiserSettings.partsInterval.toString())

        editLongContact = findViewById(R.id.edit_long_contact)
        editLongContact.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0 != null && p0.isNotEmpty()) {
                        GlobalSettings.longContactDuration = p0.toString().toInt()
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }
            }
        )
        editLongContact.setText(GlobalSettings.longContactDuration.toString())

        editFakeLoss = findViewById(R.id.edit_packet_loss)
        editFakeLoss.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0 != null && p0.isNotEmpty()) {
                        GlobalSettings.packetLoss = p0.toString().toInt()
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //Nothing TODO
                }
            }
        )
        editFakeLoss.onFocusChangeListener =
            View.OnFocusChangeListener { v, hasFocus -> editFakeLoss.setText(GlobalSettings.packetLoss.toString()) }
        editFakeLoss.setText(GlobalSettings.packetLoss.toString())

        // initial button status
        mAdvertiseButton.isEnabled = false
        mScanButton.isEnabled = false
        setExpnaeButton.isEnabled = true
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        // TODO
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent?.id == R.id.spinner_advertiser_tx_power) {
            AdvertiserSettings.txPowerLevelPos = position
        } else if (parent?.id == R.id.spinner_advertiser_interval_bt) {
            AdvertiserSettings.intervalPos = position
        } else if (parent?.id == R.id.spinner_scanner_mode) {
            ScannerSettings.scanMode = position
        } else if (parent?.id == R.id.spinner_advertiser_modes) {
            val tr_part: TableRow = findViewById(R.id.tr_adv_part)
            val tr_rotation: TableRow = findViewById(R.id.tr_adv_rotation)
            val et_reportdelay: EditText = findViewById(R.id.edit_report_delay)
            when (position) {
                2 -> {
                    GlobalSettings.connectedMode = true
                    tr_part.visibility = View.INVISIBLE
                    tr_rotation.visibility = View.INVISIBLE
                    et_reportdelay.setText("60000")
                }
                1 -> {
                    GlobalSettings.connectedMode = false
                    tr_part.visibility = View.VISIBLE
                    tr_rotation.visibility = View.VISIBLE
                    et_reportdelay.setText("1500")
                }
                0 -> {
                    GlobalSettings.connectedMode = false
                    tr_part.visibility = View.VISIBLE
                    tr_rotation.visibility = View.INVISIBLE
                    et_reportdelay.setText("1500")
                }

            }
        } else if (parent?.id == R.id.spinner_risk_score) {
            GlobalSettings.riskScore = position
        }

    }
}