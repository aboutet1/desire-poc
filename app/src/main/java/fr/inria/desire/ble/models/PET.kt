package fr.inria.desire.ble.models

import android.util.Log
import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.settings.AdvertiserSettings
import fr.inria.desire.ble.settings.GlobalSettings
import java.util.Date

@ExperimentalUnsignedTypes
class PET(val etl: ByteArray, val rtl: ByteArray) {
    companion object {

        fun fromEBID(distEbid: EBID): PET? {
            val localEbid = EBID.createFromUByteArray(CryptoManager.publicKey!!.toUByteArray())
            if (distEbid.ebid != null && CryptoManager.publicKey != null) {
                val pet1 = CryptoManager.generatePet(
                    distEbid.ebid!!.asByteArray(),
                    byteArrayOf("1".toByte())
                )
                val pet2 = CryptoManager.generatePet(
                    distEbid.ebid!!.asByteArray(),
                    byteArrayOf("2".toByte())
                )
                if (pet1 == null || pet2 == null) {
                    Log.d("PET", "PET creation failed")
                    return null
                }

                return if (localEbid > distEbid) {
                    PET(pet1, pet2)
                } else {
                    PET(pet2, pet1)
                }
            } else {
                Log.d("PET", "one of the EBID is null")
                return null
            }
        }
    }

    val timestamp: Long = (Date().time / 1000)
}