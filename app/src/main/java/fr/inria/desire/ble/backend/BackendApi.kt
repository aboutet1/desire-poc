package fr.inria.desire.ble.backend

import android.util.Log
import fr.inria.desire.ble.models.PET
import fr.inria.desire.ble.activities.StatusActivity
import fr.inria.desire.ble.activities.TAG
import java.math.BigInteger
import java.util.*

// based on the desire.pem setup on the server
val public_key_n: BigInteger =
    BigInteger("22859415401688594574226161338526647114997810868885918956038237851734163364791116757008181052201531696390098964940881131807207711136141939148541572278753613102980026798692338044735621006712536638996968025766889852940297203852339839714947630247297201550319653693665117372371404811861204807537657750785490907573658669534302871802222509071280699782536078870438075473687153662844033115950875396535070229540496976100292338953009560461548411552226401662004648876667079924003698795652284531153395388627055581166287782128440804505257510942949012342559137117078187423100732508229350878272820220287488962342216273366751827360683")
val public_key_e: Int = BigInteger(1, Base64.getDecoder().decode("AAEAAQ==")).toInt()
//val public_key_n: BigInteger = BigInteger(1, Base64.getDecoder().decode("3U6hsrSv26DaAr/t9Dn5oEOgTfOH/m85QLW8lBWujTmPorDI2ikU1KX2JcXiTshWyrtz68L7tUMhR76WlWYN8qPn9ZB2m8i3IDHPTmQkIsyX5T/LOP+9ZV3ZoCXe4rbNOT1QODqBuf76/Z3ZdzwLVJqDMOH6uBve67Ac3Zs1H4oOTCWAIRd8MbaiND2El0vM509ZSNjg9skYAQXJQx3LBcfbambt2CzD7RdfxwS/ektWqUhiP0xQMdn9pe0uth2nBobpxUQsnkAqFqhbl1sRWeOdR/7gvDbZ6BfsdYUs1e5I0cWs2m4AkwTPC/ezLrJx/opWS6BIZX2SWgQuB0Z86w=="))

var secret_r: BigInteger = BigInteger("0")
var secret_c: BigInteger = BigInteger("0")
var inv_secret_c: BigInteger = BigInteger("0")
var secret: BigInteger = BigInteger("0")


fun registerGetApi(phone: String) {
    // setup backend clients
    val registerBackend: RegisterClient = RegisterClient(StatusActivity)

    Log.d(TAG, "registerGetApi with phone " + phone)
    registerBackend.getRegister(phone)
}

fun registerPostApi(pin: String): Array<Any> {
    // setup backend clients
    val registerBackend: RegisterClient = RegisterClient(StatusActivity)

    // TODO the values from the key are fixed for a phone/backend deployment (desire.pem)
    // TODO pin code is however different for each registration

    Log.i(TAG, "registerPostTest using public_key_e " + public_key_e.toString())
    Log.i(TAG, "registerPostTest using public_key_n " + public_key_n.toString())

    val secretValues = gen_secret(public_key_e, public_key_n)

    secret_r = secretValues[0] as BigInteger
    secret_c = secretValues[1] as BigInteger
    inv_secret_c = secretValues[2] as BigInteger
    secret = secretValues[3] as BigInteger

    Log.i(TAG, "registerPostTest secret_r " + secret_r.toString())
    Log.i(TAG, "registerPostTest secret_c " + secret_c.toString())
    Log.i(TAG, "registerPostTest inv_secret_c " + inv_secret_c.toString())
    Log.i(TAG, "registerPostTest secret " + secret.toString())

    val intBytes = secret.toByteArray()
    val secretStr = Base64.getEncoder().encodeToString(intBytes)

    Log.w(TAG, "registerPostApi with pin " + pin + " secret " + secretStr)
    registerBackend.postRegister(pin, secretStr)

    return secretValues
}


fun registerPutApi(rep: BigInteger) {
    // setup backend clients
    val registerBackend: RegisterClient = RegisterClient(StatusActivity)

    //Log.i(TAG, "registerPutApi rep " + rep.toString())
    //Log.i(TAG, "registerPutApi secret_r " + secret_r.toString())
    //Log.i(TAG, "registerPutApi secret_c " + secret_c.toString())
    //Log.i(TAG, "registerPutApi inv_secret_c " + inv_secret_c.toString())
    //Log.i(TAG, "registerPutApi secret " + secret.toString())


    val str_secret_r: String =
        Base64.getEncoder().encodeToString(int32toByteArray(secret_r.toInt())).toString()

    val secret_sigma = (rep * inv_secret_c).mod(public_key_n)
    val str_secret_sigma = Base64.getEncoder().encodeToString(secret_sigma.toByteArray())

    Backend.secret_sigma = str_secret_sigma
    Backend.secret_r = str_secret_r
    registerBackend.putRegister(str_secret_r, str_secret_sigma)
}

fun reportGetApi(tokens: Int) {
    // setup backend clients
    val reportBackend: ReportClient = ReportClient(StatusActivity)

    val r: String = Backend.secret_r
    val sigma: String = Backend.secret_sigma

    Log.d(TAG, "reportGetTest with nb of tokens " + tokens)
    reportBackend.getReport(r, sigma, tokens)
}

fun reportPostApi(pet: PET, token: String) {
    // setup backend clients
    val reportBackend: ReportClient = ReportClient(StatusActivity)

    val timestamp: Long = pet.timestamp

    Log.d(TAG, "reportPostApi with pets " + pet + " time " + timestamp.toString())
    return reportBackend.postReport(
        token,
        pet.rtl.fold("", { str, it -> str + "%02x".format(it) }),
        timestamp
    )
}

fun statusGetApi(uid: String, pets: List<String>, key: String) {
    // setup backend clients
    val statusBackend: StatusClient = StatusClient(StatusActivity)

    // val pets: List<String> = mutableListOf("xxx", "yyy")
    val timestamp: Long = Date().time

    Log.d(TAG, "statusGetTest with id " + uid + " time " + timestamp.toString())
    return statusBackend.getStatus(uid, key, pets)
}

fun exposedGetApi() {
    // setup backend clients
    val exposedBackend: ExposedClient = ExposedClient(StatusActivity)

    val r: String = Backend.secret_r
    val sigma: String = Backend.secret_sigma

    return exposedBackend.getExposed(r, sigma)
}

//
//
fun testUtils() {

    val phone: String = "06000000"

    val h1 = sha256("06000000".toByteArray(Charsets.UTF_8))
    //Log.d(TAG, "shadigest " + Base64.getEncoder().encodeToString((h1)))
    Log.d(TAG, "shadigest 2 " + h1.toHexString())

    //val h2 = sha256hex(phone)
    //Log.d(TAG, "sha256hex " + h2)

    val h3 = token_hash(BigInteger("255"))
    Log.d(TAG, "token_hash " + h3.toString())

    val h8 = gen_secret(
        65537,
        BigInteger("22859415401688594574226161338526647114997810868885918956038237851734163364791116757008181052201531696390098964940881131807207711136141939148541572278753613102980026798692338044735621006712536638996968025766889852940297203852339839714947630247297201550319653693665117372371404811861204807537657750785490907573658669534302871802222509071280699782536078870438075473687153662844033115950875396535070229540496976100292338953009560461548411552226401662004648876667079924003698795652284531153395388627055581166287782128440804505257510942949012342559137117078187423100732508229350878272820220287488962342216273366751827360683")
    )
    Log.d(TAG, "gen_secret " + h8[0].toString())
    Log.d(TAG, "gen_secret " + h8[1].toString())
    Log.d(TAG, "gen_secret " + h8[2].toString())
    Log.d(TAG, "gen_secret " + h8[3].toString())

}
