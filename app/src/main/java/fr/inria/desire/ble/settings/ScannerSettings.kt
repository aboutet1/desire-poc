package fr.inria.desire.ble.settings

import android.bluetooth.le.ScanSettings
import fr.inria.desire.ble.interfaces.IObservable
import fr.inria.desire.ble.interfaces.IObserver
import fr.inria.desire.ble.models.ContactedEbid
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.RTLData
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantLock
import java.util.function.Predicate
import kotlin.concurrent.withLock

object ScannerSettings : IObservable {
    var scannerStartTs: Long = 0
    override val observers: ArrayList<IObserver> = ArrayList()

    var started = false
    private val connectLock = ReentrantLock()

    var reportDelay: Long = 1000
    var scanModePos: Int = 1
    var scanMode: Int = 0
        get() = when (scanModePos) {
            2 -> ScanSettings.SCAN_MODE_LOW_LATENCY
            1 -> ScanSettings.SCAN_MODE_BALANCED
            else -> ScanSettings.SCAN_MODE_LOW_POWER
        }
    var seenEBID: ConcurrentHashMap<String, ContactedEbid> = ConcurrentHashMap()

    @ExperimentalUnsignedTypes
    var EBIDdata: ConcurrentHashMap<EBID, RTLData> = ConcurrentHashMap()

    private var currentNeabyDevices: MutableList<NearbyDevice> = ArrayList()

    fun addDevice(sid: String) {
        connectLock.withLock {
            var found = false
            currentNeabyDevices.forEach {
                if (sid == it.sid) {
                    it.updateTimestamp()
                    found = true
                }
            }
            if (!found) {
                currentNeabyDevices.add(
                    NearbyDevice(sid)
                )
            }
        }
        // force UI update thru observales
        dataChanged()
    }

    fun nearbyDevicesCount(): Int {
        connectLock.withLock {
            val condition: Predicate<NearbyDevice> =
                Predicate<NearbyDevice> { device -> device.isOutdated() }
            currentNeabyDevices.removeIf(condition)
        }
        return currentNeabyDevices.count()
    }
}


class NearbyDevice(var sid: String) {
    private var timestamp: Long =
        LocalDateTime.now(ZoneOffset.UTC).atZone(ZoneOffset.UTC).toEpochSecond()

    fun updateTimestamp() {
        timestamp = LocalDateTime.now(ZoneOffset.UTC).atZone(ZoneOffset.UTC).toEpochSecond()
    }

    fun isOutdated(): Boolean {
        val delay =
            LocalDateTime.now(ZoneOffset.UTC).atZone(ZoneOffset.UTC).toEpochSecond() - timestamp
        return delay > 60
    }
}