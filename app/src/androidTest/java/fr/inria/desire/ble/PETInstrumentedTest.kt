package fr.inria.desire.ble

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.PET
import fr.inria.desire.ble.settings.Constants
import org.junit.Assert

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

@ExperimentalUnsignedTypes
@RunWith(AndroidJUnit4::class)
class PETInstrumentedTest {

    @Test
    fun petGenerate_isCorrect() {

        val ebid1: EBID = EBID()
        ebid1.generate()
        val keyPar1 = CryptoManager.getKeys()

        val ebid2: EBID = EBID()
        ebid2.generate()
        val keyPar2 = CryptoManager.getKeys()

        val pet21 = PET.fromEBID(ebid1)
        assertNotNull(pet21)


        CryptoManager.setKeys(keyPar1.first, keyPar1.second)
        val pet12 = PET.fromEBID(ebid2)
        assertNotNull(pet12)
        val keyPar3 = CryptoManager.getKeys()

        Assert.assertTrue(keyPar1.first.contentEquals(keyPar3.first))
        Assert.assertTrue(keyPar1.second.contentEquals(keyPar3.second))

        assertTrue(pet12!!.etl.contentEquals(pet21!!.rtl))
        assertTrue(pet21.etl.contentEquals(pet12.rtl))

    }


}

