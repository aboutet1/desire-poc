package fr.inria.desire.ble


import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.PET
import fr.inria.desire.ble.settings.Constants
import org.junit.Assert

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets.UTF_8
import java.util.*
import java.util.zip.GZIPOutputStream
import kotlin.collections.ArrayList

import java.io.Serializable

@ExperimentalUnsignedTypes
@RunWith(AndroidJUnit4::class)
class PETJsonTest {

    @Test
    fun petGenerate_isCorrect() {

        // generate keys and EBID for device
        val localEbid: EBID = EBID()
        localEbid.generate()
        val localKeys = CryptoManager.getKeys()

        var rtl: MutableList<Pair<PET, MutableList<RemoteInfo<Long, Int, Int>>>> = ArrayList()
        var keys: MutableList<Pair<EBID, Pair<ByteArray, ByteArray>>> = ArrayList()

        // generate EBIDs for remote devices and the associated PETs
        for (index in 1..100) {
            val remoteEbid: EBID = EBID()
            remoteEbid.generate()
            val remoteKeys = CryptoManager.getKeys()

            val remoteInfo = Pair(remoteEbid, remoteKeys)
            keys.add(remoteInfo)

            CryptoManager.setKeys(localKeys.first, localKeys.second)
            val petArray = CryptoManager.generatePet(remoteEbid.ebid!!.asByteArray(), ByteArray(0))
            val remootePet = PET(petArray!!, petArray)

            val nbMessages = (10..80).random()
            var msgInfos: MutableList<RemoteInfo<Long, Int, Int>> = ArrayList()
            for (indexMsg in 1..nbMessages) {
                var msgInfo = RemoteInfo((Date().time / 1000), (1..10).random(), (0..255).random())
                msgInfos.add(msgInfo)
            }
            rtl.add(Pair(remootePet, msgInfos))
        }

        // create list of local EBID/pets with the associated emmision power and random
        var etl: MutableList<Pair<PET, LocalInfo<Long, Int, Int, Int>>> = ArrayList()
        // assumes new EBID every 15 min
        val NB_PETS_PER_DAY = 4 * 24
        for (index in 1..NB_PETS_PER_DAY) {

            val tmpEbid: EBID = EBID()
            tmpEbid.generate()
            CryptoManager.setKeys(localKeys.first, localKeys.second)
            val tmpPetArray = CryptoManager.generatePet(tmpEbid.ebid!!.asByteArray(), ByteArray(0))
            val tmpPet = PET(tmpPetArray!!, tmpPetArray)

            etl.add(
                Pair(
                    tmpPet,
                    LocalInfo(
                        (Date().time / 1000),
                        (1..10).random(),
                        (0..255).random(),
                        (0..255).random()
                    )
                )
            )
        }

        val gson = Gson()
        val gsonPretty = GsonBuilder().setPrettyPrinting().create()

        val jsonEtl: String = gson.toJson(etl)
        println(jsonEtl)
        println(jsonEtl.length)
        val compressedEtl = gzip(jsonEtl)
        println(compressedEtl.size)


        val jsonRtl: String = gson.toJson(rtl)
        println(jsonRtl)
        println(jsonRtl.length)
        val compressedRtl = gzip(jsonRtl)
        println(compressedRtl.size)

    }


    fun gzip(content: String): ByteArray {
        val bos = ByteArrayOutputStream()
        GZIPOutputStream(bos).bufferedWriter(UTF_8).use { it.write(content) }
        return bos.toByteArray()
    }
}


data class RemoteInfo<out A, out B, out C>(
    val ts: A,
    val rssi: B,
    val grx: C
) : Serializable {
    override fun toString(): String = "($ts, $rssi, $grx)"
}

data class LocalInfo<out A, out B, out C, out D>(
    val ts: A,
    val rand: B,
    val txpower: C,
    val gtx: D
) : Serializable {
    override fun toString(): String = "($ts, $rand, $txpower, $gtx)"
}
