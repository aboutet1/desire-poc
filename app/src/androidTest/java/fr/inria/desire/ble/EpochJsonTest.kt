package fr.inria.desire.ble

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.gson.Gson
import fr.inria.desire.ble.managers.EpochDataManager
import fr.inria.desire.ble.models.EBID
import fr.inria.desire.ble.models.RTLData
import org.junit.Test
import org.junit.runner.RunWith
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets
import java.util.zip.GZIPOutputStream
import kotlin.random.Random

@ExperimentalUnsignedTypes
@RunWith(AndroidJUnit4::class)
class EpochJsonTest {

    @Test
    fun epoch_json_one_remote() {
        val remoteEBID = EBID()
        remoteEBID.generate()
        val localEBID = EBID()
        localEBID.generate()
        val EBIDData = HashMap<EBID, RTLData>()
        val tmpRtlData = RTLData()
        val ts = System.currentTimeMillis()

        tmpRtlData.startTimestamp = ts
        EBIDData[remoteEBID] = tmpRtlData
        tmpRtlData.computeObf(localEBID, remoteEBID)

        for (cTs in ts until ts + (15 * 60 * 1000) step 5000) {
            tmpRtlData.add(Random.nextInt(-66, -40), cTs)
        }
        EpochDataManager.addEpochData(localEBID, EBIDData, ts)

        val reqData = EpochDataManager.getRequestData()
        val expData = EpochDataManager.getExposedData()
        val listExp = ArrayList<JSONStructExposed>()
        for (d in expData) {
            listExp.add(
                JSONStructExposed(
                    d.first.etl.joinToString("") { "%02x".format(it) },
                    d.second
                )
            )
        }
        val gson = Gson()
        val expJson = gson.toJson(listExp)
        Log.d("Exposed", expJson)
        Log.d(
            "Exposed",
            "Uncompressed size : " + expJson.length + " gzipped size : " + gzip(expJson).size
        )
        val listReq = ArrayList<JSONStructRequest>()
        for (d in reqData) {
            val listWin = ArrayList<JSONStructWindow>()
            for (w in d.second.asTriple()) {
                listWin.add(JSONStructWindow(w.third, w.first))
            }
            listReq.add(
                JSONStructRequest(
                    d.first.rtl.joinToString("") { "%02x".format(it) },
                    listWin
                )
            )
        }
        val reqJson = gson.toJson(listReq)
        Log.d("Request", reqJson)
        Log.d(
            "Request",
            "Uncompressed size : " + reqJson.length + " gzipped size : " + gzip(reqJson).size
        )
    }

    @Test
    fun epoch_json_100_remote() {
        val remoteEBIDs = ArrayList<EBID>()
        for (i in 1..100) {
            val remoteEBID = EBID()
            remoteEBID.generate()
            remoteEBIDs.add(remoteEBID)
        }
        Log.d("remote", remoteEBIDs.size.toString())
        val localEBID = EBID()
        localEBID.generate()
        val EBIDData = HashMap<EBID, RTLData>()
        val ts = System.currentTimeMillis()
        for (remoteEBID in remoteEBIDs) {
            val tmpRtlData = RTLData()
            tmpRtlData.startTimestamp = ts
            EBIDData[remoteEBID] = tmpRtlData
            tmpRtlData.computeObf(localEBID, remoteEBID)
        }
        for (tmpRtlData in EBIDData.values) {
            tmpRtlData.add(Random.nextInt(-66, -40), ts)
        }

        for (i in 1..1000) {
            for (tmpRtlData in EBIDData.values) {
                tmpRtlData.add(Random.nextInt(-66, -40), Random.nextLong(ts, ts + (15 * 60 * 1000)))
            }
        }

        for (tmpRtlData in EBIDData.values) {
            tmpRtlData.add(Random.nextInt(-66, -40), ts + (15 * 60 * 1000))
        }

        EpochDataManager.addEpochData(localEBID, EBIDData, ts)

        val reqData = EpochDataManager.getRequestData()
        val expData = EpochDataManager.getExposedData()
        val listExp = ArrayList<JSONStructExposed>()
        for (d in expData) {
            listExp.add(
                JSONStructExposed(
                    d.first.etl.joinToString("") { "%02x".format(it) },
                    d.second
                )
            )
        }
        val gson = Gson()
        val expJson = gson.toJson(listExp)
        Log.d("Exposed", listExp.size.toString())
        Log.d("Exposed", expJson)
        Log.d(
            "Exposed",
            "Uncompressed size : " + expJson.length + " gzipped size : " + gzip(expJson).size
        )
        val listReq = ArrayList<JSONStructRequest>()
        for (d in reqData) {
            val listWin = ArrayList<JSONStructWindow>()
            for (w in d.second.asTriple()) {
                listWin.add(JSONStructWindow(w.third, w.first))
            }
            listReq.add(
                JSONStructRequest(
                    d.first.rtl.joinToString("") { "%02x".format(it) },
                    listWin
                )
            )
        }
        val reqJson = gson.toJson(listReq)
        Log.d("Request", listReq.size.toString())
        Log.d("Request", reqJson)
        Log.d(
            "Request",
            "Uncompressed size : " + reqJson.length + " gzipped size : " + gzip(reqJson).size
        )
    }

    fun gzip(content: String): ByteArray {
        val bos = ByteArrayOutputStream()
        GZIPOutputStream(bos).bufferedWriter(StandardCharsets.UTF_8).use { it.write(content) }
        return bos.toByteArray()
    }

    data class JSONStructExposed(val pet: String, val gtx: Int) {}
    data class JSONStructRequest(val pet: String, val windows: List<JSONStructWindow>) {}
    data class JSONStructWindow(val ts: Long, val rssi: Int?) {}

    fun byteArray2HexString(bArr: ByteArray): String {
        var ret = ""
        for (b in bArr) {
            ret += b.toString(16).padStart(2, '0')
        }
        return ret
    }
}