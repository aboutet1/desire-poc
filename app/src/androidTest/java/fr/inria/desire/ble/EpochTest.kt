package fr.inria.desire.ble


import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.inria.desire.ble.managers.CryptoManager
import fr.inria.desire.ble.managers.EpochDataManager
import fr.inria.desire.ble.models.*
import fr.inria.desire.ble.settings.Constants
import org.junit.Assert

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets.UTF_8
import java.util.*
import java.util.zip.GZIPOutputStream
import kotlin.collections.ArrayList

import java.io.Serializable

@ExperimentalUnsignedTypes
@RunWith(AndroidJUnit4::class)
class EpochTest {

    @Test
    fun epoch_creation() {
        val startTs: Long = 1000
        val endTs: Long = 10000
        val epoch = Epoch(startTs, endTs)
        assertEquals(startTs, epoch.startTimestamp)
        assertEquals(endTs, epoch.endTimestamp)
    }

    @Test
    fun epochData_isCorrect() {
        var crypto: CryptoManager = CryptoManager
        val ebid1: EBID = EBID()
        ebid1.generate()
        val keyPar1 = CryptoManager.getKeys()


        val startTs: Long = 0
        val endTs: Long = 1000
        val epoch1 = Epoch(startTs, endTs)

        val ts1: Long = 10
        var epochData1: Triple<RTLData, PET, EBID> = createEpochDatForOneEbida(ts1)

        val ts2: Long = ts1 + 100
        var epochData2: Triple<RTLData, PET, EBID> = createEpochDatForOneEbida(ts2)

        val ts3: Long = ts2 + 15 * 60 * 1000
        var epochData3: Triple<RTLData, PET, EBID> = createEpochDatForOneEbida(ts3)

        var ok: Boolean = epoch1.add(epochData1)
        assertTrue(ok)
        ok = epoch1.add(epochData2)
        assertTrue(ok)
        ok = epoch1.add(epochData3)
        assertFalse(ok)
        assertTrue(epoch1.data.size == 2)

        var epochMgr: EpochDataManager = EpochDataManager
        epochMgr.addEpochData(ebid1, , 0)

    }


    //
    //
    //

    fun createEbidData(ts: Long) {
    }

    fun createEpochDatForOneEbida(ts: Long): Triple<RTLData, PET, EBID> {
        val rtldata: RTLData = RTLData()

        rtldata.startTimestamp = ts
        val item1: RTLItem = RTLItem()
        item1.rssi = 10
        item1.timestamp = ts + 10
        rtldata.data.add(item1)
        val item2: RTLItem = RTLItem()
        item2.rssi = 20
        item2.timestamp = ts + 100
        rtldata.data.add(item2)

        val ebidDistant: EBID = EBID()
        ebidDistant.generate()
        val pet1 = PET.fromEBID(ebidDistant)
        assertNotNull(pet1)

        return Triple(rtldata, pet1!!, ebidDistant)
        fail()
    }
}


